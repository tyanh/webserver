﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Webserver.Models
{
    public class ConfigTable
    {
        public string id { get; set; }
        public string name_ { get; set; }
        public string title { get; set; }
        public string titleGv { get; set; }
        public string kind { get; set; }
        public string typedisplay { get; set; }
        public string column { get; set; }
        public string columnGv { get; set; }
        public string valid { get; set; }
        public string url { get; set; }
        public string key_ { get; set; }
        public string seotab { get; set; }
        public string attrtab { get; set; }
        public string pathupload { get; set; }
        public string sortorder { get; set; }
        public string export { get; set; }
        public string search { get; set; }
        public string keywork { get; set; }
        public string query { get; set; }
        public string subsearch { get; set; }
        public string valsubsearch { get; set; }
        public string dlonelinemobile { get; set; }
        public string headerline { get; set; }
        public string titlepage { get; set; }
        public string idmenu { get; set; }
        public string isscan { get; set; }

        public List<string> ltable = new List<string>();
        public List<string> laction = new List<string>();
        public void SetConfigTable(string table,string sub)
        {
            Sqlcommon sql = new Sqlcommon();
            if (sub != "" && sub != null)
                sub = "/" + sub;
            else
                sub = "";
            // string str = "select attribute_table.*,menus.id as idmenu from attribute_table.frendly_url LEFT JOIN menus ON menus.link like '%'+attribute_table.frendly_url+'%' where frendly_url='" + table + "'";
            string qrmenu = "menus.name_ as titlepage,menus.id as idmenu,menus.notcreate as notcreate,menus.notdelete as notdelete,menus.notexport as notexport,menus.noedit as noedit,menus.isinvoice as isinvoice";
            string str = "select attribute_table.*,"+ qrmenu + " from attribute_table LEFT JOIN menus ON menus.link like '%'+attribute_table.frendly_url+'%' where frendly_url='" + table + "'";
            if(sub!="")
                str = "select attribute_table.*," + qrmenu + " from attribute_table LEFT JOIN menus ON menus.link like '%'+attribute_table.frendly_url+'%' where frendly_url='" + table + "' AND menus.link='" + table + sub + "'";

            var data = sql.finddata(str);
            name_ = data["name_"].ToString();
            titlepage = data["titlepage"].ToString();
            title = data["titles"].ToString();
            kind = data["atr_"].ToString();
            column = data["columns"].ToString();
            subsearch = data["subsearch"].ToString();
            valid = data["valid"].ToString();
            columnGv = data["coldisplay"].ToString();
            titleGv = data["titledisplay"].ToString();
            url = data["frendly_url"].ToString();
            key_ = data["key_"].ToString();
            seotab = data["seotab"].ToString();
            attrtab = data["attributestab"].ToString();
            pathupload = data["pathupload"].ToString();
            typedisplay = data["typedisplay"].ToString();
            id= data["id"].ToString();
            sortorder = data["sortorder"].ToString();
            export = data["exportcl"].ToString();
            dlonelinemobile = data["dlonelinemobile"].ToString();
            headerline = data["headerline"].ToString();
            search = data["search"].ToString();
            idmenu = data["idmenu"].ToString();
            isscan = data["isscan"].ToString();
            laction.Add(data["notcreate"].ToString());
            laction.Add(data["noedit"].ToString());
            laction.Add(data["notdelete"].ToString());
            laction.Add(data["notexport"].ToString());
            laction.Add(data["isinvoice"].ToString());
        }
    }
}
