﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Webserver.Models
{
    public class LoginModel
    {
        public string id { get; set; }
        public string username { get; set; }
        public string pass { get; set; }
        public string admin { get; set; }
        public string delete { get; set; }
        public string permistion { get; set; }
        public string sessionlg { get; set; }
        public string groupid { get; set; }

    }
}
