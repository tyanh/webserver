﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Webserver.Models
{
    public class SqlDataOld
    {
        public string cnn;
        //  private readonly Common cmdcommon = new Common();
        public Dictionary<string, object> SerializeRow(IEnumerable<string> cols, SqlDataReader reader)
        {
            var result = new Dictionary<string, object>();
            foreach (var col in cols)
            {
                try { result.Add(col, reader[col]); } catch { };
            }
            string rs = JsonConvert.SerializeObject(result);
            return result;
        }
        public List<string> GetcolumnTableNames(string tablename)
        {
            var result = new List<string>();
            List<string> exp = new List<string>() { "usercreate", "createdate", "lastupdate", "userupdate" };
            using (var sqlCon = new SqlConnection(cnn))
            {
                sqlCon.Open();
                var sqlCmd = sqlCon.CreateCommand();
                sqlCmd.CommandText = "select * from " + tablename + " where 1=0";  // No data wanted, only schema
                sqlCmd.CommandType = CommandType.Text;

                var sqlDR = sqlCmd.ExecuteReader();
                var dataTable = sqlDR.GetSchemaTable();

                foreach (DataRow row in dataTable.Rows)
                {
                    var cl = row.ItemArray[0].ToString();
                    if (exp.IndexOf(cl) == -1)
                        result.Add(row.ItemArray[0].ToString());
                }
            }

            return result;
        }
        public List<string> GetTableNames()
        {
            SqlConnection con = new SqlConnection(cnn);
            con.Open();
            System.Data.DataTable dt = con.GetSchema("Tables");
            List<string> tables = new List<string>();
            foreach (System.Data.DataRow row in dt.Rows)
            {
                if (row[3].ToString().Equals("BASE TABLE", StringComparison.OrdinalIgnoreCase)) //ignore views
                {
                    string tableName = row[2].ToString();
                    //string schema = row[1].ToString();
                    tables.Add(tableName);
                }
            }
            con.Close();
            return tables;
        }
        public List<Dictionary<string, object>> getmultidata(List<string> sql)
        {

            List<Dictionary<string, object>> results = new List<Dictionary<string, object>>();

            SqlConnection con = new SqlConnection(cnn);
            con.Open();
            foreach (var j in sql)
            {
                List<Dictionary<string, object>> rs = new List<Dictionary<string, object>>();
                List<string> namecl = new List<string>();
                SqlCommand sqlCommand = new SqlCommand(j, con);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                for (var i = 0; i < reader.FieldCount; i++)
                    namecl.Add(reader.GetName(i));
                while (reader.Read())
                {
                    results.Add(SerializeRow(namecl, reader));
                }

            }
            con.Close();

            return results;
        }
        public List<Dictionary<string, object>> getdataSQL(string tb, string where)
        {

            List<Dictionary<string, object>> results = new List<Dictionary<string, object>>();

            List<string> namecl = new List<string>();
            SqlConnection con = new SqlConnection(cnn);
            con.Open();
            string str = "SELECT * FROM " + tb + " " + where;
            SqlCommand sqlCommand = new SqlCommand(str, con);
            SqlDataReader reader = sqlCommand.ExecuteReader();
            for (var i = 0; i < reader.FieldCount; i++)
                namecl.Add(reader.GetName(i));
            while (reader.Read())
            {

                results.Add(SerializeRow(namecl, reader));
            }
            con.Close();

            return results;

        }
        public SqlDataOld()
        {
            var configurationBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path, false);
            var root = configurationBuilder.Build();
            cnn = root.GetSection("ConnectionStrings").GetSection("connectdataold").Value;

        }
    }
}
