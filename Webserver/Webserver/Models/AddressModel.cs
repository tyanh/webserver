﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Webserver.Models
{
    public  class AddressModel
    {
       public List<dynamic> city = JsonConvert.DeserializeObject<List<dynamic>>(File.ReadAllText("wwwroot/Citys.txt"));
       public List<dynamic> distr = JsonConvert.DeserializeObject<List<dynamic>>(File.ReadAllText("wwwroot/Township.txt"));
        public string getcity(string o)
        {
            return city.FirstOrDefault(s=>s["ma"].ToString()==o)["ten"];
        }
        public string getdistr(string o)
        {
            return distr.FirstOrDefault(s => s["ma"].ToString() == o)["ten"];
        }
    }
}
