﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Webserver.Models
{
    public class headerinvoiceModel
    {
        public string nameus { get; set; }
        public string idus { get; set; }
        public string invoicenumber { get; set; }
        public List<Dictionary<string, object>> wareh { get; set; }
    }
}
