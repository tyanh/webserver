﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Webserver.Models
{
    public class ChatModel
    {

        public string name { get; set; }
        public string code { get; set; }
        public string contents { get; set; }
        public string isproduct { get; set; }
        public string status { get; set; }
        public List<Dictionary<string, object>> keysall { get; set; }
        public List<Dictionary<string, object>> keyuser { get; set; }
        public void setkeyall(string code)
        {
            sqlchat sql = new sqlchat();
            keysall =sql.getdataSQL("keyworks", "");
            if (code != "")
            {
                keyuser = keysall.Where(s => s["coderl"].ToString() == code).ToList();
                keysall = keysall.Except(keyuser).ToList();
            }
        }
    }
}
