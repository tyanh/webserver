﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Webserver.Models
{
    public class sqlchat
    {
        public string cnn;
        static readonly string[] VietNamChar = new string[]
          {
                "aAeEoOuUiIdDyY",
                "áàạảãâấầậẩẫăắằặẳẵ",
                "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
                "éèẹẻẽêếềệểễ",
                "ÉÈẸẺẼÊẾỀỆỂỄ",
                "óòọỏõôốồộổỗơớờợởỡ",
                "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
                "úùụủũưứừựửữ",
                "ÚÙỤỦŨƯỨỪỰỬỮ",
                "íìịỉĩ",
                "ÍÌỊỈĨ",
                "đ",
                "Đ",
                "ýỳỵỷỹ",
                "ÝỲỴỶỸ"
          };
        public Dictionary<string, object> finddata2(string table, string where)
        {
            string str = "select top 1 * from " + table + " " + where;
            Dictionary<string, object> results = new Dictionary<string, object>();
            List<string> namecl = new List<string>();
            SqlConnection con = new SqlConnection(cnn);
            con.Open();
            SqlCommand sqlCommand = new SqlCommand(str, con);
            SqlDataReader reader = sqlCommand.ExecuteReader();
            for (var i = 0; i < reader.FieldCount; i++)
                namecl.Add(reader.GetName(i));
            while (reader.Read())
            {

                results = SerializeRow(namecl, reader);
            }
            con.Close();

            return results;
        }
        public string removeVn(string str)
        {
            str = str.ToLower();
            for (int i = 1; i < VietNamChar.Length; i++)
            {
                for (int j = 0; j < VietNamChar[i].Length; j++)
                    str = str.Replace(VietNamChar[i][j], VietNamChar[0][i - 1]);
            }
            return str;
        }
        public bool checkdataExit(string tb, string where)
        {
            bool rs = true;
            SqlConnection conn = new SqlConnection(cnn);
            conn.Open();
            string str = "SELECT id FROM " + tb + " " + where;
            SqlCommand cmd = new SqlCommand(str, conn);
            try
            {
                int count = int.Parse(cmd.ExecuteScalar().ToString());
                if (count > 0)
                    rs = false;
            }
            catch { rs = true; }
            conn.Close();
            return rs;
        }
        public sqlchat()
        {
            var configurationBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path, false);
            var root = configurationBuilder.Build();
            cnn = root.GetSection("ConnectionStrings").GetSection("connectchat").Value;

        }
        public bool runSQL(string str)
        {
            SqlConnection conn = new SqlConnection(cnn);
            conn.Open();
            SqlCommand cmd = new SqlCommand(str, conn);
            cmd.ExecuteNonQuery();
            // Execute the command
            conn.Close();
            return true;
        }
        public Dictionary<string, object> SerializeRow(IEnumerable<string> cols, SqlDataReader reader)
        {
            var result = new Dictionary<string, object>();
            foreach (var col in cols)
            {
                try { result.Add(col, reader[col]); } catch { };
            }
            string rs = JsonConvert.SerializeObject(result);
            return result;
        }
        
        public List<Dictionary<string, object>> getdataSQL(string tb, string where)
        {

            List<Dictionary<string, object>> results = new List<Dictionary<string, object>>();

            List<string> namecl = new List<string>();
            SqlConnection con = new SqlConnection(cnn);
            con.Open();
            string str = "SELECT * FROM " + tb + " " + where;
            SqlCommand sqlCommand = new SqlCommand(str, con);
            SqlDataReader reader = sqlCommand.ExecuteReader();
            for (var i = 0; i < reader.FieldCount; i++)
                namecl.Add(reader.GetName(i));
            while (reader.Read())
            {

                results.Add(SerializeRow(namecl, reader));
            }
            con.Close();

            return results;

        }
    }
}
