﻿using DocumentFormat.OpenXml.InkML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Webserver.Models
{
    public class ModelViews
    {
        public ConfigTable config = new ConfigTable();
        public DataList data { get; set; }
        public string name { get; set; }

        public string totalrecord { get; set; }
        public void initView(string table, string id, string keywork,string subkey, ConfigTable config,int skip)
        {
            Sqlcommon sql = new Sqlcommon();
            Common cmd = new Common();
            this.config = config;
            config.keywork = keywork;
            data = new DataList();
            string link = table;
                if (id != null)
                    link = link + "/" + id;
                List<string> searchlike = new List<string>();
                List<string> searchAnd = new List<string>();
                if (subkey != null && subkey != "" && subkey != "[]")
                {
                    config.valsubsearch = subkey;
                    List<dynamic> subfind = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(subkey);
                    string[] ss_ = config.subsearch.Split(',');
                    foreach (var s_ in ss_)
                    {

                        var x = s_.Split('/');
                        var vl = subfind.FirstOrDefault(s => s.cl == x[3]).vl;
                        if (vl == "")
                            continue;
                        string dtype = x[0];

                        if (dtype == "datepicker")
                        {
                            vl = cmd.convertDate(vl.ToString());
                            if (x[x.Length - 1] == "+")
                                vl = DateTime.Parse(vl).AddDays(1).ToString();
                        }
                        searchAnd.Add(x[1] + x[2] + "N'" + vl + "'");
                    }
                }
                if (keywork != null && keywork != "")
                {
                    var search = config.search.Split(',');
                    foreach (var s_ in search)
                    {
                        var sItem = s_.Split('/');
                        if (sItem[0] == "main")
                            searchlike.Add("LOWER("+sItem[1]+")" + " like N'%" +cmd.encodeHtml(keywork) + "%'");
                        else
                        {
                            string tb = sItem[0];
                            try
                            {
                                string clindex = sItem[1];
                                if (sItem[1].ToString().Split('&').Length>1)
                                {
                                var arr_ = sItem[1].ToString().Split('&');
                                clindex = arr_[0];
                                if (arr_[1] == "seostring")
                                    keywork = cmd.RemoveUnicode(keywork).Replace(" ","-");
                                }
                                string sql_sub = "where " + clindex + " like N'%" + cmd.encodeHtml(keywork) + "%' ";
                                var strsql = sql.finddata2(tb, "where " + clindex + " like N'%" + cmd.encodeHtml(keywork) + "%' ")[sItem[3]].ToString();
                                searchlike.Add(sItem[2] + " like '%" + strsql + "%'");
                            }
                            catch { }
                        }
                    }
                }
                var getwhere = sql.finddata2("menus", "where link='" + link + "'");
            if (getwhere.Count > 0)
            {
                name = getwhere["name_"].ToString();
            }
                string order = "order by lastupdate desc";
                if (config.sortorder != "")
                    order = "order by " + config.sortorder + "";
                var find = "where ";
                if (searchlike.Count > 0)
                {
                    find = find + string.Join(" OR ", searchlike);

                }
                if (searchAnd.Count > 0)
                {
                    string subfind = string.Join(" AND ", searchAnd);
                    if (searchlike.Count > 0)
                        find = find + " AND ";
                    find = find + "(" + subfind + ")";
                }
                if (searchlike.Count > 0 || searchAnd.Count > 0)
                    order = find + " " + order;
                if (getwhere.Count > 0 && getwhere["query"].ToString() != "")
                {
                    order = "where " + getwhere["query"].ToString() + " " + order;
                }
                data.key = keywork;
                string strgetdata = "select * from " + config.name_ + " " + order + " OFFSET "+ skip + " ROWS FETCH NEXT " + Startup.maxrecord + " ROWS ONLY";
                data.data = sql.getdataSQLstr(strgetdata);
                if (skip <= 0)
                {
                    if (find == "where ")
                        find = "";
                    data.sumdata = sql.sumdata(config.name_, config.columnGv.Split(','), config.typedisplay.Split(','), find);
                }
                
        }
    }
}
