﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Webserver.Models
{
    public class SqlTable
    {
        string cnn="";
        public SqlTable()
        {
            var configurationBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path, false);
            var root = configurationBuilder.Build();
            cnn = root.GetSection("ConnectionStrings").GetSection("connectdata").Value;

        }
        public List<dynamic> GetcolumnTableNames(string tablename)
        {
            var result = new List<dynamic>();
            List<string> exp = new List<string>() { "usercreate", "createdate", "lastupdate", "userupdate" };
            using (var sqlCon = new SqlConnection(cnn))
            {
                sqlCon.Open();
                var sqlCmd = sqlCon.CreateCommand();
                sqlCmd.CommandText = "select * from " + tablename + " where 1=0";  // No data wanted, only schema
                sqlCmd.CommandType = CommandType.Text;

                var sqlDR = sqlCmd.ExecuteReader();
                var dataTable = sqlDR.GetSchemaTable();

                foreach (DataRow row in dataTable.Rows)
                {
                    var cl = row.ItemArray[0].ToString();
                    if (exp.IndexOf(cl) == -1)
                    {
                        var name = row.ItemArray[0].ToString();
                        var type = row.ItemArray[24].ToString();
                        var length = row.ItemArray[2].ToString();
                        result.Add(new {name= name,type=type, length= length });
                    }
                }
            }
            result = result.OrderBy(s => s.name).ToList();
            return result;
        }
        public List<string> GetTableNames()
        {
            SqlConnection con = new SqlConnection(cnn);
            con.Open();
            System.Data.DataTable dt = con.GetSchema("Tables");
            List<string> tables = new List<string>();
            foreach (System.Data.DataRow row in dt.Rows)
            {
                if (row[3].ToString().Equals("BASE TABLE", StringComparison.OrdinalIgnoreCase)) //ignore views
                {
                    string tableName = row[2].ToString();
                    //string schema = row[1].ToString();
                    tables.Add(tableName);
                }
            }
            con.Close();
            return tables.OrderBy(s=>s).ToList();
        }

    }
}
