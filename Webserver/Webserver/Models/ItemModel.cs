﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Webserver.Models
{
    public class ItemModel
    {
        public string cldata { get; set; }
        public string vldata { get; set; }
        public List<string> key { get; set; }
        public string title { get; set; }
        public ConfigTable config { get; set; }
        public Dictionary<string, object> data { get; set; }
        public Dictionary<string, object> data_sub = new Dictionary<string, object>();
    }
}
