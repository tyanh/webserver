﻿using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Webserver.Models
{
    public class Mysqlcommon
    {
        public string cnn;
        public Mysqlcommon()
        {
            var configurationBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path, false);
            var root = configurationBuilder.Build();
            cnn = root.GetSection("ConnectionStrings").GetSection("connectdata").Value;

        }
        //  private readonly Common cmdcommon = new Common();
        public List<Dictionary<string, object>> getmultidata(List<string> sql)
        {

            List<Dictionary<string, object>> results = new List<Dictionary<string, object>>();

            MySqlConnection con = new MySqlConnection(cnn);
            con.Open();
            foreach (var j in sql)
            {
                List<Dictionary<string, object>> rs = new List<Dictionary<string, object>>();
                List<string> namecl = new List<string>();
                MySqlCommand sqlCommand = new MySqlCommand(j, con);
                MySqlDataReader reader = sqlCommand.ExecuteReader();
                for (var i = 0; i < reader.FieldCount; i++)
                    namecl.Add(reader.GetName(i));
                while (reader.Read())
                {
                    results.Add(SerializeRow(namecl, reader));
                }

            }
            con.Close();

            return results;
        }
        
        public long countId(string table)
        {
            MySqlConnection con = new MySqlConnection(cnn);
            con.Open();
            MySqlCommand comm = new MySqlCommand("SELECT COUNT(id) FROM " + table + "", con);
            Int32 count = (Int32)comm.ExecuteScalar();
            con.Close();
            return count;
        }
        public string strUpdateGiftUse(int id, string qlt1, string qlt2, string user)
        {
            Common cmd = new Common();
            List<string> clguser = new List<string>() { "qualityuser" };
            double u = cmd.convertNumberDouble(qlt1) + cmd.convertNumberDouble(qlt2);
            List<string> vlu = new List<string>() { u.ToString() };
            string upgift = updatesqlOneItemstr("gifts", clguser, vlu, id, user);
            return upgift;

        }
        public bool checkInv(Dictionary<string, object> p, int idhouse, double qlt)
        {
            Common cmd = new Common();
            string warehouse = p["inventoryhouse"].ToString();
            List<dynamic> warehouses = JsonConvert.DeserializeObject<List<dynamic>>(warehouse);
            foreach (var h in warehouses)
            {
                int ii = h.id;
                if (idhouse == ii)
                {
                    double o = cmd.convertNumberDouble(h["remain"].ToString());
                    if (o < qlt)
                        return false;
                }
            }

            return true;
        }
        public string strinventori(Dictionary<string, object> p, double qlt2, string type, dynamic houseid, int cal)
        {
            Common cmd = new Common();
            string warehouse = p["inventoryhouse"].ToString();
            List<dynamic> warehouses = JsonConvert.DeserializeObject<List<dynamic>>(warehouse);
            Dictionary<string, object> iv_ = new Dictionary<string, object>();
            iv_["id"] = houseid["code"].ToString();
            iv_["name"] = houseid["name"].ToString();
            if (cal == -1)
                qlt2 = qlt2 * (-1);
            foreach (var i in cmd.actionIV)
            {
                if (i == type)
                    iv_[i] = qlt2;
                else
                    iv_[i] = 0;
            }
            iv_["remain"] = qlt2;

            if (warehouses != null)
            {
                bool r = false;
                foreach (var h in warehouses)
                {
                    int ii = h.id;

                    if (int.Parse(houseid["code"].ToString()) == ii)
                    {
                        h.name = houseid["name"].ToString();
                        r = true;
                        foreach (var i in cmd.actionIV)
                        {
                            if (i == type)
                            {
                                double qq = cmd.convertNumberDouble(h[i].ToString());
                                qq = qq + qlt2;
                                h[i] = qq;
                                break;
                            }
                        }
                        double qlt_input = cmd.convertNumberDouble(h["input"].ToString());
                        double qlt_return = cmd.convertNumberDouble(h["return"].ToString());
                        double qlt_export = cmd.convertNumberDouble(h["export"].ToString());
                        double qlt_sale = cmd.convertNumberDouble(h["sale"].ToString());
                        double qlt_remain = (qlt_sale + qlt_export) - (qlt_input + qlt_input);
                        h["remain"] = qlt_remain;
                        break;
                    }

                }
                if (!r)
                    warehouses.Add(iv_);
                return JsonConvert.SerializeObject(warehouses);
            }
            else
            {

                warehouses = new List<dynamic>();
                warehouses.Add(iv_);
            }
            return JsonConvert.SerializeObject(warehouses);

        }
        public string UpdateNumberProductWhen_(Dictionary<string, object> p, double qlt2, int type, dynamic houseid, string user, List<string> subcl, List<string> subvl, string typeiv)
        {
            Common cmd = new Common();
            List<string> cl = new List<string>() { "inventoryhouse" };
            List<string> vlsub = new List<string>();

            string warehouse = strinventori(p, qlt2, typeiv, houseid, type);
            vlsub.Add(warehouse);
            for (var i = 0; i < subcl.Count; i++)
            {
                cl.Add(subcl[i]);
                double tmp_ = cmd.convertNumberDouble(p[subcl[i]].ToString());
                if (type == 1)
                    tmp_ = tmp_ + cmd.convertNumberDouble(subvl[i]);
                else
                    tmp_ = tmp_ - cmd.convertNumberDouble(subvl[i]);
                vlsub.Add(tmp_.ToString());
            }

            string up = updatesqlOneItemstr("products", cl, vlsub, int.Parse(p["id"].ToString()), user);
            return up;

        }
        public string UpdateNumberProductWhen_Sale_return(Dictionary<string, object> p, double qlt, string type, dynamic house, List<string> cl_in, List<string> vl_in, string user)
        {
            Common cmd = new Common();
            List<string> cl = new List<string>() { "inventoryhouse", "inventory" };
            cl.InsertRange(0, cl_in);
            List<string> vlsub = new List<string>();
            string warehouse = p["inventoryhouse"].ToString();
            List<Dictionary<string, object>> warehouses = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(warehouse);
            if (warehouses == null)
                warehouses = new List<Dictionary<string, object>>();
            var i_ = warehouses.FirstOrDefault(s => s["id"].ToString() == house["code"].ToString());
            double inventory = cmd.convertNumberDouble(p["inventory"].ToString());
            double price = cmd.convertNumberDouble(p["pricesale"].ToString());
            double money = qlt * price;
            if (type == cmd.actionIV[0])
                inventory = inventory - qlt;
            else if (type == cmd.actionIV[2])
                inventory = inventory + qlt;
            if (i_ == null)
            {
                i_ = new Dictionary<string, object>();
                i_["id"] = house["code"].ToString();
                i_["name"] = house["name"].ToString();
                foreach (var h in cl_in)
                {
                    i_[h] = "0";
                }
                warehouses.Add(i_);
            }
            for (var h = 0; h < cl_in.Count(); h++)
            {
                double v_ = cmd.convertNumberDouble(vl_in[h]);
                try
                {
                    i_[cl_in[h]] = v_ + cmd.convertNumberDouble(i_[cl_in[h]].ToString());
                }
                catch
                {
                    i_[cl_in[h]] = v_;
                }
                double v2 = cmd.convertNumberDouble(p[cl_in[h]].ToString()) + v_;
                vlsub.Add(v2.ToString());
            }
            double rm = cmd.calcuremainProduct(i_);
            i_["remain"] = rm;
            vlsub.Add(JsonConvert.SerializeObject(warehouses));
            vlsub.Add(inventory.ToString());
            string up = updatesqlOneItemstr("products", cl, vlsub, int.Parse(p["id"].ToString()), user);
            return up;

        }
        public string remainInvent(Dictionary<string, object> iv)
        {
            Common cmd = new Common();
            var cl_ = cmd.actionIV;
            var cal_ = cmd.calIV;
            List<double> cl_plus = new List<double>();
            List<double> cl_minus = new List<double>();
            int bg = 0;
            foreach (var j in cal_)
            {
                if (j == "-")
                    cl_minus.Add(cmd.convertNumberDouble(iv[cl_[bg]].ToString()));
                else
                    cl_plus.Add(cmd.convertNumberDouble(iv[cl_[bg]].ToString()));
                bg++;
            }
            double remain = cl_plus.Sum() - cl_minus.Sum();
            return remain.ToString();
        }
        public Dictionary<string, object> strupinventorinew_(List<string> type, List<string> vl, dynamic houseid)
        {
            Common cmd = new Common();
            var cl_ = cmd.actionIV;
            var cal_ = cmd.calIV;
            List<double> cl_plus = new List<double>();
            List<double> cl_minus = new List<double>();
            Dictionary<string, object> iv_ = new Dictionary<string, object>();
            iv_["id"] = houseid["code"].ToString();
            iv_["name"] = houseid["name"].ToString();
            foreach (var j in cl_)
            {
                iv_[j] = "0";
            }
            for (var i = 0; i < type.Count; i++)
            {
                iv_[type[i]] = vl[i];
            }

            iv_["remain"] = remainInvent(iv_);
            return iv_;
        }
        public List<string> strupinventori(Dictionary<string, object> p, List<Dictionary<string, object>> iv, List<string> type, List<string> vl, dynamic houseid)
        {
            Common cmd = new Common();
            List<string> rs = new List<string>();
            string warehouse = p["inventoryhouse"].ToString();
            List<Dictionary<string, object>> warehouses = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(warehouse);
            dynamic w = new System.Dynamic.ExpandoObject();
            try { w = warehouses.FirstOrDefault(s => s["id"].ToString() == houseid["code"].ToString()); } catch { }
            if (warehouses == null || w == null)
            {
                if (warehouses == null)
                    warehouses = new List<Dictionary<string, object>>();
                var n_ = strupinventorinew_(type, vl, houseid);
                warehouses.Add(n_);
                rs.Add(n_["remain"].ToString());
            }
            else
            {
                for (var i = 0; i < type.Count; i++)
                {
                    double qq = 0;
                    qq = cmd.convertNumberDouble(w[type[i]].ToString());
                    qq = qq + cmd.convertNumberDouble(vl[i]);
                    w[type[i]] = qq;
                }
                w["remain"] = remainInvent(w);
                double srm_ = warehouses.Sum(s => cmd.convertNumberDouble(s["remain"].ToString()));
                rs.Add(srm_.ToString());
            }
            rs.Add(JsonConvert.SerializeObject(warehouses));
            return rs;
        }
        public string UpdateinventoriProductWhen_(Dictionary<string, object> p, List<Dictionary<string, object>> iv, List<string> type, List<string> vl, dynamic houseid, string user, List<string> subType, List<string> subVl)
        {
            Common cmd = new Common();
            List<string> cl = new List<string>() { "inventoryhouse", "inventory" };
            cl.AddRange(subType);
            List<string> vlsub = new List<string>();
            List<string> rsInvent = strupinventori(p, iv, type, vl, houseid);
            string inventory = rsInvent[0];
            string warehouse = rsInvent[1];
            vlsub.Add(warehouse);
            vlsub.Add(inventory);
            int bg = 0;
            foreach (var i in subType)
            {
                double old_ = cmd.convertNumberDouble(p[i].ToString());
                double sub_ = cmd.convertNumberDouble(subVl[bg].ToString());
                double newqlt = old_ + sub_;
                vlsub.Add(newqlt.ToString());
                bg++;
            }
            string up = updatesqlOneItemstr("products", cl, vlsub, int.Parse(p["id"].ToString()), user);
            return up;

        }
        public string strUpdatemoneyCtm(Dictionary<string, object> p, string money, string debt, string user)
        {
            Common cmd = new Common();
            List<string> cl = new List<string>() { "totalmoney", "member", "totaldebt" };
            double u = cmd.convertNumberDouble(p["totalmoney"].ToString()) + cmd.convertNumberDouble(money);
            double d = cmd.convertNumberDouble(p["totaldebt"].ToString()) + cmd.convertNumberDouble(debt);
            var mem = finddata2("membercard", "where valuefrom >=" + u + " AND valueto<=" + u + "");
            string m = "-1";
            if (mem.Count > 0)
                m = mem["id"].ToString();
            List<string> vlu = new List<string>() { u.ToString(), m, d.ToString() };
            string str = updatesqlOneItemstr("customers", cl, vlu, int.Parse(p["id"].ToString()), user);
            return str;

        }
        public string insert_htr_item(string table, string id, string newcontain, string oldconten, string user, string action)
        {
            List<string> cl = new List<string>() { "title", "iditem", "tableonenew", "tableoneold", "action" };
            List<string> vl = new List<string>() { table, id.ToString(), newcontain, oldconten, action };
            string str = insertsqlstr("historyitems", cl, vl, user.ToString());
            return str;
        }
        public string updatesqlOneItemstr(string table, List<string> cl, List<string> vls, int id, string user)
        {
            int idex = cl.IndexOf("id");
            if (idex > -1)
            {
                cl.RemoveAt(idex);
                vls.RemoveAt(idex);
            }
            cl.Add("lastupdate"); vls.Add(DateTime.Now.ToString());
            cl.Add("userupdate"); vls.Add(user);
            for (var i = 0; i < vls.Count(); i++)
            {
                if (vls[i] == "" || vls[i] == null)
                {
                    vls[i] = "null";
                }
                else if (vls[i].ToLower() == "true" || vls[i].ToLower() == "false")
                {
                    vls[i] = vls[i];
                }
                else
                {
                    try
                    {
                        if (cl[i] == "phone")
                            vls[i] = "N'" + vls[i] + "'";
                        else
                            int.Parse(vls[i]);
                    }
                    catch
                    {

                        vls[i] = "N'" + vls[i] + "'";
                    }
                }
            }
            string str = "Update " + table;
            str += " Set ";
            List<string> vlpass = new List<string>();
            for (int i = 0; i < cl.Count; i++)
            {
                string a = cl[i] + "=" + vls[i];
                vlpass.Add(a);
            }
            str += string.Join(",", vlpass);
            str += " where id=" + id + "";
            return str;
        }
        public string updatesqlcolumn(string table, List<string> cl, List<string> vls, string where, string user)
        {
            cl.Add("lastupdate"); vls.Add(DateTime.Now.ToString());
            cl.Add("userupdate"); vls.Add(user);
            string str = "Update " + table;
            str += " Set ";
            List<string> vlpass = new List<string>();
            for (int i = 0; i < cl.Count; i++)
            {
                string a = cl[i] + "='" + vls[i] + "'";
                vlpass.Add(a);
            }
            str += string.Join(",", vlpass);
            str += " " + where;
            return str;
        }
        public string insertsqlstr(string table, List<string> cl, List<string> vl, string user)
        {
            cl.Add("createdate"); vl.Add(DateTime.Now.ToString());
            cl.Add("usercreate"); vl.Add(user);
            string str = "INSERT INTO";
            str += " " + table + "(";
            str += string.Join(",", cl);
            str += ") values(";
            for (int i = 0; i < vl.Count; i++)
            {
                if (vl[i] == "" || vl[i] == null)
                {
                    vl[i] = "null";
                }
                else if (vl[i].ToLower() == "true" || vl[i].ToLower() == "false")
                {
                    vl[i] = vl[i];
                }
                else
                {
                    try
                    {
                        int.Parse(vl[i]);
                    }
                    catch
                    {

                        vl[i] = "N'" + vl[i] + "'";
                    }
                }
            }
            str += string.Join(",", vl) + ")";
            return str;
        }
        public Dictionary<string, object> finddata2(string table, string where)
        {
            string str = "select top 1 * from " + table + " " + where;
            Dictionary<string, object> results = new Dictionary<string, object>();
            List<string> namecl = new List<string>();
            MySqlConnection con = new MySqlConnection(cnn);
            con.Open();
            MySqlCommand sqlCommand = new MySqlCommand(str, con);
            MySqlDataReader reader = sqlCommand.ExecuteReader();
            for (var i = 0; i < reader.FieldCount; i++)
                namecl.Add(reader.GetName(i));
            while (reader.Read())
            {

                results = SerializeRow(namecl, reader);
            }
            con.Close();

            return results;
        }
        public Dictionary<string, object> finddata(string str)
        {

            Dictionary<string, object> results = new Dictionary<string, object>();
            List<string> namecl = new List<string>();
            MySqlConnection con = new MySqlConnection(cnn);
            con.Open();
            MySqlCommand sqlCommand = new MySqlCommand(str, con);
            MySqlDataReader reader = sqlCommand.ExecuteReader();
            for (var i = 0; i < reader.FieldCount; i++)
                namecl.Add(reader.GetName(i));
            while (reader.Read())
            {

                results = SerializeRow(namecl, reader);
            }
            con.Close();

            return results;
        }
        public List<Dictionary<string, object>> getdataSQLstr(string str)
        {
            List<Dictionary<string, object>> results = new List<Dictionary<string, object>>();

            List<string> namecl = new List<string>();
            MySqlConnection con = new MySqlConnection(cnn);
            con.Open();
            MySqlCommand sqlCommand = new MySqlCommand(str, con);
            MySqlDataReader reader = sqlCommand.ExecuteReader();
            for (var i = 0; i < reader.FieldCount; i++)
                namecl.Add(reader.GetName(i));
            while (reader.Read())
            {

                results.Add(SerializeRow(namecl, reader));
            }
            con.Close();

            return results;

        }
        public Dictionary<string, object> SerializeRow(IEnumerable<string> cols, MySqlDataReader reader)
        {
            var result = new Dictionary<string, object>();
            foreach (var col in cols)
            {
                try { result.Add(col, reader[col]); } catch { };
            }
            string rs = JsonConvert.SerializeObject(result);
            return result;
        }
        public bool runSQL(string str)
        {
            MySqlConnection conn = new MySqlConnection(cnn);
            conn.Open();
            MySqlCommand cmd = new MySqlCommand(str, conn);
            cmd.ExecuteNonQuery();
            // Execute the command
            conn.Close();
            return true;
        }
        public List<Dictionary<string, object>> getdataSQL(string tb, string where)
        {

            List<Dictionary<string, object>> results = new List<Dictionary<string, object>>();

            List<string> namecl = new List<string>();
            MySqlConnection con = new MySqlConnection(cnn);
            con.Open();
            string str = "SELECT * FROM " + tb + " " + where;
            MySqlCommand sqlCommand = new MySqlCommand(str, con);
            MySqlDataReader reader = sqlCommand.ExecuteReader();
            for (var i = 0; i < reader.FieldCount; i++)
                namecl.Add(reader.GetName(i));
            while (reader.Read())
            {

                results.Add(SerializeRow(namecl, reader));
            }
            con.Close();

            return results;

        }
        public List<Dictionary<string, object>> getdataSQLjoinTable(string str)
        {

            List<Dictionary<string, object>> results = new List<Dictionary<string, object>>();

            List<string> namecl = new List<string>();

            MySqlConnection con = new MySqlConnection(cnn);
            con.Open();
            MySqlCommand sqlCommand = new MySqlCommand(str, con);
            MySqlDataReader reader = sqlCommand.ExecuteReader();
            for (var i = 0; i < reader.FieldCount; i++)
                namecl.Add(reader.GetName(i));
            while (reader.Read())
            {

                results.Add(SerializeRow(namecl, reader));
            }
            con.Close();

            return results;

        }

        public bool checkdataExit(string tb, string where)
        {
            bool rs = true;
            MySqlConnection conn = new MySqlConnection(cnn);
            conn.Open();
            string str = "SELECT id FROM " + tb + " " + where;
            MySqlCommand cmd = new MySqlCommand(str, conn);
            try
            {
                Int32 count = (Int32)cmd.ExecuteScalar();
                if (count > 0)
                    rs = false;
            }
            catch { rs = true; }
            conn.Close();
            return rs;
        }
        public string insert_htr_invoice(string table, string codeid, string newcontainb_, string oldcontenb_, string newdetailb_, string olddetailb_, string user, string action)
        {
            List<string> cl = new List<string>() { "title", "codeid", "tableoneold", "tableonenew", "tabletwoold", "tabletwonew", "action" };
            List<string> vl = new List<string>() { table, codeid, oldcontenb_, newcontainb_, olddetailb_, newdetailb_, action };
            string str = insertsqlstr("histories", cl, vl, user.ToString());
            return str;
        }
    }
}
