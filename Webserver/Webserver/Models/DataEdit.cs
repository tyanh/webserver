﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Webserver.Models
{
    public class DataEdit
    {
        public string code = "";
        public Dictionary<string, object> bill = new Dictionary<string, object>();
        public List<dynamic> data = new List<dynamic>();
    }
}
