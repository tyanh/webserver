﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Webserver.Models
{
    public class StaticObjModel
    {
        public List<Dictionary<string, object>> city { get; set; }
        public List<Dictionary<string, object>> district { get; set; }
        public List<Dictionary<string, object>> ward { get; set; }
        public void InitObj()
        {
            Sqlcommon sql = new Sqlcommon();
            city = sql.getdataSQL("citys", "");
            district = sql.getdataSQL("distrists", "");
            ward = sql.getdataSQL("wards", "");
        }
        public string searchname(string code,string obj)
        {
            try
            {
                string name = "";
                if(obj=="city")
                    name = city.FirstOrDefault(s=>s["code"].ToString() == code)["name_"].ToString();
                else if (obj == "district")
                    name = district.FirstOrDefault(s => s["code"].ToString() == code)["name_"].ToString();
                else if (obj == "ward" && code!="")
                    name = ward.FirstOrDefault(s => s["code"].ToString() == code)["name_"].ToString();
                return name;
            }
            catch
            {
                return code;
            }
        }
    }
}
