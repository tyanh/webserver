﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Webserver.Models
{
    public class WorkerModel
    {
        public int id { get; set; }
        public Dictionary<string, object> permission { get; set; }
        public string privilege { get; set; }
        public string fullname { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public bool superadmin { get; set; }
        public bool delete { get; set; }
        public void getPermission(int idwk)
        {
            Sqlcommon sql = new Sqlcommon();
            permission = sql.finddata2("workers", "where id=" + idwk + "");
        }
    }
}
