﻿using DocumentFormat.OpenXml.EMMA;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Configuration;
using MySqlX.XDevAPI;
using Newtonsoft.Json;
using Org.BouncyCastle.Asn1.Cms;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using Ubiety.Dns.Core;
using Webserver.Controllers;

namespace Webserver.Models
{

    public class Common
    {

        public string[] actionIV = new string[] { "import", "export", "sale", "inven" };
        public string[] calIV = new string[] { "+", "-", "+", "sum"};
        public string[] actionTranf = new string[] { "tranfer", "moneytranfer", "tranferrcv", "moneytranferrcv","remove", "moneyremove" };
        public string pathWeb=Directory.GetCurrentDirectory().ToString();
        Sqlcommon sql = new Sqlcommon();
        public string hostimg;
        public Common()
        {
            var configurationBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path, false);
            var root = configurationBuilder.Build();
            hostimg = root.GetSection("AppSettings").GetSection("FolderImg").Value;
 
        }
        public int CompareTime(Dictionary<string, object> obj,string time)
        {
            int rs = -1;
            try {
                string daytime = obj["datebill"].ToString();
                var t_ = time.Split('-');
                var t1 = DateTime.Parse(daytime).TimeOfDay;
                var t2 = DateTime.Parse(t_[0] + ":00").TimeOfDay;
                var t3 = DateTime.Parse(t_[1] + ":00").TimeOfDay;
                var c=TimeSpan.Compare(t1, t2);
                var d = TimeSpan.Compare(t1, t3);
                if ((c == 1 || c == 0) && (d == -1 || d == 0))
                    rs = 0;
            }
            catch
            {
               
            }
            return rs;
        }
        public int randoomsecond()
        {
            Random rnd = new Random();
            int num = rnd.Next(3, 10)*1000;
            return num;
        }
        public string convertDatetoString(string d)
        {
            try
            {
                return DateTime.Parse(d).ToString("dd-MM-yyyy");
            }
            catch
            {
                return "";
            }
        }
        public string calmoneygift(dynamic gift,double vl)
        {
            try
            {
                var vlg = gift.value.ToString();
                if (vlg == "0")
                    return "0";
                if (gift.unit.ToString() == "%")
                {
                    var o = (vl * (convertNumberDouble(vlg))) / 100;
                    return o.ToString();
                }
                return vlg;
            }
            catch
            {
                return "0";
            }
        }
        public dynamic newDynamicObj(List<string> column, List<string> value)
        {
            dynamic o = new System.Dynamic.ExpandoObject();
            for(var i=0;i<column.Count;i++)
            {
                o.cl =column[i];
                o.vl= value[i].ToString();
            }
            return o;
        }
        public string GetGiftCode(string code)
        {
            Sqlcommon sql = new Sqlcommon();
            string strgif = "select codes,promotion.* from giftcode left join promotion on giftcode.codepmt=promotion.codeid where giftcode.codes='" + code + "' AND giftcode.status<>1 AND promotion.status<>1";
            var lgift = sql.getdataSQLstr(strgif);
            if (lgift.Count <= 0)
                return "-1";
            else
            {
                double max = -1;
                var obj = lgift[0];
                double qltuser = convertNumberDouble(obj["qltuser"].ToString());
                if (obj["maxqlt"].ToString() != "")
                    max = int.Parse(obj["maxqlt"].ToString());
                if (qltuser >= max)
                    return "1";
                var now = DateTime.Now;
                var start = DateTime.Parse(obj["startday"].ToString());
                var end = DateTime.Parse(obj["endday"].ToString());
                if (now < start || start > end.AddDays(1))
                    return "2";
            }
            return JsonConvert.SerializeObject(lgift);
        }
        public bool eidtvalueInvoice(dynamic obj,string cl,string vl)
        {
            obj[cl]= vl;
           return true;
        }
        public string RemoveUnicode(string text)
        {
            string[] arr1 = new string[] { "á", "à", "ả", "ã", "ạ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ",
            "đ",
            "é","è","ẻ","ẽ","ẹ","ê","ế","ề","ể","ễ","ệ",
            "í","ì","ỉ","ĩ","ị",
            "ó","ò","ỏ","õ","ọ","ô","ố","ồ","ổ","ỗ","ộ","ơ","ớ","ờ","ở","ỡ","ợ",
            "ú","ù","ủ","ũ","ụ","ư","ứ","ừ","ử","ữ","ự",
            "ý","ỳ","ỷ","ỹ","ỵ",};
                    string[] arr2 = new string[] { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
            "d",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",};
            for (int i = 0; i < arr1.Length; i++)
            {
                text = text.Replace(arr1[i], arr2[i]);
                text = text.Replace(arr1[i].ToUpper(), arr2[i].ToUpper());
            }
            return text;
        }
        public Boolean createFolder(string n)
        {

            if (!Directory.Exists(n))
            {
                Directory.CreateDirectory(n);
            }
            return true;
        }
        public Bitmap GetImgBitmap(string str)
        {
            string convert = str.Replace("data:image/png;base64,", String.Empty);
            convert = convert.Replace("data:image/jpeg;base64,", String.Empty);
            convert = convert.Replace(" ", "+");
            using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(convert)))
            {
                float width = 1024;
                float height = 768;

                using (Bitmap bm2 = new Bitmap(ms))
                {

                    //float scale = Math.Min(width / bm2.Width, height / bm2.Height);
                    //var scaleWidth = (int)(bm2.Width * scale);
                    //var scaleHeight = (int)(bm2.Height * scale);
                    Bitmap bitmap = new Bitmap(bm2, new Size(bm2.Width, bm2.Height));
                    return bitmap;
                }
            }

            //return name + "." + ext;
        }
        public string GetImg(string str, string path,string name,string ext)
        {
            string convert = str.Replace("data:image/png;base64,", String.Empty);
            convert = convert.Replace("data:image/jpeg;base64,", String.Empty);
            convert = convert.Replace(" ", "+");
            using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(convert)))
            {
                float width = 1024;
                float height = 768;

                using (Bitmap bm2 = new Bitmap(ms))
                {

                    float scale = Math.Min(width / bm2.Width, height / bm2.Height);
                    var scaleWidth = (int)(bm2.Width * scale);
                    var scaleHeight = (int)(bm2.Height * scale);
                    Bitmap bitmap = new Bitmap(bm2, new Size(scaleWidth, scaleHeight));
                    bitmap.Save(path+"/" + name + "."+ ext);
                }
            }

            return name + "." + ext;
        }
        public Image watermarkBarcodeonImage(string txt, Bitmap bm, string FolderSource_, string fname)
        {
            string watermarkText = txt;
            Image myimg = Code128Rendering.MakeBarcodeImage(watermarkText, 1, true);
            Image file = (Image)bm;
            using (Graphics grp = Graphics.FromImage(file))
            {
                Point position = new Point(0, 0);
                if(txt!="")
                grp.DrawImage(myimg, position);
                Brush brush = new SolidBrush(Color.Gray);
                Font font = new Font("UTM Dax", 16, GraphicsUnit.Point);
           
                Point positiontext = new Point(10, 50);
                grp.DrawString(watermarkText, font, brush, positiontext);
                return file;

            }

        }

        public Image watermarkStronImage(string txt, Bitmap bm, string FolderSource_, string fname)
        {
            string watermarkText = txt;
           
            Image file = (Image)bm;
            using (Graphics grp = Graphics.FromImage(file))
            {
                grp.TranslateTransform(file.Width / 2, file.Height / 2);
                grp.RotateTransform(270);

                Brush brush = new SolidBrush(Color.Gray);
                Font font = new Font("UTM Dax", 40, GraphicsUnit.Point);
                //SizeF textSize = grp.MeasureString(watermarkText, font);
                int x = ((file.Height / 2)) * -1;
                int y = (file.Width / 2) - 60;
                Point position = new Point(x, y);
                grp.DrawString(watermarkText, font, brush, position);
                return file;
              
            }

        }

        public bool ismobile(string userAgent) {
            string[] mobileDevices = new string[] {"iphone","ppc",
                                                   "windows ce","blackberry",
                                                   "opera mini","mobile","palm",
                                                   "portable","opera mobi" };
      
            var mb = mobileDevices.Any(x => userAgent.Contains(x));
            return mb;
        }
        public string strinventory_input(string inventoryhouse, string inventory,int id,string user,string qltinput,string moneyinput)
        {
            List<string> cl = new List<string>() { "inventoryhouse", "inventory", "qltinput", "moneyinput" };
            List<string> vl = new List<string>() { inventoryhouse, inventory, qltinput, moneyinput };
            string str = sql.updatesqlOneItemstr("products", cl, vl, id, user);
            return str;
        }
        public string Edit_Inventory_json(string inventoryhouse, List<string> cl, List<string> vl,dynamic wh)
        {
            var w = JsonConvert.DeserializeObject<List<dynamic>>(inventoryhouse);
            if (w == null)
                w = new List<dynamic>();
            var i = w.FirstOrDefault(s => s["id"] == wh.id);
            if (i == null)
            {
                i = new System.Dynamic.ExpandoObject();
                for(var j=0;j<cl.Count;j++)
                {
                    i[cl[j]] = vl[j];
                }
                w.Add(i);
            }
            else
            {
                for (var j = 0; j < cl.Count; j++)
                {
                    double vl_ = 0;
                    try
                    {
                        vl_ = convertNumberDouble(i[cl[j]].ToString());
                    }
                    catch
                    {

                    }
                    i[cl[j]] =convertNumberDouble(vl[j])+vl_;
                }
            }
            return JsonConvert.SerializeObject(w);
        }
        public bool isnumber(string v)
        {
            try
            {
                var i = double.Parse(v);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public double calculator(double vl1, double vl2,string cal)
        {
            double rs = 0;
            if (cal == "+")
                rs = vl1 + vl2;
            else if (cal == "-")
                rs = vl1 - vl2;
            else if (cal == "x")
                rs = vl1 * vl2;
            else if (cal == ":")
                rs = vl1 / vl2;
            return rs;
        }
        public double calcuremainProduct(Dictionary<string, object> o)
        {
            double rs = 0;
            for(var i=0;i< calIV.Length; i++)
            {
                try
                {
                    double a = convertNumberDouble(o[actionIV[i]].ToString());
                 
                    if (calIV[i] == "+")
                        rs = rs + a;
                    else if (calIV[i] == "-")
                        rs = rs - a;
                }
                catch { }
            }
            return rs;
        }
        public Dictionary<string, object> dnmInven(Dictionary<string, object> hourse, Dictionary<string, object> product, string quality, string type,List<string> subtype, List<double> subvl)
        {
            Dictionary<string, object> a_rt = new Dictionary<string, object>();
            a_rt["warehouse"] = hourse;
            a_rt["warehouseId"] = hourse["code"].ToString();
            a_rt["product"] = product;
            a_rt["idproduct"] = product["id"].ToString();
            a_rt[type] = quality;
            for (var i = 0; i < subtype.Count; i++)
            {
                a_rt[subtype[i]] = subvl[i];
            }
            return a_rt;
        }
        public string strNotdelete()
        {
            return "status<>1 OR status is null";
        }
        public string strselect(string table,string where)
        {
            if (where != "")
                where = where + " AND ";
            else
                where = "where";
            return "select '"+table+"' as site, * from "+table+" "+where+"(status<>1 OR status is null)";
        }
        public string fotmatmoney(string o)
        {
            try
            {
                if (o == "" || o == "0")
                    return "0";
                else
                {
                    if (double.Parse(o) < 10)
                        return o;
                    return String.Format("{0:0,0}", double.Parse(o));
                }
            }
            catch
            {
                return "0";
            }
        }
        public bool checkrightwarehouse(string user,string idwh)
        {
            Sqlcommon sql = new Sqlcommon();
            var w = sql.finddata2("warehouse", "where id=" + idwh + " AND status<>1");
            if (w.Count <= 0)
                return false;
            string[] wh = w["usermanager"].ToString().Split(',');
            if (wh.ToList().IndexOf(user) == -1)
                return false;
            return true;
        }
        public Dictionary<string, object> getUserLogin(string session)
        {
            Sqlcommon sql = new Sqlcommon();
            var w = sql.finddata2("workers", "where session='"+session+ "' AND login=1 AND status<>1");
            return w;
        }
        public string rightUserView(Dictionary<string, object> w,string url)
        {
            string rs = "";
            var menus = sql.finddata2("menus", "where link='"+url+"' AND status=0");
            if (menus.Count > 0)
            {
                if (w["superadmin"].ToString() != "1")
                {

                    List<dynamic> L_pms = JsonConvert.DeserializeObject<List<dynamic>>(w["privilege"].ToString());
                    var L_name_per = L_pms.FirstOrDefault(s => s.name.ToString()==menus["id"].ToString());
                    if(L_name_per!=null)
                    {
                        if (L_name_per.view == "" && L_name_per.all == "")
                            rs = "";
                        else
                            rs = JsonConvert.SerializeObject(L_name_per);
                    }
                }
            }
            return rs;
        }
        public List<Dictionary<string, object>> getwarehousesale(string user,int issupper)
        {
            Sqlcommon sql = new Sqlcommon();
            var w = sql.getdataSQL("warehouse", "where issale=1 AND status<>1");
            if (issupper != 1)
            {
                List<string> rm = new List<string>();
                foreach (var i in w)
                {
                    string[] wh = i["usermanager"].ToString().Split(',');
                    if (wh.ToList().IndexOf(user) == -1)
                        rm.Add(i["id"].ToString());
                }
                w = w.Where(s => rm.IndexOf(s["id"].ToString()) == -1).ToList();
            }
            return w;
        }
        public List<Dictionary<string, object>> getwarehouse(string user)
        {
            Sqlcommon sql = new Sqlcommon();
            var w = sql.getdataSQL("warehouse", "where status<>1");
            List<string> rm = new List<string>();
            foreach (var i in w)
            {
                string[] wh = i["usermanager"].ToString().Split(',');
                if (wh.ToList().IndexOf(user) == -1)
                    rm.Add(i["id"].ToString());
            }
            w = w.Where(s => rm.IndexOf(s["id"].ToString()) == -1).ToList();
             
            return w;
        }
        public  bool ispermission(string idtb,string act, LoginModel login)
        {
            
            if (login == null)
                return false;
             if (login.admin.ToString() != "1")
            {
                List<dynamic> per = JsonConvert.DeserializeObject<List<dynamic>>(login.permistion);
                var obj = per.FirstOrDefault(s => s["obj"].ToString() == idtb);
                var c = obj[act];
                if (c != "checked" && obj["all"] != "checked")
                {
                    
                    return false;
                }
             
            }
            return true;
        }
        public bool IsMD5(string input)
        {
            if (String.IsNullOrEmpty(input))
            {
                return false;
            }

            return Regex.IsMatch(input, "^[0-9a-fA-F]{32}$", RegexOptions.Compiled);
        }
        public string MD5(string o)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(o);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
        public string convertDate(string o)
        {
            if (o =="")
                return o;
            o = o.Replace("/", "-");
            string[] a = o.Split('-');
            return a[1] + "/" + a[0] + "/" + a[2];
        }
        public string convertDateD_M_Y(string o)
        {
            if (o == "")
                return o;
            return DateTime.Parse(o).ToString("dd-MM-yyyy hh:mm");
        }
        public string convertNumber(string a)
        {
         
            return a.Replace(",", "");
        }
       
        public string convertNumberToString(string a)
        {
            string rs = a;
            try { 
                double cv = double.Parse(a);
                if(cv>=1000)
                    rs=string.Format("{0:0,0}", cv);
            }
            catch
            {
                rs = a;
            }
            return rs;
        }
        public string updateInventory(Dictionary<string,object> Item,dynamic detail, List<dynamic> inforbill,string[] actioniven, string actionwh,string table,string userid)
        {
            List<string> clupdate = new List<string>();
            List<string> vlupdate = new List<string>();
            foreach (var a in actioniven)
            {
                if (a == "")
                    continue;
                var aa = a.Split('#');
                foreach (var a_ in aa)
                {
                    var aa_ = a.Split('/');
                    var cal = aa_[1];
                    var vlnew = detail[aa_[0]].ToString();
                    var clold = aa_[2].Split('&');
                    foreach (var old in clold)
                    {
                        var vlold = Item[old].ToString();
                        var vlup = calculator(convertNumberDouble(vlold), convertNumberDouble(vlnew), cal);
                        Item[old] = vlup;
                        clupdate.Add(old);
                        vlupdate.Add(vlup.ToString());
                    }
                   
                }
            }
            List<Dictionary<string, object>> lw = new List<Dictionary<string, object>>();
            lw = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(Item["inventdetail"].ToString());
            clupdate.Add("inventdetail");
            var inventwh = actionwh.Split('#');
            foreach(var i in inventwh)
            {
                if (i == "")
                    continue;
                var aw = i.Split('&');
                var clidwh = aw[0].Split('/')[1];
                Dictionary<string, object> w = new Dictionary<string, object>();
                w["idwhouse"] = inforbill.FirstOrDefault(s => s.cl.ToString() == clidwh).vl.ToString(); ;
                w["name"] = "";
                foreach (var aw_ in aw)
                {
                    var aa = aw_.Split('/');
                    var vlnew = detail[aa[0]].ToString();
                    w[aa[3]] =vlnew;
                }
                if (lw == null)
                {
                    lw = new List<Dictionary<string, object>>();
                    lw.Add(w);
                }
                else
                {
                    var items = lw.FirstOrDefault(s => s["idwhouse"].ToString() == w["idwhouse"].ToString());
                    if (items == null)
                        lw.Add(w);
                    else
                    {
                        foreach (var aw_ in aw)
                        {
                            var aa = aw_.Split('/');
                            try
                            {
                                var vlold = items[aa[3]].ToString();
                                items[aa[3]] = calculator(convertNumberDouble(vlold), convertNumberDouble(w[aa[3]].ToString()), aa[2].ToString());
                            }
                            catch
                            {
                                items[aa[3]] = calculator(0, convertNumberDouble(w[aa[3]].ToString()), aa[2].ToString());
                            }
                        }
                            
                    }
                }

            }
            vlupdate.Add(JsonConvert.SerializeObject(lw));

            return sql.updatesqlcolumn(table,clupdate,vlupdate,"where id="+Item["id"]+"", userid);
        }
       
        public double convertNumberDouble(string a)
        {
            if (a == "")
                return 0;
            return double.Parse(a.Replace(",", ""));
        }
        public List<string> CreateListValue(dynamic o,List<string> cl)
        {
            List<string> vl = new List<string>();
            foreach(var i in cl)
            {
                var v = o[i].ToString();
                vl.Add(v);
            }
            return vl;
        }
        public string CreateDictionary(List<string> cl,List<string> vl)
        {
            var a = new Dictionary<string, object>();
            for(var i=0;i<cl.Count;i++)
            {
                a[cl[i]] = vl[i];
            }
            return JsonConvert.SerializeObject(a);
        }
        public string action_htr(string o)
        {
            List<string> id = new List<string>() {"1" };
            List<string> vl = new List<string>() { "Edit khách hàng" };
            return vl[id.IndexOf(o)];
        }
        public string decodeHtml(string o)
        {
            return HttpUtility.HtmlDecode(o);
        }
        public string encodeHtml(string o)
        {
            return HttpUtility.HtmlEncode(o);
        }
        public string CreatCode(string ext)
        {
            string g = Guid.NewGuid().ToString().Replace("-","");
            return ext + g;

        }
       
        public string CreatNumberBill(string table)
        {
            Sqlcommon sql = new Sqlcommon();
            string str = (sql.countId(table,"")+1).ToString();
            if(str.Length<6)
            {
                int b = 6 - str.Length;
                for (var i=0;i<b;i++)
                {
                    str = "0" + str;
                }
            }
          
            return str;

        }
    }
    
}
