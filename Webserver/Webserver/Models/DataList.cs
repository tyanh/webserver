﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Webserver.Models
{
    public class DataList
    {
        public List<Dictionary<string, object>> data { get; set; }
        public string key { get; set; }
        public string fromday { get; set; }
        public string today { get; set; }
        public string count { get; set; }
        public List<dynamic> sumdata { get; set; }
    }
}
