﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Webserver.Models
{
   
    public class ChatServer
    {
        public string cnnchat;
        Sqlcommon sql = new Sqlcommon();
        //  private readonly Common cmdcommon = new Common();
        public ChatServer()
        {
            var configurationBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path, false);
            var root = configurationBuilder.Build();
            cnnchat = root.GetSection("ConnectionStrings").GetSection("connectchat").Value;

        }
        public Dictionary<string, object> SerializeRow(IEnumerable<string> cols, SqlDataReader reader)
        {
            var result = new Dictionary<string, object>();
            foreach (var col in cols)
            {
                try { result.Add(col, reader[col]); } catch { };
            }
            string rs = JsonConvert.SerializeObject(result);
            return result;
        }
        public bool runSQL(string str)
        {
            SqlConnection conn = new SqlConnection(cnnchat);
            conn.Open();
            SqlCommand cmd = new SqlCommand(str, conn);
            cmd.ExecuteNonQuery();
            // Execute the command
            conn.Close();
            return true;
        }
        public bool checkdataExit(string tb, string where)
        {
            bool rs = true;
            SqlConnection conn = new SqlConnection(cnnchat);
            conn.Open();
            string str = "SELECT id FROM " + tb + " " + where;
            SqlCommand cmd = new SqlCommand(str, conn);
            try
            {
                Int64 count = (Int64)cmd.ExecuteScalar();
                if (count > 0)
                    rs = false;
            }
             catch { rs = true; }
            conn.Close();
            return rs;
        }
        public string JoinGroup(string data)
        {
            string str = "";
            dynamic dt = JsonConvert.DeserializeObject<dynamic>(data);
            List<string> cl = new List<string>() { "name", "groupchat", "idclient", "ipclient", "status", "iduser" };
            List<string> vl = new List<string>() { dt.name.ToString(), dt.groupchat.ToString(), dt.idclient.ToString(), dt.ipclient.ToString(),"0", dt.iduser.ToString() };
            if (checkdataExit("loginserver", "where ipclient='" + dt.ipclient.ToString() + "'"))
            {
                str = sql.insertsqlstr("loginserver", cl, vl, "1");
              
            }
            else
            {
                str = "UPDATE loginserver SET idclient = '"+ dt.idclient.ToString() + "',ipclient = '" + dt.ipclient.ToString() + "',status = '0' WHERE iduser='" + dt.iduser.ToString() + "'";
            }
            runSQL(str).ToString();
            return "";
        }
        public string outGroup(string iduser)
        {
            List<string> cl = new List<string>() {"status" };
            List<string> vl = new List<string>() { "1" };
            string now = DateTime.Now.ToShortDateString();
            string insert = "UPDATE loginserver SET status = -1,lastupdate='"+now+"' WHERE idclient='"+ iduser + "'";
            return runSQL(insert).ToString();
        }
        public List<Dictionary<string, object>> getdataSQL(string tb, string where)
        {

            List<Dictionary<string, object>> results = new List<Dictionary<string, object>>();

            List<string> namecl = new List<string>();
            SqlConnection con = new SqlConnection(cnnchat);
            con.Open();
            string str = "SELECT * FROM " + tb + " " + where;
            SqlCommand sqlCommand = new SqlCommand(str, con);
            SqlDataReader reader = sqlCommand.ExecuteReader();
            for (var i = 0; i < reader.FieldCount; i++)
                namecl.Add(reader.GetName(i));
            while (reader.Read())
            {

                results.Add(SerializeRow(namecl, reader));
            }
            con.Close();

            return results;

        }
    }
}
