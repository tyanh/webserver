﻿using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Webserver.Models
{
    public class ReportModel
    {
        public string datefrom = "";
        public string dateto = "";
        public List<Dictionary<string,object>> sale { get; set; }
        public List<Dictionary<string, object>> detailsale { get; set; }
        public List<Dictionary<string, object>> detailbilltranfer { get; set; }
        public List<Dictionary<string, object>> detailbillreturn { get; set; }
        public List<Dictionary<string, object>> detailbillimport { get; set; }
        public List<Dictionary<string, object>> detailbillexport { get; set; }
        public List<Dictionary<string, object>> inventory { get; set; }
        public List<Dictionary<string, object>> return_ { get; set; }
        public List<Dictionary<string, object>> import { get; set; }
        public List<Dictionary<string, object>> export { get; set; }
        public List<Dictionary<string, object>> recei { get; set; }
        public List<Dictionary<string, object>> pay { get; set; }
        public List<Dictionary<string, object>> warehouse { get; set; }
        public List<Dictionary<string, object>> catelogy { get; set; }
        public List<Dictionary<string, object>> product { get; set; }
        public void initdata(string datefrom,string dateto, string wh)
        {
            Sqlcommon sql = new Sqlcommon();
            Common cmd = new Common();
            warehouse = sql.getdataSQL("warehouse", "");
            if (datefrom == "")
                return;
            this.datefrom = datefrom;
            this.dateto = dateto;
            string dfrom = cmd.convertDate(datefrom);
            string dto = "";
            try { dto=DateTime.Parse(cmd.convertDate(dateto)).AddDays(1).ToShortDateString(); } catch { };
            string w = "where datebill>='"+ dfrom + "' AND datebill<'" + dto + "'";
            catelogy = sql.getdataSQL("category", "");
            product= sql.getdataSQL("products", "");
            sale = sql.getdataSQL("billsale","");
            detailsale = sql.getdataSQL("detailbillsale", w);
            detailbillexport = sql.getdataSQL("detailbillsale", w);
            detailbillimport = sql.getdataSQL("detailbillimport", w);
            detailbillreturn = sql.getdataSQL("detailbillreturn", w);
            detailbilltranfer = sql.getdataSQL("detailbilltranfer", w);
            return_ = sql.getdataSQL("billreturn", "");
            import = sql.getdataSQL("billimport", "");
            export = sql.getdataSQL("billexport", "");
            recei = sql.getdataSQL("receipts", "");
            pay = sql.getdataSQL("paymentbill", "");
        }
        public string inventorys(string idcata,string warehouse)
        {
            var iditem = product.Where(s => s["category"].ToString().Split(',').IndexOf(idcata) > -1).Select(s => s["id"].ToString()).ToList();
            var sumsale = detailsale.Where(s => iditem.Contains(s["iditem"].ToString()) && s["warehouse"].ToString()== warehouse).Sum(s=>double.Parse(s["qlt"].ToString()));
            var sumexport = detailbillexport.Where(s => iditem.Contains(s["iditem"].ToString()) && s["warehouse"].ToString() == warehouse).Sum(s => double.Parse(s["qlt"].ToString()));
            var sumimport = detailbillimport.Where(s => iditem.Contains(s["iditem"].ToString()) && s["warehouse"].ToString() == warehouse).Sum(s => double.Parse(s["qlt"].ToString()));
            var sumreturn = detailbillreturn.Where(s => iditem.Contains(s["iditem"].ToString()) && s["warehouse"].ToString() == warehouse).Sum(s => double.Parse(s["qlt"].ToString()));
            var sumtranferimport = detailbilltranfer.Where(s =>s["warehouseimport"].ToString()==warehouse && iditem.Contains(s["iditem"].ToString()) && s["warehouse"].ToString() == warehouse).Sum(s => double.Parse(s["qlt"].ToString()));
            var inv = (sumimport + sumreturn + sumtranferimport) - (sumexport+ sumsale);
            return inv.ToString();
        }
    }
}
