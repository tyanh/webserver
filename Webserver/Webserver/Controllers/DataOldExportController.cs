﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Webdll.Models;
using Webserver.Models;

namespace Webserver.Controllers
{
    public class DataOldExportController : Controller
    {
        SqlDataOld sql = new SqlDataOld();
        Common cmd = new Common();
        Sqlcommon sqln = new Sqlcommon();
        string hostimold = "https://new.monaco.vn/";
        IXLRow header;
        commondll dll = new commondll();
        public IActionResult Index()
        {
            var tb = sql.GetTableNames();
            tb= tb.OrderBy(q => q).ToList();
            return View(tb);
        }
        int indexCell(string o)
        {
            int total = header.CellCount();
            for (var i=1;i<= total; i++)
            {
                if (header.Cell(i).Value.ToString() == o)
                    return i;
            }
            return -1;
        }

        string getvalue(List<IXLRow> row,string n)
        {
            try
            {
                string vl = row.Select(s => s.Cell(indexCell(n)).Value.ToString()).FirstOrDefault();
                return vl;
            }
            catch
            {
                return "";
            }
        }
        string getvaluesinglerow(IXLRow row, string n)
        {
            string vl = row.Cell(indexCell(n)).Value.ToString();
            return vl;
        }
        bool upbillsale(List<dynamic> cls, string table, string user, List<IXLRow> ws2)
        {
            Sqlcommon sqlnew = new Sqlcommon();
            var ws3 = ws2.Skip(1).GroupBy(s => s.Cell(1).Value).ToList();
            List<string> sqls = new List<string>();
            var ctms = sqln.getdataSQL("customers","");
            foreach (var j in ws3)
            {
                if (j.Key.ToString() == "-1" || j.Key.ToString() == "")
                    continue;
                var y = j;
                var ctm = ctms.FirstOrDefault(s => s["oldid"].ToString() == y.Key.ToString());
                var money = y.Sum(s => cmd.convertNumberDouble(s.Cell(3).Value.ToString()));
                var disc = y.Sum(s => cmd.convertNumberDouble(s.Cell(9).Value.ToString()));
                var cl_ = new List<string>() { "moneysale", "moneydiscount"};
                var vl_ = new List<string>() { money.ToString(), disc.ToString() };
                string str = sqln.updatesqlOneItemstr("customers", cl_, vl_, int.Parse(ctm["id"].ToString()), user);
                sqls.Add(str);
            }
            sqln.runSQL(string.Join(";", sqls));
            return true;
        }
            bool upcustomer(List<dynamic> cls, string table, string user, List<IXLRow> ws2)
        {
            Sqlcommon sqlnew = new Sqlcommon();
            var ws3 = ws2.Skip(1).GroupBy(s => s.Cell(3).Value).ToList();
            // var citys_ = sql.getdataSQL("citys", "");
            // var districts_ = sql.getdataSQL("distrists", "");
            // var wards_ = sql.getdataSQL("wards", "");
            List<string> sqls = new List<string>();
            foreach (var j in ws3)
            {
                
                var y = j;
                
                string address = getvalue(y.ToList(), "diachi");
                string birthday =getvalue(y.ToList(), "ngaysinh");
                string email = getvalue(y.ToList(), "email");
                string genders = "3";
                if(getvalue(y.ToList(), "gioitinh").ToLower()=="nữ")
                    genders = "2";
                if (getvalue(y.ToList(), "gioitinh").ToLower() == "nam")
                    genders = "1";
                string name_ = getvalue(y.ToList(), "tenkh");
                string password =cmd.MD5(getvalue(y.ToList(), "matkhau"));
                string phone =y.Key.ToString();
                string typectm = "";

                string username = getvalue(y.ToList(), "username");
                string oldid = getvalue(y.ToList(), "id");
                List<string> cl_ = new List<string>() { "address", "birthday", "email", "genders", "name_", "password", "phone", "typectm", "username", "oldid" };
                List<string> vl_ = new List<string>() { address, birthday,email,genders,name_, password, phone, typectm, username, oldid };
                string s_ = sqln.insertsqlstr("customers", cl_, vl_, user);
                sqls.Add(s_);
            }
            sqln.runSQL(string.Join(";",sqls));
            return true;
        }
        bool upproduct(List<dynamic> cls, string table, string user, List<IXLRow> ws2)
        {
            dynamic iinfor = JsonConvert.DeserializeObject<dynamic>(table);
            var indexname = indexCell("tensp");
            var ws3 = ws2.Skip(1).GroupBy(s => s.Cell(3).Value).ToList();
           
          
            Sqlcommon sqlnew = new Sqlcommon();
            var  news = cls.Select(s => s["clnew"].ToString()).ToList();
            var olds = cls.Select(s => s["old"].ToString()).ToList();
            var lhang = sqlnew.getdataSQL("category", "");
            var lbrand = sqlnew.getdataSQL("brands", "");
            var vlum = sqlnew.getdataSQL("size", "");
            var origins = sqlnew.getdataSQL("origin", "");
            var nameenglish = sqlnew.getdataSQL("nameenglish", "");
            var pros = sqln.getdataSQL("descproduct", "");
            foreach (var j in ws3)
            {
                var pp = pros.FirstOrDefault(s => s["name_"].ToString() == j.Key.ToString());
                if (pp != null)
                    continue;
                List<string> sqls = new List<string>();
                var y = j;

                List<string> clip = new List<string>() { "category", "seoname", "origin","brandcode", "genders", "nameEn","poster", "descrip", "name_", "photo", "years", "youtube" };
                List<string> indexvl = new List<string>() {"mah","seoname", "origin","nhanhieu", "sex", "mota", "poster", "gioithieu", "tensp", "hinhanh", "namsx", "video" };
                List<string> vls = new List<string>();
              
                string img = getvalue(y.ToList(), "hinhanh");
                string poster = getvalue(y.ToList(), "poster");
                if (poster != null && poster!="")
                {
                    string sublink = poster.Substring(0, 1);
                    if (sublink == "\\")
                    {
                        poster = poster.Remove(0, 2);
                    }
                }
                string code = cmd.CreatCode("desc");
                string brans_ = getvalue(y.ToList(), "nhanhieu");
                string nameimg = cmd.RemoveUnicode(j.Key.ToString()).Replace(" ", "_").Replace("&", "_");
                string branden = cmd.RemoveUnicode(brans_).Replace(" ","_").Replace("&", "_");
                string pathImg = Path.Combine(@"D:\buildserver\wwwroot\imgWeb\san_pham\media-72020\");
                string folders = "";
                if (img!="")
                    dll.downloadfile(hostimold+ img, nameimg + ".jpg", pathImg, folders,false, code);
                if (poster != "")
                    dll.downloadfile(hostimold + poster,"poster_"+ nameimg + ".jpg", pathImg, folders, false, code);
                for (var n = 0; n < indexvl.Count; n++)
                {
             
                    string clips = clip[n].ToString();
                    string vl = getvalue(y.ToList(), indexvl[n].ToString());
                    if (clips== "brandcode")
                    {
                        try
                        {
                            vl = lbrand.FirstOrDefault(s => s["name_"].ToString() == System.Web.HttpUtility.HtmlEncode(vl))["code"].ToString();
                        }
                        catch
                        {
                            vl = "";
                        }
                    }
                    else if (clips == "category")
                    {
                        vl = "1";
                    }
                    else if (clips == "nameEn")
                    {
                        var objvl = nameenglish.FirstOrDefault(s => s["name_"].ToString() == System.Web.HttpUtility.HtmlEncode(vl.ToLower()));
                        if (objvl == null)
                            vl = "";
                        else
                            vl = objvl["id"].ToString();

                    }
                    else if (clips == "seoname")
                    {
                        vl =cmd.RemoveUnicode(getvalue(y.ToList(),"tensp")).Replace(" ","-");
                    }
                    else if (clips == "photo")
                    {
                        vl = "san_pham/media-72020/" + nameimg + ".jpg";
                    }
                    else if (clips == "poster")
                    {
                        if(vl!="")
                            vl = "san_pham/media-72020/poster_" + nameimg + ".jpg";
                    }
                    else if (clips == "origin")
                    {
                        if (vl != "")
                        {
                            var org = origins.FirstOrDefault(s => s["name_"].ToString() == vl.ToLower());
                            if (org != null)
                                vl = org["code"].ToString();
                            else
                                vl = "";
                         }
                    }
                            
                    vls.Add(vl);
                }
                clip.Add("subtab");
                List<dynamic> subtab = new List<dynamic>();
                List<string> lstab = new List<string>() { "hương đầu","hương giữa","hương cuối"};
                List<string> lstabex = new List<string>() { "huongdau", "huonggiua", "huongcuoi" };
                for(var st=0;st< lstab.Count;st++)
                {
                    dynamic m = new System.Dynamic.ExpandoObject();
                    m.name = lstab[st];
                    m.vl= getvalue(y.ToList(), lstabex[st]);
                    subtab.Add(m);
                }
                vls.Add(JsonConvert.SerializeObject(subtab));

                clip.Add("code");
                vls.Add(code);
                string insert = sqlnew.insertsqlstr("descproduct", clip, vls, user);
                sqls.Add(insert);
                for (var c = 0; c < y.Count(); c++)
                {
                    List<string> listtype = new List<string>() { "1" };
                    var item = y.ElementAt(c);
                    string banchay = getvaluesinglerow(item, "banchay");
                    string sieusang = getvaluesinglerow(item, "sieusang");
                    string kmai = getvaluesinglerow(item, "kmai"); 

                    string typesp = getvaluesinglerow(item, "mah");
                    if (banchay == "1")
                        listtype.Add(lhang.FirstOrDefault(s => s["name_"].ToString() == "nước hoa bán chạy nhất")["id"].ToString());
                    if (sieusang == "1")
                        listtype.Add(lhang.FirstOrDefault(s => s["name_"].ToString() == "nước hoa siêu sang")["id"].ToString());
                    if (kmai == "1")
                        listtype.Add(lhang.FirstOrDefault(s => s["name_"].ToString() == "nước hoa khuyến mãi")["id"].ToString());
                    if (typesp == "GIFTSET")
                        listtype.Add(lhang.FirstOrDefault(s => s["name_"].ToString() == "bộ quà tặng")["id"].ToString());
                    else if (typesp == "nhdl")
                        listtype.Add(lhang.FirstOrDefault(s => s["name_"].ToString() == "nước hoa du lịch")["id"].ToString());
                    else if (typesp == "nhdsd")
                        listtype.Add(lhang.FirstOrDefault(s => s["name_"].ToString() == "nước hoa đã sử dụng")["id"].ToString());
                    else if (typesp == "tangpham")
                        listtype.Add(lhang.FirstOrDefault(s => s["name_"].ToString() == "tặng phẩm")["id"].ToString());
                    else if (typesp == "MN")
                        listtype.Add(lhang.FirstOrDefault(s => s["name_"].ToString() == "nước hoa mini")["id"].ToString());
                    else if (typesp == "NHT")
                        listtype.Add(lhang.FirstOrDefault(s => s["name_"].ToString() == "nước hoa test")["id"].ToString());


                    List<string> clipItem = new List<string>() { "barcode", "category", "size", "pricesale", "pricemarket", "percentdown" };
                    List<string> indexItem = new List<string>() { "mavachsp","mah", "sodungtich", "giaban", "giathitruong", "phamtramgiam" };
                    List<string> valueItem = new List<string>();
                   // List<string> clipItemvl = new List<string>() { "name_", "val", "code" };

                    for (var h = 0; h < clipItem.Count; h++)
                    {
                        var vldt = getvaluesinglerow(item, indexItem[h]);
                        if (clipItem[h] == "category")
                            vldt = string.Join(",", listtype);
                        else if (clipItem[h] == "size")
                            vldt = vlum.FirstOrDefault(s => s["val"].ToString() == vldt)["id"].ToString();
                        valueItem.Add(vldt);
                    }
                    clipItem.Add("codedescr");
                    valueItem.Add(code);
                    clipItem.Add("inventory");
                    var tonndc = getvaluesinglerow(item, "tonndc");
                    var tontx = getvaluesinglerow(item, "tontx");
                    var tonkc = getvaluesinglerow(item, "tonkc");
                    var invent = (int.Parse(tonndc) + int.Parse(tontx) + int.Parse(tonkc)).ToString();
                    valueItem.Add(invent);
                    clipItem.Add("inventdetail");
                   
                    List<dynamic> invents = new List<dynamic>();
                    List<string> lwh = new List<string>() {"1","2" };
                    List<string> lwhex = new List<string>() { "tonndc", "tonkc" };
                    for(var h=0;h< lwh.Count;h++)
                    {
                        var qqlt= getvaluesinglerow(item, lwhex[h]);
                        dynamic mm = new System.Dynamic.ExpandoObject();
                        mm.idwhouse = lwh[h];
                        mm.firstqlt = qqlt;
                        mm.invent = qqlt;
                        invents.Add(mm);
                    }
                    valueItem.Add(JsonConvert.SerializeObject(invents));
                    clipItem.Add("startinven");
                    valueItem.Add(JsonConvert.SerializeObject(invents));
                    string insertItstr = sqlnew.insertsqlstr("products", clipItem, valueItem, user);
                    sqls.Add(insertItstr);
                }
                sqlnew.runSQL(string.Join(";", sqls));
    
                //foreach (var n in clip)
                //{
                //   if()
                //}
                
            }
            return true;
        }
        bool upbrand(List<dynamic> cls, string table, string user,List<IXLRow> ws2)
        {
            string pathImg = Path.Combine(@"D:\buildserver\wwwroot\imgWeb\brands\");

            dynamic iinfor = JsonConvert.DeserializeObject<dynamic>(table);
            Common cmd = new Common();
            Sqlcommon sqln = new Sqlcommon();
            List<string> sqls = new List<string>();
            foreach (var j in ws2.Skip(1))
            {
                List<string> lkey = new List<string>();
                foreach (var l in cls)
                {
                    lkey.Add(l.clnew.ToString());
                }
                List<string> vls = new List<string>();
                string vlsub = "";
                var codeb = cmd.CreatCode("br");
                foreach (var m in cls)
                {
                    int b = int.Parse(m.old.ToString()) + 1;
                    var vl = j.Cell(b).Value.ToString();
                    var vlnew = m.clnew.ToString();

                    if (vlnew == "photo" || vlnew == "photodesk")
                    {
                             
                        if (vl != "")
                        {
                            dll.downloadfile(hostimold + vl, codeb + ".jpg", pathImg, "", false, "");
                            string[] lvl = vl.Split(@"\");
                            vl = lvl[lvl.Length - 1];
                            
                        }
                    }
                    else
                        vl = System.Web.HttpUtility.HtmlEncode(vl);
                    vls.Add(vl);
                    if (vlnew == "name_")
                        vlsub = vl;
                }

                if (iinfor.table.ToString() == "brands")
                {
                    lkey.Add("code");
                    vls.Add(codeb);

                    lkey.Add("seoname");
                    vls.Add(cmd.RemoveUnicode(vlsub).Replace(" ","-"));

                }
                string strsql = sqln.insertsqlstr(iinfor.table.ToString(), lkey, vls, user);
                sqls.Add(strsql);
                //sqln.runSQL(strsql);
            }
            sqln.runSQL(string.Join(";", sqls));
            return true;
        }
        bool upcity(List<dynamic> cls, string table, string user, List<IXLRow> ws2)
        {
            dynamic iinfor = JsonConvert.DeserializeObject<dynamic>(table);
            Common cmd = new Common();
            Sqlcommon sqln = new Sqlcommon();
            List<string> sqls = new List<string>();
            foreach (var y in ws2.Skip(1))
            {
                List<string> clip = new List<string>() { "name_","code", "levels"};
                List<string> indexvl = new List<string>() { "ten", "ma", "cap"};
                List<string> vls = new List<string>();
                for (var n = 0; n < clip.Count; n++)
                {
                    string clips = clip[n].ToString();
                    string vl = getvaluesinglerow(y, indexvl[n].ToString());
                    vls.Add(vl);
                }
                string strsql = sqln.insertsqlstr(iinfor.table.ToString(), clip, vls, user);
                sqln.runSQL(strsql);
            }
            return true;
        }
        bool upward(List<dynamic> cls, string table, string user, List<IXLRow> ws2)
        {
            dynamic iinfor = JsonConvert.DeserializeObject<dynamic>(table);
            Common cmd = new Common();
            Sqlcommon sqln = new Sqlcommon();
            List<string> sqls = new List<string>();
            foreach (var y in ws2.Skip(1))
            {
                List<string> clip = new List<string>() { "name_", "code", "levels", "codecity" };
                List<string> indexvl = new List<string>() { "ten", "ma", "cap", "maT" };
                List<string> vls = new List<string>();
                for (var n = 0; n < clip.Count; n++)
                {
                    string clips = clip[n].ToString();
                    string vl = getvaluesinglerow(y, indexvl[n].ToString());
                    vls.Add(vl);
                }
                string strsql = sqln.insertsqlstr(iinfor.table.ToString(), clip, vls, user);
                sqln.runSQL(strsql);
            }
            return true;
        }
        bool updistrict(List<dynamic> cls, string table, string user, List<IXLRow> ws2)
        {
            dynamic iinfor = JsonConvert.DeserializeObject<dynamic>(table);
            Common cmd = new Common();
            Sqlcommon sqln = new Sqlcommon();
            List<string> sqls = new List<string>();
            foreach (var y in ws2.Skip(1))
            {
                List<string> clip = new List<string>() { "name_", "code", "levels", "codeward", "codecity" };
                List<string> indexvl = new List<string>() { "ten", "ma", "cap", "maQH", "maTP" };
                List<string> vls = new List<string>();
                for (var n = 0; n < clip.Count; n++)
                {
                    string clips = clip[n].ToString();
                    string vl = getvaluesinglerow(y, indexvl[n].ToString());
                    vls.Add(vl);
                }
                string strsql = sqln.insertsqlstr(iinfor.table.ToString(), clip, vls, user);
                sqln.runSQL(strsql);
            }
            return true;
        }
        [HttpPost]
        public IActionResult Index(IFormFile files,string colums,string table)
        {
            
            dynamic iinfor = JsonConvert.DeserializeObject<dynamic>(table);
            List<dynamic> cls = JsonConvert.DeserializeObject<List<dynamic>>(colums);

            string FolderSource = Path.Combine(@"wwwroot\excels\");
            var path = Path.Combine(FolderSource, "", files.FileName);
            FileStream fileStream = new FileStream(path, FileMode.Create);
            files.CopyTo(fileStream);
            fileStream.Dispose();
            string fileName = path;
            var workbook = new XLWorkbook(fileName);
            var ws1 = workbook.Worksheet(1);
            var ws2 = ws1.Rows().ToList();
            header = ws2.FirstOrDefault();
            if (iinfor.table.ToString() == "brands")
                upbrand(cls, table, "1", ws2);
           else if (iinfor.table.ToString() == "customers")
                upcustomer(cls, table, "1", ws2);
            else if (iinfor.table.ToString() == "billsale")
                upbillsale(cls, table, "1", ws2);
            else if (iinfor.table.ToString() == "descproduct")
            {
                //var indexsdt = indexCell("sodungtich");
                //var sdt = ws2.Select(s => s.Cell(indexsdt).Value.ToString()).ToList();
                //var sdts = sdt.Skip(1).GroupBy(s => s).ToList();
                //List<string> sqlsize = new List<string>();
                //foreach (var x in sdts)
                //{
                //    string sizstr = sqln.insertsqlstr("size", new List<string>() { "name_","val" }, new List<string>() {"ml",x.Key },"1");
                //    sqlsize.Add(sizstr);
                //}
                //var indexorgs = indexCell("xuatxu");
                //var org = ws2.Select(s => s.Cell(indexorgs).Value.ToString().ToLower()).ToList();
                //var orgs = org.Skip(1).GroupBy(s => s).ToList();
                //foreach (var x in orgs)
                //{
                //    string codeorg = cmd.CreatCode("org");
                //    string sizorg = sqln.insertsqlstr("origin", new List<string>() { "name_", "code" }, new List<string>() { x.Key,codeorg }, "1");
                //    sqlsize.Add(sizorg);
                //}
                //sqln.runSQL(string.Join(";", sqlsize));
                upproduct(cls, table, "1", ws2);
            }
            else if (iinfor.table.ToString() == "citys")
                upcity(cls, table, "1", ws2);
            else if (iinfor.table.ToString() == "wards")
                upward(cls, table, "1", ws2);
            else if (iinfor.table.ToString() == "distrists")
                updistrict(cls, table, "1", ws2);
            //sqln.runSQL(string.Join(";",sqls));
            var tb = sql.GetTableNames();
            return View(tb);
        }
        public IActionResult SeoName()
        {
            return View();
        }
        [HttpPost]
        public IActionResult SeoName(string table)
        {
            var users = HttpContext.Session.Get<LoginModel>("user");
            string user = users.id;
            var data = sqln.getdataSQL(table, "");
            foreach(var i in data)
            {
                string name =cmd.RemoveUnicode(i["name_"].ToString()).Replace(" ","-");
                string str = sqln.updatesqlOneItemstr(table, new List<string>() { "seo_name" }, new List<string>() { name }, int.Parse(i["id"].ToString()), user);
                sqln.runSQL(str);
            }
            return View();
        }
    }
}