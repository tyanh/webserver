﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Office.CustomUI;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;
using Webserver.Models;

namespace Webserver.Controllers
{
    public class ActionController : Controller
    {
        Common cmd = new Common();
        Sqlcommon sql = new Sqlcommon();
        LoginModel user = new LoginModel();
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        public ActionController(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
             user =_session.Get<LoginModel>("userlogin");
        }
        public IActionResult CreateInvoice(string table)
        {
          
                Dictionary<string, object> obj = new Dictionary<string, object>();
               
                ConfigTable cf = new ConfigTable();
                cf.SetConfigTable(table,"");
                if (!cmd.ispermission(cf.idmenu, "add", user))
                    return Redirect("/Home/Login");
                Sqlcommon sql = new Sqlcommon();
                ViewBag.table = table;
                var heads = sql.getdataSQL("headerinvoice", "where " + cmd.strNotdelete());
                var head = heads.FirstOrDefault(s => s["invoiceid"].ToString().Split(',').Contains(cf.idmenu));

                var company = sql.finddata2("inforcompany", "where id<>-1");
                var detail = sql.finddata2("attribute_table", "where name_='" + head["tabledetail"] + "'");
              
                List<Dictionary<string, object>> tabledata = new List<Dictionary<string, object>>();
                string[] tbdata = head["tablegetdata"].ToString().Split(',');
                foreach (var t in tbdata)
                {
                    if (t == "")
                        continue;
                    Dictionary<string, object> d = new Dictionary<string, object>();
                    d["table"] = t;
                    if (t != "products")
                        d["data"] = sql.getdataSQL(t, "where " + cmd.strNotdelete());
                    else
                    {
                        string strget = "select descproduct.name_ as nameitem,products.*, CONCAT(size.val,size.name_) as size_ FROM descproduct INNER JOIN products ON descproduct.code=products.codedescr left JOIN size ON products.size=size.id";
                        d["data"] = sql.getdataSQLstr(strget);
                    }
                    tabledata.Add(d);
                }
                obj["titlehead"] = head["titles"].ToString().Split(',').ToList();
                obj["type"] = head["columndata"].ToString().Split(',').ToList();
                obj["company"] = company;
                obj["tinvoice"] = head["name_"].ToString();
                obj["checkinvent"] = head["checkinvent"].ToString();
                obj["isdiscount"] = head["isdiscount"].ToString();
                obj["notforlack"] = head["notforlack"].ToString();
                obj["giftcode"] = head["giftcode"].ToString();
                obj["payment"] = head["payment"].ToString();
                obj["keyadd"] = head["keyadd"].ToString();
                obj["datamap"] = head["mapdata"].ToString();
                obj["columnsum"] = head["columnsum"].ToString();
                obj["sumend"] = head["clsumtotal"].ToString();
                obj["tablemapcolumn"] = head["mapdatatable"].ToString();
                obj["tabletotable"] = head["tabletotable"].ToString();
                obj["keysnotsame"] = head["keys"].ToString();
                obj["tabledata"] = JsonConvert.SerializeObject(tabledata);
                obj["keyinfor"] = head["keyinfor"].ToString();
                obj["detail"] = detail;
                obj["headnotnull"] = cf.valid;
                obj["headmapdata"] = cf.pathupload;
                obj["username"] = user.username;
                obj["invoicenum"] = head["prefix"].ToString() + cmd.CreatNumberBill(table);
                obj["dataroot"] = "";
                string objtable = Request.Query["obj"];
                if (objtable != null)
                {
                    string[] paraconnect = head["paraconnect"].ToString().Split('-');
                string wheref = "where id=" + Request.Query["id"];
                if (paraconnect[0] != "")
                    wheref = wheref + " AND (" + cmd.strNotdelete()+")";
                    var dataroot = sql.finddata2(objtable, wheref);
                    
                    if (dataroot.Count <= 0)
                        return Redirect("/Home/Login");
                    obj["dataroot"] = dataroot;
                }
               
            return View(obj);
        }
        [HttpPost]
        public async Task<IActionResult> CreateInvoice(string table,string txt_bill, string txt_detail)
        {

            //Page.ClientScript.RegisterStartupScript(GetType(), "hwa", "alert('Hello World');", true);
            var ssinvoice = HttpContext.Session.Get<string>("ssinvoice");
            if (ssinvoice == null)
                Redirect("/");
            do
            {
                await Task.Delay(cmd.randoomsecond());
                //indext++;
                //if (indext > 10)
                //    Startup.runsql = "";
                if (Startup.runsql == "")
                {
                   Startup.runsql = ssinvoice;
                   break;
                }
            }
            while (Startup.runsql != ssinvoice);
            List<string> sqls = new List<string>();
            Dictionary<string, object> obj = new Dictionary<string, object>();
                var bill = JsonConvert.DeserializeObject<List<dynamic>>(txt_bill);
                var billString = JsonConvert.DeserializeObject<List<dynamic>>(txt_bill);
                dynamic username = new System.Dynamic.ExpandoObject();
                username.cl = "username";
                username.vl = user.username;
                billString.Add(username);
                var detail = JsonConvert.DeserializeObject<List<dynamic>>(txt_detail);
                ViewBag.table = table;
                ConfigTable cf = new ConfigTable();
                cf.SetConfigTable(table,"");
                var heads = sql.getdataSQL("headerinvoice", "where " + cmd.strNotdelete());
                var head = heads.FirstOrDefault(s => s["invoiceid"].ToString().Split(',').Contains(cf.idmenu));
                var applycustomer = head["applycustomer"].ToString();
                var prefix = head["prefix"].ToString();
                var uptimedesc = head["updatedescproduct"].ToString();
                var keyinfor = head["keyinfor"].ToString().Split('-');
                var detailltable = sql.finddata2("attribute_table", "where name_='" + head["tabledetail"] + "'");
                var clwrongdt = head["clwrongdetail"].ToString().Split(",");
                var datanotsame = head["datanotsame"].ToString().Split(",");
                var actioninvent = head["actioninvent"].ToString().Split("#");
                var waitcheck = head["waitcheck"].ToString();
                var checkinvent = head["checkinvent"].ToString().Split('/');
                var clinsertdt = detailltable["columns"].ToString().Split(",");
                var typedt = detailltable["atr_"].ToString().Split(",");
                var titledetail = detailltable["titles"].ToString().Split(",");
                var mapdatatable = System.Web.HttpUtility.HtmlDecode(head["mapdatatable"].ToString()).Split(',');
                var tabletotable = System.Web.HttpUtility.HtmlDecode(head["tabletotable"].ToString()).Split(',');
                var mapdata = head["mapdata"].ToString().Split(',');
                string now = DateTime.Now.ToString();
                var clinsertbill = cf.column.Split(",");
                var typebill = cf.kind.Split(",");
                var validbill = cf.valid.Split(",");
                var mapbill = cf.pathupload.Split(",");
                List<string> vlbill = new List<string>();
                string code = cmd.CreatCode(table);
                string statusbill = "";
                foreach (var sttusb in typebill)
                {
                    if (sttusb.Contains("statusiv"))
                    {
                        string[] ttiv = sttusb.Split('-');
                        string vltt = bill[typebill.IndexOf(sttusb)].vl.ToString();
                        string strtt = "select " + ttiv[1] + ".id," + ttiv[1] + ".calinvent," + ttiv[1] + ".name_ from " + ttiv[1] + " left join headerinvoice on " + ttiv[1] + ".invoice like headerinvoice.id where headerinvoice.id=" + head["id"] + "";
                        var stobjs = sql.getdataSQLstr(strtt);
                        var stobj = stobjs.FirstOrDefault(s => s["name_"].ToString() == vltt);
                        bill[typebill.IndexOf(sttusb)].vl = stobj["id"];
                        statusbill = stobj["calinvent"].ToString();
                        break;
                    }
                }
                for (var drop = 0; drop < typebill.Length; drop++)
                {
                    if (typebill[drop].Contains("getvlfromkey"))
                    {
                        var oj = bill.FirstOrDefault(s => s.cl == clinsertbill[drop]);
                        var name = oj.vl.ToString();
                        var iget = typebill[drop].Split('-');
                        var oobj = sql.finddata2(iget[1], "where name_=N'" + name + "'");
                        if (oobj.Count <= 0)
                        {
                            var namecl = clinsertbill[drop];
                            if (validbill.IndexOf(namecl) != -1)
                                return View();
                        }
                        else
                        {
                            oj.vl = oobj[iget[2]].ToString();
                        }
                    }
                    else if (typebill[drop].Contains("datafromcolumn"))
                    {
                        var stype = typebill[drop].Split('-');
                        var checkcl = bill.FirstOrDefault(s => s.cl == clinsertbill[drop]);
                        if (checkcl == null)
                        {
                            dynamic newcl = new System.Dynamic.ExpandoObject();
                            newcl.cl = clinsertbill[drop];
                            newcl.vl = "";
                            bill.Add(JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(newcl)));
                        }
                        string vlcompare = bill.FirstOrDefault(s => s.cl.ToString() == stype[4].ToString()).vl.ToString();
                        string strfind = "select top 1 " + stype[2] + " from " + stype[1] + " where " + stype[3] + "='" + vlcompare + "' AND (" + cmd.strNotdelete()+")";
                        var ofind = sql.getdataSQLstr(strfind);
                        if (ofind.Count > 0)
                            bill[bill.Count - 1].vl = ofind[0][stype[2]].ToString();
                        else
                        {
                            string insertnew = sql.insertsqlstr(stype[1], new List<string>() { stype[3] }, new List<string>() { vlcompare }, user.id);
                            bill[bill.Count - 1].vl = sql.runSQLAndGetId(insertnew, stype[3], vlcompare, stype[1]);
                        }
                    }
                }
                List<Dictionary<string, object>> tablemap = new List<Dictionary<string, object>>();
                foreach (var m in mapdatatable)
                {
                    var ma = m.Split('-');
                    Dictionary<string, object> table_ = new Dictionary<string, object>();
                    table_["table"] = ma[1];
                    table_["cltable"] = ma[3];
                    table_["clitem"] = ma[0];
                    table_["clgetvl"] = ma[2];
                    tablemap.Add(table_);
                }
                var lkeys = detail.Select(s => "'" + s[keyinfor[0]].ToString() + "'").ToList();
                Dictionary<string, object> billdetail = new Dictionary<string, object>();
                string where = "where " + keyinfor[2] + " in (" + string.Join(",", lkeys) + ")";
                var listItem = sql.getdataSQL(keyinfor[1], where);
                List<Dictionary<string, object>> listdesc = new List<Dictionary<string, object>>();
                if (uptimedesc == "1")
                {
                    List<string> desccode = listItem.Select(s =>"'"+s["codedescr"].ToString()+"'").ToList();
                    string wheredesc= "where code in (" + string.Join(",", desccode)+")";
                    listdesc = sql.getdataSQL("descproduct", wheredesc);
                    foreach(var desc in listdesc)
                    {
                        string updatedesc = sql.updatesqlOneItemstr("descproduct", new List<string>(), new List<string>(),int.Parse(desc["id"].ToString()), user.id);
                        sqls.Add(updatedesc);
                    }
                }
           
            List <Dictionary<string, object>> subdatamap = new List<Dictionary<string, object>>();
               
                foreach (var m in tablemap)
                {
                    if (subdatamap.FirstOrDefault(s => s["table"].ToString() == m["table"].ToString()) != null)
                        continue;
                    var clget = listItem.Select(s => "'" + s[m["clgetvl"].ToString()] + "'").ToList();
                    var clgets = clget.GroupBy(s => s).ToList();
                    string w_ = "where " + m["cltable"] + " in (" + string.Join(",", clgets.Select(s => s.Key)) + ")";
                    Dictionary<string, object> s_ = new Dictionary<string, object>();
                    s_["table"] = m["table"];
                    s_["data"] = sql.getdataSQL(m["table"].ToString(), w_);
                    subdatamap.Add(s_);
                }
                foreach (var m in tabletotable)
                {
                    var ma = m.Split('-');
                    if (subdatamap.FirstOrDefault(s => s["table"].ToString() == ma[2].ToString()) != null)
                        continue;
                    Dictionary<string, object> s_ = new Dictionary<string, object>();
                    s_["table"] = ma[2];
                    s_["data"] = sql.getdataSQL(ma[2].ToString(), "where " + cmd.strNotdelete());
                    subdatamap.Add(s_);
                }
                foreach (var m in datanotsame)
                {
                    if (m == "")
                        continue;
                    var mm = m.Split('-');
                    var vl1 = bill.FirstOrDefault(s => s.cl.ToString() == mm[0]).vl.ToString();
                    var vl2 = bill.FirstOrDefault(s => s.cl.ToString() == mm[1]).vl.ToString();
                    if (vl1 == vl2)
                        return View();
                }
                foreach (var i in detail)
                {

                    var keys = i[keyinfor[0]].ToString();

                    var it = listItem.FirstOrDefault(s => s[keyinfor[2]].ToString() == keys);
                    if (checkinvent.Count() > 1)
                    {
                        var keycheck = checkinvent[1].Split('&');
                        var k1 = bill.FirstOrDefault(s => s.cl.ToString() == keycheck[1]).vl.ToString();
                        var datakey = JsonConvert.DeserializeObject<List<dynamic>>(it[checkinvent[0]].ToString());
                        var wget = datakey.FirstOrDefault(s => s[keycheck[0]].ToString() == k1);
                        var qltep = i[checkinvent[4]].ToString();
                        var qltepinv = wget[checkinvent[2]].ToString();
                        if (cmd.convertNumberDouble(qltep) > cmd.convertNumberDouble(qltepinv))
                        {
                            return View();
                        }
                    }
                    foreach (var mcl in mapdata)
                    {
                        var clm = mcl.Split('-');
                        var idtype = clinsertdt.ToList().IndexOf(clm[0]);

                        if (!typedt[idtype].Contains("textbind-"))
                        {
                            if (clwrongdt.Contains(clm[0]))
                            {
                                var vlcp1 = i[clm[0]];
                                var vlcp2 = it[clm[1]];
                                if (cmd.isnumber(vlcp2.ToString()))
                                {
                                    if (cmd.convertNumber(vlcp1.ToString()).ToString() != vlcp2.ToString())
                                        return View();
                                }
                                else if (vlcp1.ToString() != vlcp2.ToString())
                                    return View();
                            }
                            cmd.eidtvalueInvoice(i, clm[0], it[clm[1]].ToString());
                        }


                    }
                    foreach (var m in mapdatatable)
                    {
                        var mp = m.Split("-");
                        var cls = mp[0].ToString().Split('&');
                        var clg = mp[4].ToString().Split('&');
                        var tmap = (List<Dictionary<string, object>>)subdatamap.FirstOrDefault(s => s["table"].ToString() == mp[1].ToString())["data"];
                        var objs = tmap.FirstOrDefault(s => s[mp[3].ToString()].ToString() == it[mp[2].ToString()].ToString());
                        for (var v = 0; v < cls.Length; v++)
                        {
                            cmd.eidtvalueInvoice(i, cls[v], objs[mp[3]].ToString());
                            foreach (var mtb in tabletotable)
                            {
                                var mptmp = mtb.Split("-");
                                if (mptmp[0] == cls[v])
                                {
                                    cmd.eidtvalueInvoice(i, mptmp[1], objs[mptmp[3]].ToString());
                                    break;
                                }
                            }
                        }
                    }
                    for (var cal = 0; cal < typedt.Length; cal++)
                    {
                        var clindex = clinsertdt[cal];
                        if (typedt[cal].Contains("calculator"))
                        {
                            var cals = typedt[cal].Split('?')[1].Split('/');
                            var vl1 = i[cals[0]].ToString();
                            var vl2 = i[cals[2]].ToString();
                            var vlsum = cmd.calculator(cmd.convertNumberDouble(vl1), cmd.convertNumberDouble(vl2), cals[1]);
                            if (clwrongdt.Contains(clindex))
                            {
                                if (cmd.convertNumber(i[clindex].ToString()).ToString() != vlsum.ToString())
                                    return View();
                            }
                            i[clindex] = vlsum.ToString();
                        }
                        else if (typedt[cal].Contains("numbers"))
                        {
                            i[clindex] = cmd.convertNumberDouble(i[clindex].ToString());
                        }
                        else if (typedt[cal] == "idproduct")
                        {
                            i[clinsertdt[cal]] = it["id"].ToString();

                        }
                    }
                    if (waitcheck != "1")
                    {
                        if (statusbill != "")
                        {
                            var sttb = statusbill.Split("|");
                            actioninvent = sttb[0].ToString().Split("#");
                            head["pluswarehouse"] = sttb[1].ToString();
                        }
                        string upitem = cmd.updateInventory(it, i, bill, actioninvent, head["pluswarehouse"].ToString(), keyinfor[1], user.id);
                        sqls.Add(upitem);
                    }

                }
                foreach (var item in detail)
                {
                    List<string> vldetailinsert = new List<string>();
                    var clisdt = clinsertdt.ToList();
                    for (var u = 0; u < titledetail.Length; u++)
                    {
                        vldetailinsert.Add(item[clinsertdt[u]].ToString());
                    }
                    for (var u = titledetail.Length; u < clinsertdt.Length; u++)
                    {
                        if (typedt[u].Contains("mapbill"))
                        {
                            var map = typedt[u].Split('-');
                            vldetailinsert.Add(bill.FirstOrDefault(s => s.cl.ToString() == map[1].ToString()).vl.ToString());
                        }
                        else if (typedt[u] == "code")
                            vldetailinsert.Add(code);
                        else if (typedt[u] == "datebill")
                            vldetailinsert.Add(now);
                        else
                            vldetailinsert.Add(item[clinsertdt[u]].ToString());
                    }
                    string insertstr = sql.insertsqlstr(head["tabledetail"].ToString(), clisdt, vldetailinsert, user.id);
                    sqls.Add(insertstr);
                }

                for (var b = 0; b < mapbill.Length; b++)
                {
                    var vl = bill.FirstOrDefault(s => s.cl.ToString() == mapbill[b]).vl.ToString();
                    if (typebill[b].Contains("numbers"))
                        vl = cmd.convertNumberDouble(vl).ToString();
                    billdetail[mapbill[b]] = vl;
                    vlbill.Add(vl);
                }
                dynamic vlgift = new System.Dynamic.ExpandoObject();
                for (var i = vlbill.Count; i < clinsertbill.Length; i++)
                {
                    var svl = "";
                    var ttmp = typebill[i];
                    if (typebill[i] == "code")
                        svl = code;
                    else if (typebill[i] == "datebill")
                        svl = now;
                    else if (typebill[i] == "jsondetail")
                        svl = txt_detail;
                    else if (typebill[i] == "jsoninfor")
                        svl = JsonConvert.SerializeObject(billString);
                    else if (typebill[i] == "invoicenumber")
                        svl = prefix + cmd.CreatNumberBill(table);
                    else if (typebill[i].Contains("statusiv"))
                    {
                        string[] ttiv = typebill[i].Split('-');
                        string strtt = "select " + ttiv[1] + ".id,headerinvoice.name_ from " + ttiv[1] + " left join headerinvoice on " + ttiv[1] + ".invoice like headerinvoice.id where " + ttiv[1] + ".isdefault=1 AND headerinvoice.id=" + head["id"] + "";
                        var stobj = sql.getdataSQLstr(strtt);
                        svl = stobj[0]["id"].ToString();
                    }
                    else if (typebill[i].Contains("giftcode"))
                    {
                        var stype = typebill[i].Split('-');
                        string codegift = bill.FirstOrDefault(s => s.cl.ToString() == stype[3]).vl.ToString();
                        if (codegift == "")
                        {
                            vlbill.Add(svl);
                            continue;
                        }
                        string gift = cmd.GetGiftCode(codegift);
                        if (cmd.isnumber(gift) && codegift != "")
                            return View();

                        List<dynamic> dnmgift = JsonConvert.DeserializeObject<List<dynamic>>(gift);
                        vlbill[clinsertbill.IndexOf(stype[5])] = dnmgift[0].id;
                        List<string> codeuser = new List<string>();
                        if (dnmgift[0].codeuser.ToString() != "")
                        {

                            codeuser.AddRange(dnmgift[0].codeuser.ToString().Split(','));
                            if (codeuser.IndexOf(codegift) != -1)
                                return View();

                        }
                        var lbind = System.Web.HttpUtility.HtmlDecode(stype[4]).Split('&');
                        var checkcl = bill.FirstOrDefault(s => s.cl.ToString() == lbind[1]);
                        if (checkcl == null)
                        {
                            var newobj = cmd.newDynamicObj(new List<string>() { lbind[1].ToString() }, new List<string>() { dnmgift[0][lbind[0]].ToString() });
                            var n_ = JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(newobj));
                            bill.Add(n_);
                            billString.Add(n_);
                        }
                        else
                        {
                            bill.FirstOrDefault(s => s.cl.ToString() == lbind[1]).vl = dnmgift[0][lbind[0]];
                            billString.FirstOrDefault(s => s.cl.ToString() == lbind[1]).vl = dnmgift[0][lbind[0]];
                        }
                        codeuser.Add(codegift);
                        var qltuser = cmd.convertNumberDouble(dnmgift[0].qltuser.ToString()) + 1;
                        string upgift = sql.updatesqlOneItemstr("promotion", new List<string>() { "qltuser", "codeuser" }, new List<string>() { qltuser.ToString(), string.Join(",", codeuser) }, int.Parse(dnmgift[0].id.ToString()), user.id);
                        sqls.Add(upgift);
                        svl = dnmgift[0].valuecode.ToString();
                        vlgift.unit = dnmgift[0].unit;
                        vlgift.value = dnmgift[0][lbind[0]];
                    }
                    else if (typebill[i].Contains("calculator"))
                    {
                        var cal = System.Web.HttpUtility.HtmlDecode(typebill[i]).Split('?');
                        if (cal[1] == "sumitem")
                        {
                            svl = detail.Sum(s => s[cal[2]]).ToString();
                        }
                        else if (cal[1] == "sumbill")
                        {
                            var scal = cal[2].Split('&');
                            var f = scal[0].Split('/');
                            var vf1 = billdetail[f[0]].ToString();
                            var vf2 = billdetail[f[2]].ToString();
                            var sumfirst = cmd.calculator(cmd.convertNumberDouble(vf1), cmd.convertNumberDouble(vf2), f[1]);
                            foreach (var cal_ in scal.Skip(1))
                            {
                                var scal_ = cal_.Split('/');
                                var vlnext_ = scal_[1];
                                if (!cmd.isnumber(vlnext_))
                                    vlnext_ = billdetail[vlnext_].ToString();

                                sumfirst = cmd.calculator(sumfirst, cmd.convertNumberDouble(vlnext_), scal_[0]);
                            }
                            svl = sumfirst.ToString();
                        }
                        else
                        {
                            var scal = cal[1].Split('&');
                            var f = scal[0].Split('/');
                            var vf1 = vlbill[clinsertbill.IndexOf(f[0])].ToString();
                            var vf2 = vlbill[clinsertbill.IndexOf(f[2])].ToString();
                            var sumfirst = cmd.calculator(cmd.convertNumberDouble(vf1), cmd.convertNumberDouble(vf2), f[1]);
                            foreach (var cal_ in scal.Skip(1))
                            {
                                var scal_ = cal_.Split('/');
                                var vlnext_ = scal_[1];
                                if (vlnext_ == "vlgift")
                                    vlnext_ = cmd.calmoneygift(vlgift, sumfirst);
                                else if (!cmd.isnumber(vlnext_))
                                    vlnext_ = vlbill[clinsertbill.IndexOf(vlnext_)].ToString();
                                sumfirst = cmd.calculator(sumfirst, cmd.convertNumberDouble(vlnext_), scal_[0]);
                            }
                            svl = sumfirst.ToString();
                        }
                    }
                    else if (typebill[i] == "waitbind")
                        svl = "";
                    else
                        svl = bill.FirstOrDefault(s => s.cl.ToString() == clinsertbill[i]).vl.ToString();
                    vlbill.Add(svl);
                    if (typebill[i] != "jsondetail" && typebill[i] != "jsoninfor")
                    {
                        dynamic addmorebill = new System.Dynamic.ExpandoObject();
                        addmorebill.cl = clinsertbill[i];
                        addmorebill.vl = svl;
                        billString.Add(addmorebill);
                    }
                    billdetail[clinsertbill[i]] = svl;
                }
                foreach (var v in validbill)
                {
                    if (vlbill[clinsertbill.IndexOf(v)] == "")
                        return View();
                }
                string objtable = Request.Query["obj"];
                if (objtable != null)
                {
                    string[] paraconnect = head["paraconnect"].ToString().Split('/');
                var dataroot = sql.finddata2(objtable, "where id=" + Request.Query["id"] + " AND (remain>0 OR remain is null)");
                if (dataroot.Count <= 0)
                    return Redirect("/Home/Login");

                List<string> clisconnect = new List<string>();
                List<string> vlisconnect = new List<string>();
                foreach (var o_ in paraconnect)
                {
                    string[] paraconnect1 =o_.Split('-');
                    if (paraconnect1[0] == objtable)
                    {
                        vlisconnect = dataroot[paraconnect1[1]].ToString().Split(",").ToList();
                        vlisconnect.Add(code);
                        clisconnect.Add(paraconnect1[0]);
                      
                        double cTotal = cmd.convertNumberDouble(dataroot[paraconnect1[2]].ToString());
                        double cTotalbill = cmd.convertNumberDouble(vlbill[clinsertbill.ToList().IndexOf(paraconnect1[3])]);
                        double cremain = cmd.convertNumberDouble(dataroot["remain"].ToString());
                        double checkqlt = cTotal - (cremain - cTotalbill);
                        if (checkqlt <0)
                        {
                            return Redirect("/Home/Login");
                        }
                        clisconnect.Add("remain");
                        vlisconnect.Add(checkqlt.ToString());
                        break;
                    }
                }
                    string strupcode = sql.updatesqlOneItemstr(objtable, clisconnect, vlisconnect, int.Parse(Request.Query["id"]), user.id);
                    sqls.Add(strupcode);
                }
                string indsertstrb = sql.insertsqlstr(table, clinsertbill.ToList(), vlbill, user.id);
                sqls.Add(indsertstrb);

            if (applycustomer != "")
            {
                var strctm = sql.updatecusInvoice(applycustomer, vlbill, clinsertbill.ToList(), user.id);
                if(strctm!="")
                    sqls.Add(strctm);
            }
                //sql.runSQL(string.Join(";", sqls));
                Startup.runsql = "";
                return Redirect("/Action/PrintInvoice?objtb=" + table + "&code=" + code + "");
               
        }
        public IActionResult PrintInvoice(string objtb,string code)
        {
            ConfigTable cf = new ConfigTable();
            string table = objtb;
            var dataprint= sql.finddata2(table, "where code='" + code + "'");
            return View(dataprint);
        }
        [HttpPost]
        public IActionResult Create(string table, string id, ItemModel model)
        {
            
            ViewBag.table = table;
            ConfigTable cf = new ConfigTable();
            cf.SetConfigTable(table,"");
           
            if (!cmd.ispermission(cf.idmenu, "add", user))
                return Redirect("/Home/Login");
            if (cf.name_ == "workers" || cf.name_== "groupmanager")
            {
                   Startup.userupdate.Add(cf.name_+id);
            }
            string[] valid = cf.valid.Split(',');
            string[] type = cf.kind.Split(',');
            string[] keys = cf.key_.Split('-');
            string key = keys[0];
            List<dynamic> cl = JsonConvert.DeserializeObject<List<dynamic>>(model.cldata);
          
            List<dynamic> vl = JsonConvert.DeserializeObject<List<dynamic>>(model.vldata);
            int indexkey = cl.IndexOf(key);
            if (indexkey > -1)
            {
                string strkey = "where " + key + "='" + vl[indexkey] + "'";
                if (keys.Length > 1)
                {
                    foreach (var l in keys.Skip(1))
                    {
                        var key_tmp = l.Split(" ");
                        int indexskey = cl.IndexOf(key_tmp[1]);

                        strkey = strkey + " " + key_tmp[0] + " " + key_tmp[1] + "='" + vl[indexskey] + "'";
                    }
                }
                var ckkey = sql.finddata2(cf.name_, strkey);
                if (ckkey.Count > 0)
                {
                    if (id == null)
                        return View(model);
                    else if (ckkey["id"].ToString() != id)
                        return View(model);
                }
            }
            string[] columns = cf.column.Split(',');
            int removepass = -1;
            for (var j = 0; j < type.Length; j++)
            {
                if (type[j] != "ricktext")
                {
                    try
                    {
                        vl[j] = vl[j].ToString();
                    }
                    catch
                    {

                    }
                }
                if (type[j] == "numbers")
                {
                    int idx = cl.IndexOf(columns[j]);
                    vl[idx] = cmd.convertNumber(vl[idx].ToString());
                }
                else if (type[j] == "date")
                {
                    int idx = cl.IndexOf(columns[j]);
                    vl[idx] = cmd.convertDate(vl[idx].ToString());
                }
                else if (type[j] == "password")
                {
                    int idx = cl.IndexOf(columns[j]);
                    if (!cmd.IsMD5(vl[idx].ToString()))
                    {
                        vl[idx] = cmd.MD5(vl[idx].ToString());
                    }
                    else
                    {
                        removepass = idx;
                    }
                }
                //else if (type[j] == "address")
                //{
                //    int idx = cl.IndexOf(columns[j]);
                //    dynamic a_ = JsonConvert.DeserializeObject<dynamic>(vl[idx].ToString());
                //    cl.Add("city");
                //    cl.Add("distrist");
                //    vl[idx] = a_.a_.ToString();
                //    vl.Add(a_.city_.ToString());
                //    vl.Add(a_.District_.ToString());
                //}
               
            }
            foreach (var i in valid)
            {
                int index = cl.IndexOf(i);
                if (index > -1)
                {
                    if (vl[index] == "")
                        return View(model);
                }
            }
            List<string> clsql = new List<string>();
            List<string> vlsql = new List<string>();
            if (type.IndexOf("code")>-1)
            {
                string nameclcode = columns[type.IndexOf("code")];
                int indexcode = cl.IndexOf(nameclcode);
                if (id != null)
                {

                    cl.RemoveAt(indexcode);
                    vl.RemoveAt(indexcode);
                }
            }
            if (removepass > -1)
            {
                cl.RemoveAt(removepass);
                vl.RemoveAt(removepass);
            }
            for (var i = 0; i < cl.Count; i++)
            {
                clsql.Add(cl[i]);
               
                vlsql.Add(vl[i]);
            }

            List<string> strsql = new List<string>();
            int index_name = cl.IndexOf("name_");
            var a = new Dictionary<string, object>();
            if (index_name != -1 & indexkey>-1)
            {
                 a = sql.finddata2(cf.name_, "where name_ like N'" + vlsql[index_name].ToString() + "'");
            }
            if (id == null && a.Count <= 0)
                    strsql.Add(sql.insertsqlstr(cf.name_, clsql, vlsql, user.id));
                else if (id != null && (a.Count == 0 || a["id"].ToString() == id))
                {
                    string content_new = cmd.CreateDictionary(clsql, vlsql);
                    strsql.Add(sql.insert_htr_item(table, id, content_new, JsonConvert.SerializeObject(a), user.id, "Edit"));
                    strsql.Add(sql.updatesqlOneItemstr(cf.name_, clsql, vlsql, int.Parse(id), user.id));
                }
            
            sql.runSQL(string.Join(";", strsql));
            
            return RedirectToAction();
        }
        public IActionResult Create(string table,string id)
        {
            ConfigTable config = new ConfigTable();
            config.SetConfigTable(table,"");
            if (!cmd.ispermission(config.idmenu,"add", user))
                return Redirect("/Home/Login");
            ItemModel model = new ItemModel();
            ViewBag.table = table;
            ViewBag.pathupload = config.pathupload;
            model.config = config;
            model.key = new List<string>();
            model.key.Add(config.name_.ToString());
            model.title = config.titlepage;
            if (id != null)
            {
                model.title = "chỉnh sửa " + model.title;
                model.key.Add(id);
                model.data = sql.finddata2(config.name_.ToString(), "where id=" + id + "");
            }
            else
            {
                model.title = "tạo mới " + model.title;
                model.key.Add("-1");
            }
             return View(model);
        }
        public IActionResult ViewHistoryItem()
        {
            var user = sql.getdataSQL("workers", "");
            DataList db = new DataList();
            db.data = sql.getdataSQL("historyitems", "order by id desc");
            int id = 1;
            foreach(var i in db.data)
            {
                i["id"] = id;
                i["usercreate"] = user.FirstOrDefault(s => s["id"].ToString() == i["usercreate"].ToString())["username"];
                id++;
            }
            string now = DateTime.Now.ToString("dd-M-yyyy");
            ViewBag.form = now;
            ViewBag.to = now;
            ViewBag.action = "";
            return View(db);
        }
        [HttpPost]
        public IActionResult ViewHistoryItem(string form,string to,string action)
        {
            var user = sql.getdataSQL("workers", "");
            DataList db = new DataList();
            DateTime f = DateTime.Parse(cmd.convertDate(form));
            DateTime t = DateTime.Parse(cmd.convertDate(to)).AddDays(1);
            string where = "where createdate>='" + f + "' AND createdate<'" + t + "'";
            if (action != "0")
                where += " AND action Like N'" + cmd.action_htr(action) + "'" ;
            db.data = sql.getdataSQL("historyitems", where+" order by id desc");
            int id = 1;
            foreach (var i in db.data)
            {
                i["id"] = id;
                i["usercreate"] = user.FirstOrDefault(s => s["id"].ToString() == i["usercreate"].ToString())["username"];
                id++;
            }
            ViewBag.form = form;
            ViewBag.to = to;
            ViewBag.action =action;
            return View(db);
        }
        [HttpPost]
        public IActionResult Display(string table, string id,string keywork,string subkey)
        {
            ConfigTable config = new ConfigTable();
            config.SetConfigTable(table,id);
            if (!cmd.ispermission(config.idmenu, "view", user))
            {
                return Redirect("/Home/Login");
            }
            ModelViews vmodel = new ModelViews();
            if (keywork == null)
                keywork = "";
            if (subkey == null)
                subkey = "";
            vmodel.initView(table, id, keywork.Trim().ToString(), subkey, config,0);
            
            if (vmodel.config.idmenu == null)
                return Redirect("/Home/Login");
            HttpContext.Session.Set<List<dynamic>>("sumdata", vmodel.data.sumdata);
            ViewBag.url = table;
            return View(vmodel);
        }
        public IActionResult Display(string table,string id)
        {
            ConfigTable config = new ConfigTable();
            config.SetConfigTable(table,id);
         
            if (!cmd.ispermission(config.idmenu, "view", user))
            {
                return Redirect("/Home/Login");
            }
            string page = Request.Query["page"];
            ViewBag.url = table;
            ViewBag.obj = config.name_;
            if (page == null || page =="1")
                page = "0";
            ModelViews vmodel = new ModelViews();
            vmodel.initView(table,id, "","", config,int.Parse(page)*Startup.maxrecord);
            if (int.Parse(page) <= 1)
            {
                HttpContext.Session.Set<List<dynamic>>("sumdata", vmodel.data.sumdata);
            }
            return View(vmodel);
        }
       
        public IActionResult CreateConfig(string table,string id)
        {
           
            ConfigTable cf = new ConfigTable();
            SqlTable sql = new SqlTable();
            if (table != null)
            cf.SetConfigTable(table,"");
            cf.ltable = sql.GetTableNames();
            return View(cf);
        }
        [HttpPost]
        public IActionResult CreateConfig(string table,string id,ConfigTable cf)
        {
            string str = "";
            if (cf.isscan == "true")
                cf.isscan = "1";
            else
                cf.isscan = "0";
            string[] clsql = new string[] { "name_", "titles", "atr_", "columns", "valid", "titledisplay", "coldisplay","key_", "seotab", "frendly_url", "pathupload", "typedisplay", "attributestab", "sortorder", "exportcl", "search", "subsearch", "dlonelinemobile", "headerline","isscan" };
            string[] vlsql = new string[] { cf.name_, cf.title, cf.kind, cf.column, cf.valid,cf.titleGv,cf.columnGv,cf.key_,cf.seotab,cf.url, cf.pathupload,cf.typedisplay, cf.attrtab,cf.sortorder, cf.export, cf.search ,cf.subsearch, cf.dlonelinemobile,cf.headerline,cf.isscan };
            var d = sql.finddata2("attribute_table", "where name_='" + cf.name_ + "' OR frendly_url='" + cf.url + "'");
            if (id != null && id != "")
            {
                if(d["id"].ToString()==id)
                    str = sql.updatesqlOneItemstr("attribute_table", clsql.ToList(), vlsql.ToList(), int.Parse(d["id"].ToString()), user.id);
                
            }
            else
            {
              
                if (d.Count<=0)
                    str = sql.insertsqlstr("attribute_table", clsql.ToList(), vlsql.ToList(), user.id);
            }
            if (str != "")
            {
                sql.runSQL(str);
                return RedirectToAction("Index", "Config");
            }
            return RedirectToAction();
        }
        public IActionResult Edit(string id)
        {

            return View();
        }
        private string checklogin()
        {
            var user = HttpContext.Session.Get<LoginModel>("user");
            if (user == null)
                return "-1";
            else
                return user.id;
        }
    }
}