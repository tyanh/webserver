﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Webdll.Models;
using Webserver.Models;

namespace Webserver.Controllers
{
    public class VietllotController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Index(string dayofweek)
        {
            Sqlcommon sql = new Sqlcommon();
            string now = DateTime.Now.ToString();
            string str = "";
            if (dayofweek != null)
                str = "select * from vietllot where name_='" + dayofweek + "'";
            else
                str = "select * from vietllot";
            var data = sql.getdataSQLstr(str+ " order by ngay ASC");
            

            List<string> s_ = new List<string>();
            foreach (var i in data)
            {
                for (var o = 1; o < 7; o++)
                {
                    s_.Add(i["so"+o].ToString());
                }
            }
            Dictionary<string, object> sum = new Dictionary<string, object>();
            sum["name_"] = "tổng hợp";
            var n = 1;
            List<Dictionary<string,object>> lis = new List<Dictionary<string, object>>();
            List<int> lisroot = new List<int>();
            for (var i=1;i<46;i++)
            {
                lisroot.Add(i);
                var a = s_.Count(s => s == i.ToString());
                if(a>0)
                {
                    Dictionary<string, object> tmp = new Dictionary<string, object>();
                    tmp["number"] = i.ToString();
                    tmp["count"] = a.ToString();
                    lis.Add(tmp);
                   
                }
            }
            List<Dictionary<string, object>> nodpl = new List<Dictionary<string, object>>();
            for (var g = 1; g < 46; g++)
            {
                var c = s_.Count(s =>s.ToString() == g.ToString());
                Dictionary<string, object> k = new Dictionary<string, object>();
                k["num"] = g;
                k["count"] = c;
                nodpl.Add(k);
            }
            lis = lis.OrderByDescending(s => int.Parse(s["count"].ToString())).Take(6).ToList();
            var lis2 = lis.OrderBy(s => int.Parse(s["count"].ToString())).Take(6).ToList();
            
            if (lis.Count > 0)
            {
                Dictionary<string, object> total = new Dictionary<string, object>();
                total["name_"] = "cao";
                total["ngay"] = now;
                for (var o = 0; o < 6; o++)
                {
                    total["so" + (o + 1)] = lis[o]["number"] + "(" + lis[o]["count"] + ")";
                }
                data.Add(total);

                Dictionary<string, object> total2 = new Dictionary<string, object>();
                total2["name_"] = "thap";
                total2["ngay"] = now;
                var dlis = nodpl.OrderBy(s => int.Parse(s["count"].ToString())).Take(6).ToList();
                for (var o = 0; o < 6; o++)
                {
                    total2["so" + (o + 1)] = dlis[o]["num"] + "(" + dlis[o]["count"] + ")";
                }
                data.Add(total2);
                Dictionary<string, object> total3 = new Dictionary<string, object>();
                total3["name_"] = "random";
                total3["ngay"] = now;
                Random random = new Random();
                List<int> iran = new List<int>();
                for (var o = 0; o < 6; o++)
                {
                    int start2 = 0;
                    
                    do
                    {
                        start2 = random.Next(0, lisroot.Count);
                        if (iran.IndexOf(start2) == -1)
                        {
                            total3["so" + (o + 1)] = lisroot[start2].ToString();
                            iran.Add(start2);
                            break;
                        }

                    }
                    while (iran.IndexOf(start2) != -1);
                }
                data.Add(total3);


            }
            return View(data);
        }
    }
}