﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Webserver.Models;

namespace Webserver.Controllers
{
    public class ConfigController : Controller
    {
        Sqlcommon sql = new Sqlcommon();
        public IActionResult Index()
        {
            DataList data = new DataList();
            data.data = sql.getdataSQL("attribute_table", "order by name_ desc");
            ViewBag.active = "Config";
            return View(data);
        }
    }
}