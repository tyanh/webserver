﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Renci.SshNet.Security.Cryptography;
using Webserver.Models;

namespace Webserver.Controllers
{
    [Route("api/[controller]")]
    public class sqlCtrController : Controller
    {
        [HttpGet("CreateTable")]
        public string CreateTable(string name)
        {
            var data = JsonConvert.DeserializeObject<dynamic>(name);
           var str= @"CREATE TABLE "+ data.name.ToString() + "(" +
                         "id int IDENTITY(1,1) PRIMARY KEY, " +
                         "createdate datetime, usercreate INT, lastupdate datetime, " +
                         "userupdate INT,status INT,name_ nvarchar(250)";
            if (data.seotab.ToString() == "1")
            {
                str = str + ",seotab nvarchar(MAX)";
            }
            if (data.subtab.ToString() == "1")
            {
                str = str + ",subtab nvarchar(MAX)";
            }
            str = str + ")";
            Sqlcommon sql = new Sqlcommon();
            sql.runSQL(str);
            List<string> upconfig = new List<string>() { "name_", "frendly_url" };
            List<string> vlconfig = new List<string>() { data.name.ToString(), data.name.ToString() };
            if (data.seotab.ToString() == "1")
            {
                upconfig.Add("seotab");
                vlconfig.Add("1");
            }
            if (data.subtab.ToString() == "1")
            {
                upconfig.Add("attributestab");
                vlconfig.Add("1");
            }
            Common cmd = new Common();
            string u = sql.insertsqlstr("attribute_table", upconfig, vlconfig, "1");
            sql.runSQL(u);
            return "0";
        }
        [HttpGet("Getcolumntable")]
        public string Getcolumntable(string table)
        {
            SqlTable sql = new SqlTable();
            var cl = sql.GetcolumnTableNames(table);
            return JsonConvert.SerializeObject(cl);
        }
        [HttpGet("Addcolumntable")]
        public string Addcolumntable(string table,string cl,string type)
        {
            string addstr = "ALTER TABLE " + table + " ADD " + cl + " " + type + "";
            Sqlcommon sql = new Sqlcommon();
            sql.runSQL(addstr);
            return "0";
        }
        [HttpGet("Deletetable")]
        public string Deletetable(string table)
        {
            string addstr = "DROP TABLE "+ table + "";
            Sqlcommon sql = new Sqlcommon();
            sql.runSQL(addstr);
            var strdlcg = "DELETE FROM attribute_table WHERE name_='" + table + "'";
            sql.runSQL(strdlcg);
            return "0";
        }
        [HttpGet("Upcolumntable")]
        public string Upcolumntable(string table, string cl, string type)
        {
            string addstr = "ALTER TABLE " + table + " ALTER COLUMN " + cl + " " + type + "";
            Sqlcommon sql = new Sqlcommon();
            sql.runSQL(addstr);
            return "0";
        }
        [HttpGet("Deletecolumntable")]
        public string Deletecolumntable(string table, string cl)
        {
            string addstr = "ALTER TABLE " + table + " DROP COLUMN " + cl;
            Sqlcommon sql = new Sqlcommon();
            sql.runSQL(addstr);
            return "0";
        }
    }
}