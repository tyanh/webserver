﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Webserver.Models;

namespace Webserver.Controllers
{
    public class MobileLayoutController : Controller
    {
      
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Login()
        {
            //ProcessStartInfo startinfo = new ProcessStartInfo();
            // startinfo.FileName = Path.Combine(@"C:\GitProject\winform\ConsoleApp1\ConsoleApp1\bin\Debug\app.publish\ConsoleApp1.exe");
            //startinfo.CreateNoWindow = false;
            //startinfo.UseShellExecute = false;
    
            //startinfo.Arguments = @"\\192.168.9.208\Public\phatht\Hinhf1Upf1WEBf1mới\Filef11 C:\GitProject\buildWebServer\wwwroot\images";
            //Process myProcess = Process.Start(startinfo);
            //myProcess.Start();
            //myProcess.WaitForExit();
            //ViewBag.tt = myProcess.ExitCode.ToString();
            //myProcess.Close();
            //myProcess.Dispose();
  
            return View();
        }
        [HttpPost]
        public IActionResult Login(string username, string pass)
        {
         
            Common cmd = new Common();
            Sqlcommon sql = new Sqlcommon();
            string pass_ = cmd.MD5(pass);
            var lg = sql.finddata2("workers", "where username='" + username + "' AND pass='" + pass_ + "' AND status<>1");
            if (lg.Count <= 0)
            {
                ViewBag.fail = "Username or password wrong";

            }
            else
            {
                var menu = sql.getdataSQL("menus", "");
                var pvl = JsonConvert.DeserializeObject<List<dynamic>>(lg["privilege"].ToString());
                var obj_mn = pvl.Where(s => s["view"].ToString() == "checked" || s["all"].ToString() == "checked").ToList();
                foreach(var j in obj_mn)
                {
                    string n_ = j["name"].ToString();
                    var mn = menu.FirstOrDefault(s => s["id"].ToString() == n_);
                    j["name_"] = mn["name_"].ToString();
                    j["link"] = mn["linkreal"].ToString();
                    j["icon"] = mn["icon"].ToString();
                    j["submenu"] = mn["submenu"].ToString();
                }
                obj_mn = obj_mn.Where(s => s["submenu"].ToString() == "").ToList();
                LoginModel login = new LoginModel();
                login.id = lg["id"].ToString();
                login.permistion = JsonConvert.SerializeObject(obj_mn);
                login.admin = lg["superadmin"].ToString();
                login.username = lg["username"].ToString();
                HttpContext.Session.Set<LoginModel>("user", login);

            }
            return RedirectToAction("Index", "MobileLayout");
        }
        [HttpPost]
        public IActionResult InvoicesInput(string items,string infor)
        {
            List<string> sqls = new List<string>();
            string code = HttpContext.Request.Query["code"];
            DataEdit edit = new DataEdit();
            Sqlcommon sql = new Sqlcommon();
            Common cmd = new Common();
            List<string> cl_in = new List<string>() { cmd.actionIV[1], cmd.actionIV[3], cmd.actionIV[6], cmd.actionIV[8] };
            List<string> sub_cl_in = new List<string>() { "qltinput", "moneyinput" };
            var users = HttpContext.Session.Get<LoginModel>("user");
            string user = users.id;
            string now = DateTime.Now.ToString();
            List<dynamic> it = JsonConvert.DeserializeObject<List<dynamic>>(items);
            List<Dictionary<string, object>> updateIt_ = new List<Dictionary<string, object>>();
            dynamic i = JsonConvert.DeserializeObject<dynamic>(infor);
            var newIv = cmd.CreatCode("input");
            string numbill = "IP-" + cmd.CreatNumberBill("billinput");
            string history = ""; string sqldelete = ""; string sqldelete_ = "";
            var lItem = sql.getdataSQL("products", "where " + cmd.strNotdelete() + "");
            var warehouseId_Old = "-1";
            if (code != null && code != "")
            {
        
                var o = sql.finddata2("billinput", "where codeid='" + code + "'");
                warehouseId_Old = o["warehouseId"].ToString();
                var w_ = sql.finddata2("warehouse", "where id='" + warehouseId_Old + "'");
                Dictionary<string, object> hoursert = new Dictionary<string, object>();
                hoursert["code"] = warehouseId_Old;
                hoursert["name"] = w_["name_"].ToString();
                hoursert["phone"] = w_["phone"].ToString();

                edit.data = JsonConvert.DeserializeObject<List<dynamic>>(o["detailbill"].ToString());
                newIv = o["codeid"].ToString();
                numbill = o["numberbill"].ToString();
                int suppId = -1;
                try { suppId =int.Parse(o["idcustomer"].ToString());}catch { }
                 history = sql.insert_htr_invoice("billinput", code, infor, o["ctminfor"].ToString(), items, o["detailbill"].ToString(), user, "edit");
                 sqldelete = "DELETE FROM billinput WHERE codeid='" + code + "'";
                 sqldelete_ = "DELETE FROM detailbillinput WHERE codebill='" + code + "'";
                foreach (var g in edit.data)
                {
                    List<double> sub_vl_in = new List<double>() {cmd.convertNumberDouble(g.quality.ToString())*(-1), cmd.convertNumberDouble(g.money.ToString()) * (-1) };
                    var str_inven = lItem.FirstOrDefault(s => s["id"].ToString() == g.idproduct.ToString());
                    var j_ = cmd.dnmInven(hoursert, str_inven, g.quality.ToString(), cmd.actionIV[3],sub_cl_in,sub_vl_in);
                    updateIt_.Add(j_);
                }

            }
            Dictionary<string, object> hourse = new Dictionary<string, object>();
            hourse["code"] = i.warehouse.ToString();
            hourse["name"] = i.warehouse_name.ToString();
            hourse["phone"] = i.warehouse_phone.ToString();
            var supplier = sql.finddata2("supplier", "where id='" + i.supplier.ToString() + "' AND (" + cmd.strNotdelete() + ")");
            int idsupplier = -1;
            
            foreach (var o in it)
            {
                var c = lItem.FirstOrDefault(s => s["barcode"].ToString() == o.code.ToString());
                if (c == null)
                    return View();
                double money = o.qlt * c["priceinput"];
                o.priceproduct = c["priceinput"];
                o.nameproduct = c["name_"];
                o.money = money;
                o.idproduct = c["id"];
                o.idcustomer = idsupplier;
                o.codebill = newIv;
                o.quality = o.qlt;
                o.datebill = now;
                o.codeid = numbill;
                o.img = c["photo"]; ;
                o.warehouseId = i.warehouse.ToString();
                List<string> clit = new List<string>() { "idcustomer", "quality", "datebill", "priceproduct", "money", "codebill", "idproduct", "nameproduct", "warehouseId", "codeid" };
                List<string> vlit = new List<string>();
                vlit = cmd.CreateListValue(o, clit);
                double qllty = cmd.convertNumberDouble(o.qlt.ToString());

                var returnIt_child = updateIt_.FirstOrDefault(s => s["idproduct"].ToString()== c["id"].ToString());
                if (warehouseId_Old == i.warehouse.ToString())
                {
                    if (returnIt_child != null)
                    {
                        returnIt_child[cmd.actionIV[1]] = qllty.ToString();
                        double qltinput_ = cmd.convertNumberDouble(qllty.ToString());
                        returnIt_child["qltinput"] = cmd.convertNumberDouble(returnIt_child["qltinput"].ToString()) + qltinput_;
                        returnIt_child["moneyinput"] = cmd.convertNumberDouble(returnIt_child["moneyinput"].ToString()) + money;
                    }
                    else
                    {
                        List<double> sub_vl_in = new List<double>() { qllty, money };
                        var j_ = cmd.dnmInven(hourse, c, qllty.ToString(), cmd.actionIV[1], sub_cl_in, sub_vl_in);
                        updateIt_.Add(j_);
                    }
                }
                else
                {
                    List<double> sub_vl_in = new List<double>() { qllty, money };
                    var j_ = cmd.dnmInven(hourse, c, qllty.ToString(), cmd.actionIV[1], sub_cl_in, sub_vl_in);
                    updateIt_.Add(j_);
                }
            
                string sqlit = sql.insertsqlstr("detailbillinput", clit, vlit, user);
                sqls.Add(sqlit);
            }
            var updateIt_2 = updateIt_.GroupBy(s => s["idproduct"].ToString()).ToList();
            
            foreach (var y in updateIt_2)
            {
                var t = y.ToList();
                var p = (Dictionary<string, object>)t.FirstOrDefault()["product"];
                double sumremain = 0;
                double suminput = 0;
                string warehouse = p["inventoryhouse"].ToString();
                List<Dictionary<string, object>> warehouses = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(warehouse);
                if (warehouses == null)
                    warehouses = new List<Dictionary<string, object>>();
                
                foreach (var j in t)
                {
                    var i_ = warehouses.FirstOrDefault(s => s["id"].ToString() == j["warehouseId"].ToString());
                    var wh = (Dictionary<string, object>)j["warehouse"];
                    if (i_ == null)
                    {
                        i_ = new Dictionary<string, object>();
                        i_["id"] = j["warehouseId"].ToString();
                        i_["name"] = wh["name"].ToString();
                        foreach (var h in cl_in)
                        {
                            i_[h] = "0";
                        }
                        warehouses.Add(i_);
                    }
                    foreach (var h in cl_in)
                    {
                        string v = "0";
                        try {
                            v = j[h].ToString();
                            v = (cmd.convertNumberDouble(v) + cmd.convertNumberDouble(i_[h].ToString())).ToString();
                            i_[h] = v;
                        } catch { }
                        
                    }
                    double rm = cmd.calcuremainProduct(i_);
                    i_["remain"] = rm;
                    
                }
     
                double summoney = 0;
                sumremain = warehouses.Sum(s=>cmd.convertNumberDouble(s["remain"].ToString()));
                suminput = warehouses.Sum(s => cmd.convertNumberDouble(s["input"].ToString()))  - warehouses.Sum(s => cmd.convertNumberDouble(s["returninput"].ToString()));
                summoney = suminput * cmd.convertNumberDouble(p["priceinput"].ToString());
                warehouse = JsonConvert.SerializeObject(warehouses);
                string sstr = cmd.strinventory_input(warehouse, sumremain.ToString(),int.Parse(p["id"].ToString()), user, suminput.ToString(), summoney.ToString());
                sqls.Add(sstr);
            }
            
            double sumqlt = it.Sum(s => s["quality"]);
            double moneypay = cmd.convertNumberDouble(i.pay.ToString());
            double sumbill = it.Sum(s => s["money"]);
            double sumdiscounttmp = cmd.convertNumberDouble(i.gift.ToString());
            double moneyiv = sumbill - sumdiscounttmp;
            double returnmn = moneyiv - moneypay;
            i.sumbill = sumbill.ToString();
            i.moneyiv = returnmn.ToString();
            i.sumdiscount = sumdiscounttmp.ToString();
            i.payment_money = moneypay.ToString();
            double dert = returnmn;
            if (dert < 0)
                dert = 0;
            i.debt = dert.ToString();
            i.usercreate = users.username;
            if (supplier.Count > 0)
            {
                idsupplier = int.Parse(supplier["id"].ToString());
                i.namectm = supplier["name_"].ToString();
                                
                //double summnsupplier =  cmd.convertNumberDouble(supplier["summoney"].ToString()) + moneyiv;
                //double sumdebtsupplier = cmd.convertNumberDouble(supplier["debt"].ToString()) + dert;
                //List<string> clsupplier = new List<string>() { "summoney", "debt" };
                //List<string> vlsupplier = new List<string>() { summnsupplier.ToString(), sumdebtsupplier.ToString() };
                //string upsupplies = sql.updatesqlOneItemstr("supplier", clsupplier, vlsupplier, int.Parse(supplier["id"].ToString()), user);
                //sqls.Add(upsupplies);
            }

            List<string> clbill = new List<string>() { "idcustomer", "remark", "notediscount", "detailbill", "numberbill", "datebill", "money", "codeid", "discountmoney", "moneynotdiscount", "payment_money", "debt", "ctminfor", "warehouseId", "qlt" };
            string strdtb1 = JsonConvert.SerializeObject(it);
            string strdtb2 = JsonConvert.SerializeObject(i);
            List<string> vlbill = new List<string>();
            vlbill.Add(supplier["id"].ToString());
            vlbill.Add(i.notebill.ToString());
            vlbill.Add(i.notegift.ToString());
            vlbill.Add(strdtb1);
            vlbill.Add(numbill);
            vlbill.Add(now.ToString());
            vlbill.Add(moneyiv.ToString());
            vlbill.Add(newIv);
            vlbill.Add(sumdiscounttmp.ToString());
            vlbill.Add(sumbill.ToString());
            vlbill.Add(moneypay.ToString());
            vlbill.Add(dert.ToString());
            vlbill.Add(strdtb2);
            vlbill.Add(i.warehouse.ToString());
            vlbill.Add(sumqlt.ToString());
            string sqlbill = sql.insertsqlstr("billinput", clbill, vlbill, user);
            sqls.Insert(0, sqlbill);
            if(history!="")
            {
                sqls.Insert(0,history);
                sqls.Insert(0, sqldelete);
                sqls.Insert(0, sqldelete_);
            }
            sql.runSQL(string.Join(";", sqls));
            return RedirectToAction("InvoicesInput", "MobileLayout", new { code = code });
        }
        public IActionResult InvoicesInput() {
            Common cmd = new Common();
            Sqlcommon sql = new Sqlcommon();
            DataEdit edit = new DataEdit();
 
            string code = HttpContext.Request.Query["code"];
            if(code!=null && code != "")
            {
                edit.code = code;
                var o = sql.finddata2("billinput", "where codeid='" + code + "'");
                string bill = o["ctminfor"].ToString();
                var b_ = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(bill);
                b_["datebill"]= DateTime.Parse(o["datebill"].ToString()).ToString("dd-MM-yyyy HH:MM");
                b_["numberbill"] = o["numberbill"].ToString();
                edit.bill = b_;
                edit.data = JsonConvert.DeserializeObject<List<dynamic>>(o["detailbill"].ToString());
            }
            
            ViewBag.supplier = sql.getdataSQL("supplier", "where " + cmd.strNotdelete());
            ViewBag.whouse = sql.getdataSQL("warehouse", "where " + cmd.strNotdelete());
            return View(edit);
        }
        public IActionResult Viewtransfer()
        {
            Sqlcommon sql = new Sqlcommon();
            DataList data = new DataList();
            data.data = sql.getdataSQL("billtransfer", "").OrderByDescending(s => s["id"]).ToList();
            return View(data);
        }
        public IActionResult CheckInvoicestransfer()
        {
            Common cmd = new Common();
            Sqlcommon sql = new Sqlcommon();
            DataEdit edit = new DataEdit();
            string code = HttpContext.Request.Query["code"];
           
                edit.code = code;
                var o = sql.finddata2("billtransfer", "where codeid='" + code + "'");
                string bill = o["ctminfor"].ToString();
                var b_ = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(bill);
                b_["datebill"] = DateTime.Parse(o["datebill"].ToString()).ToString("dd-MM-yyyy HH:MM");
                b_["numberbill"] = o["numberbill"].ToString();
                edit.bill = b_;
                edit.data = JsonConvert.DeserializeObject<List<dynamic>>(o["detailbill"].ToString());
     
            return View(edit);
        }
        [HttpPost]
        public IActionResult CheckInvoicestransfer(string items,string note)
        {
            Common cmd = new Common();
            Sqlcommon sql = new Sqlcommon();
            DataEdit edit = new DataEdit();
            string code = HttpContext.Request.Query["code"];
            var users = HttpContext.Session.Get<LoginModel>("user");
            string user = users.id;
            edit.code = code;
            var o = sql.finddata2("billtransfer", "where codeid='" + code + "'");
            string bill = o["ctminfor"].ToString();
            var b_ = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(bill);
            b_["datebill"] = DateTime.Parse(o["datebill"].ToString()).ToString("dd-MM-yyyy HH:MM");
            b_["numberbill"] = o["numberbill"].ToString();
            edit.bill = b_;
            edit.data = JsonConvert.DeserializeObject<List<dynamic>>(o["detailbill"].ToString());
            if (items != null)
            {
                List<string> sqls = new List<string>();
                var o_ = sql.getdataSQL("detailbilltransfer", "where codebill='" + code + "'");
                var pr_ = sql.getdataSQL("products","where "+cmd.strNotdelete());
                var items_tr = JsonConvert.DeserializeObject<List<dynamic>>(o["detailbill"].ToString());
                List<string> cl_up_tran = new List<string>() {cmd.actionTranf[0], cmd.actionTranf[1] };
                List<string> cl_up_rcv = new List<string>() { cmd.actionTranf[2], cmd.actionTranf[3] };
                string[] codes = items.Split(',');
                foreach(var i in codes)
                {
                    List<Dictionary<string, object>> updateIt_ = new List<Dictionary<string, object>>();
                    var data_str = edit.data.FirstOrDefault(s => s["code"].ToString() == i);
                    var data_str_item = items_tr.FirstOrDefault(s => s["code"].ToString() == i);
                    data_str_item.ischecked = "1";
                    data_str.ischeck = "1";
                    var pr_str = pr_.FirstOrDefault(s => s["barcode"].ToString() == i);
                    var wh_ = JsonConvert.DeserializeObject<List<dynamic>>(pr_str["inventoryhouse"].ToString());
                    if (wh_ == null)
                        wh_ = new List<dynamic>();
                    var i_tr = wh_.FirstOrDefault(s => s["id"].ToString() == o["idcustomer"].ToString());
                    var i_rcv = wh_.FirstOrDefault(s => s["id"].ToString() == o["warehouseId"].ToString());
                    var o_str = o_.FirstOrDefault(s => s["id"].ToString() == pr_str["id"].ToString());
                    double qlt = cmd.convertNumberDouble(o_str["quality"].ToString());
                    double money = cmd.convertNumberDouble(o_str["money"].ToString());
                    double remain_tr = cmd.convertNumberDouble(o_str["quality"].ToString());
                    double remain_rev = cmd.convertNumberDouble(o_str["quality"].ToString());
                    if (i_tr == null)
                    {
                        i_tr = new System.Dynamic.ExpandoObject();
                        wh_.Add(i_tr);

                    }
                    else
                    {
                        try
                        {
                            qlt = qlt + cmd.convertNumberDouble(i_tr[cmd.actionTranf[0]].ToString());
                            money = money + cmd.convertNumberDouble(i_tr[cmd.actionTranf[1]].ToString());
                            remain_tr = cmd.convertNumberDouble(i_tr["remain"].ToString()) - remain_tr;
                        }
                        catch { }
                    }
                    i_tr[cmd.actionTranf[0]] = qlt;
                    i_tr[cmd.actionTranf[1]] = money;
                    i_tr["remain"] = remain_tr;
                    if (i_rcv == null)
                    {
                        i_rcv = new System.Dynamic.ExpandoObject();
                        wh_.Add(i_rcv);

                    }
                    else
                    {
                        try
                        {
                            qlt = qlt + cmd.convertNumberDouble(i_rcv[cmd.actionTranf[2]].ToString());
                            money = money + cmd.convertNumberDouble(i_rcv[cmd.actionTranf[3]].ToString());
                            remain_rev = cmd.convertNumberDouble(i_rcv["remain"].ToString()) + remain_tr;
                        }
                        catch { }
                    }
                    i_rcv[cmd.actionTranf[2]] = qlt;
                    i_rcv[cmd.actionTranf[3]] = money;
                    i_rcv["remain"] = remain_rev;
                    string str_wh = JsonConvert.SerializeObject(wh_);
                    string where1 = "where id=" + pr_str["id"] + "";
                    string str_p = sql.updatesqlcolumn("products", new List<string>() { "inventoryhouse" }, new List<string>() { str_wh }, where1, user);
                    sqls.Add(str_p);
                    string where_tr = "where id=" + o_str["id"] + "";
                    string str_tr_it = sql.updatesqlcolumn("detailbilltransfer", new List<string>() { "ischecked" }, new List<string>() { "1" }, where_tr, user);
                    sqls.Add(str_tr_it);
                    
                }
                string where2 = "where codeid='" + code + "'";
                string str_tr = sql.updatesqlcolumn("billtransfer", new List<string>() { "ischecked", "notecheck", "detailbill" }, new List<string>() { "1",note,JsonConvert.SerializeObject(items_tr) }, where2, user);
                sqls.Add(str_tr);
                sql.runSQL(string.Join(";", sqls));
            }
            return View(edit);
        }
        public IActionResult Invoicestransfer()
        {
            Common cmd = new Common();
            Sqlcommon sql = new Sqlcommon();
            DataEdit edit = new DataEdit();
            string code = HttpContext.Request.Query["code"];
            if (code != null && code != "")
            {
                edit.code = code;
                var o = sql.finddata2("billtransfer", "where codeid='" + code + "'");
                string bill = o["ctminfor"].ToString();
                var b_ = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(bill);
                b_["datebill"] = DateTime.Parse(o["datebill"].ToString()).ToString("dd-MM-yyyy HH:MM");
                b_["numberbill"] = o["numberbill"].ToString();
                edit.bill = b_;
                edit.data = JsonConvert.DeserializeObject<List<dynamic>>(o["detailbill"].ToString());
            }
            var wh_ = sql.getdataSQL("warehouse", "where " + cmd.strNotdelete());
            ViewBag.supplier = wh_;
            ViewBag.whouse = wh_;
            return View(edit);
        }
        [HttpPost]
        public IActionResult Invoicestransfer(string items, string infor)
        {
            List<string> sqls = new List<string>();
            var users = HttpContext.Session.Get<LoginModel>("user");
            string user = users.id;
            string now = DateTime.Now.ToString();
            Common cmd = new Common();
            Sqlcommon sql = new Sqlcommon();
            List<dynamic> it = JsonConvert.DeserializeObject<List<dynamic>>(items);
            dynamic i = JsonConvert.DeserializeObject<dynamic>(infor);
            var lItem = sql.getdataSQL("products", "where " + cmd.strNotdelete() + "");
            //var warehouses = sql.getdataSQL("warehouse", "where " + cmd.strNotdelete() + "");
            var newIv = cmd.CreatCode("transfer");
            string numbill = "IP-" + cmd.CreatNumberBill("billtransfer");

            foreach (var o in it)
            {
                
                var c = lItem.FirstOrDefault(s => s["barcode"].ToString() == o.code.ToString());
                if (c == null)
                    return View();
                double money = o.qlt * c["priceinput"];
                o.priceproduct = c["priceinput"];
                o.nameproduct = c["name_"];
                o.money = money;
                o.idproduct = c["id"];
                o.idcustomer = i.supplier.ToString();
                o.codebill = newIv;
                o.quality = o.qlt;
                o.datebill = now;
                o.codeid = numbill;
                o.img = c["photo"]; ;
                o.warehouseId = i.warehouse.ToString();
                List<string> clit = new List<string>() { "idcustomer", "quality", "datebill", "priceproduct", "money", "codebill", "idproduct", "nameproduct", "warehouseId", "codeid" };
                List<string> vlit = new List<string>();
                vlit = cmd.CreateListValue(o, clit);

                string sqlit = sql.insertsqlstr("detailbilltransfer", clit, vlit, user);
                sqls.Add(sqlit);

            }
            double sumqlt = it.Sum(s => s["quality"]);
            double summoney = it.Sum(s => s["money"]);
            i.sumbill = summoney.ToString();
            i.usercreate = users.username;
            List<string> clbill = new List<string>() { "idcustomer", "remark","detailbill", "numberbill", "datebill", "money", "codeid","ctminfor", "warehouseId", "qlt" };
            string strdtb1 = JsonConvert.SerializeObject(it);
            string strdtb2 = JsonConvert.SerializeObject(i);
            List<string> vlbill = new List<string>() { i.supplier.ToString(), i.notebill.ToString(), strdtb1,numbill,now, summoney.ToString(),newIv, strdtb2, i.warehouse.ToString(), sumqlt.ToString() };

            string sqlbill = sql.insertsqlstr("billtransfer", clbill, vlbill, user);
            sqls.Insert(0, sqlbill);
            
            sql.runSQL(string.Join(";", sqls));
            return RedirectToAction("Invoicestransfer", "MobileLayout", new { code = "" });

        }
        public IActionResult InvoicesReturn()
        {
            Common cmd = new Common();
            var user = HttpContext.Session.Get<LoginModel>("user");
           
            List<Dictionary<string, object>> wh = new List<Dictionary<string, object>>();
            wh = cmd.getwarehousesale(user.id.ToString(), int.Parse(user.admin.ToString()));
            if (wh.Count <= 0)
                return RedirectToAction("pagealert", "MobileLayout", new { note = "User không có quyền" });
            headerinvoiceModel head = new headerinvoiceModel();
            head.nameus = user.username.ToString();
            head.idus = user.id.ToString();
            head.wareh = wh;
            head.invoicenumber = "MNC" + cmd.CreatNumberBill("billreturns");
            
            return View(head);
        }
        public IActionResult InvoicesSale()
        {
            Common cmd = new Common();
            var user = HttpContext.Session.Get<LoginModel>("user");

            List<Dictionary<string, object>> wh = new List<Dictionary<string, object>>();
                  wh= cmd.getwarehousesale(user.id.ToString(),int.Parse(user.admin.ToString()));
          
            if (wh.Count <= 0)
                return RedirectToAction("pagealert", "MobileLayout", new { note = "User không có quyền" });
           
            headerinvoiceModel head = new headerinvoiceModel();
            head.nameus = user.username.ToString();
            head.idus = user.id.ToString();
            head.wareh = wh;
            head.invoicenumber ="MNC"+cmd.CreatNumberBill("billsales");
            return View(head);
        }
        public IActionResult Viewremove()
        {
            Sqlcommon sql = new Sqlcommon();
            DataList data = new DataList();
            data.data = sql.getdataSQL("billremove", "").OrderByDescending(s => s["id"]).ToList();
            return View(data);
        }
        public IActionResult InvoicesRemove()
        {

            Common cmd = new Common();
            var user = HttpContext.Session.Get<LoginModel>("user");
            List<Dictionary<string, object>> wh = new List<Dictionary<string, object>>();
            wh = cmd.getwarehousesale(user.id.ToString(), int.Parse(user.admin.ToString()));

            if (wh.Count <= 0)
                return RedirectToAction("pagealert", "MobileLayout", new { note = "User không có quyền" });

            headerinvoiceModel head = new headerinvoiceModel();
            head.nameus = user.username.ToString();
            head.idus = user.id.ToString();
            head.wareh = wh;
            head.invoicenumber = "MNC" + cmd.CreatNumberBill("billremove");
            return View(head);
        }
        [HttpPost]
        public IActionResult InvoicesRemove(string items, string infor)
        {
            List<string> sqls = new List<string>();
            Common cmd = new Common();
            Sqlcommon sql = new Sqlcommon();
            var user = HttpContext.Session.Get<LoginModel>("user");
            List<Dictionary<string, object>> wh = new List<Dictionary<string, object>>();
            wh = cmd.getwarehousesale(user.id.ToString(), int.Parse(user.admin.ToString()));

            if (wh.Count <= 0)
                return RedirectToAction("pagealert", "MobileLayout", new { note = "User không có quyền" });
            List<dynamic> it = JsonConvert.DeserializeObject<List<dynamic>>(items);
            dynamic i = JsonConvert.DeserializeObject<dynamic>(infor);
            i.user = user.username;
            var lItem = sql.getdataSQL("products", "where " + cmd.strNotdelete() + "");
            var newIv = cmd.CreatCode("remove");
            string now = DateTime.Now.ToString();
            string numbill = "RM-" + cmd.CreatNumberBill("billremove");

            foreach (var o in it)
            {

                var c = lItem.FirstOrDefault(s => s["barcode"].ToString() == o.code.ToString());
                if (c == null)
                    return View();
                double money = o.qlt * c["priceinput"];
                o.priceproduct = c["priceinput"];
                o.nameproduct = c["name_"];
                o.money = money;
                o.idproduct = c["id"];
                o.codebill = newIv;
                o.quality = o.qlt;
                o.datebill = now;
                o.codeid = numbill;
                o.img = c["photo"]; ;
                o.warehouseId = i.code.ToString();
                List<string> clit = new List<string>() { "quality", "datebill", "priceproduct", "money", "codebill", "idproduct", "nameproduct", "warehouseId", "codeid" };
                List<string> vlit = new List<string>();
                vlit = cmd.CreateListValue(o, clit);
                string sqlit = sql.insertsqlstr("detailbillremove", clit, vlit, user.id);
                sqls.Add(sqlit);
                dynamic wh_ = new System.Dynamic.ExpandoObject();
                wh_.id = i.code.ToString();
                wh_.name = i.name.ToString();
                string str_iv = cmd.Edit_Inventory_json(c["inventoryhouse"].ToString(), new List<string>() { cmd.actionTranf[4], cmd.actionTranf[5] }, new List<string>() { o.qlt.ToString(), money.ToString()}, wh_);
                double qlt_nb = cmd.convertNumberDouble(o.qlt.ToString());
                double a1 =cmd.convertNumberDouble(c["inventory"].ToString())- qlt_nb;
                double a2 = cmd.convertNumberDouble(c["qltexport"].ToString())+ qlt_nb;
                double b1 = cmd.convertNumberDouble(c["moneyexport"].ToString())+money;
                string str_up_pr = sql.updatesqlcolumn("products",
                    new List<string>() { "inventory", "qltexport", "moneyexport", "inventoryhouse" },
                    new List<string>() { a1.ToString(), a2.ToString(), b1.ToString(), str_iv }, "where id=" + c["id"] + "", user.id
                    );
                sqls.Add(str_up_pr);
            }
            double sumqlt = it.Sum(s => s["quality"]);
            double summoney = it.Sum(s => s["money"]);
            List<string> clbill = new List<string>() {"remark", "detailbill", "numberbill", "datebill", "money", "codeid", "ctminfor", "warehouseId", "qlt" };
            string strdtb1 = JsonConvert.SerializeObject(it);
            string strdtb2 = JsonConvert.SerializeObject(i);
            List<string> vlbill = new List<string>() { i.note.ToString(), strdtb1, numbill, now, summoney.ToString(), newIv, strdtb2, i.code.ToString(), sumqlt.ToString() };

            string sqlbill = sql.insertsqlstr("billremove", clbill, vlbill, user.id);
            sqls.Insert(0, sqlbill);

            sql.runSQL(string.Join(";", sqls));
            return RedirectToAction("InvoicesRemove", "MobileLayout", new { code = "" });

        }
        public IActionResult pagealert ()
        {
            string note = HttpContext.Request.Query["note"];
            ViewBag.note = note.ToString();
            return View();
        }
        public IActionResult ViewCommon()
        {
            string note = HttpContext.Request.Query["ext"];
            ViewBag.note = note.ToString();
            return View();
        }
      
        public IActionResult products()
        {
            Sqlcommon sql = new Sqlcommon();
            DataList data = new DataList();
            data.data = sql.getdataSQL("products", "");
            return View(data);
        }
        [HttpPost]
        public IActionResult products(string key)
        {
            Sqlcommon sql = new Sqlcommon();
            DataList data = new DataList();
            data.data = sql.getdataSQL("products", "where barcode LIKE '%"+key+ "%' OR name_ LIKE '%" + key + "%'");
            data.key = key;
            return View(data);
        }
        public IActionResult ViewInvoicesInput()
        {
            Sqlcommon sql = new Sqlcommon();
            DataList data = new DataList();
            data.data = sql.getdataSQL("billinput", "").OrderByDescending(s => s["id"]).ToList();
            return View(data);
        }
        
        public IActionResult Invoices()
        {
            Sqlcommon sql = new Sqlcommon();
            DataList data = new DataList();
            string view= HttpContext.Request.Query["view"];
            if(view=="sale")
                data.data = sql.getdataSQL("billsales", "").OrderByDescending(s=>s["id"]).ToList();
            else if (view == "return")
                data.data = sql.getdataSQL("billreturns", "").OrderByDescending(s => s["id"]).ToList();
            ViewBag.view = view;
            return View(data);
        }
        [HttpPost]
        public IActionResult Invoices(string key,string begin, string today,string view)
        {
            Common cmd = new Common();
            Sqlcommon sql = new Sqlcommon();
            DataList data = new DataList();
            data.key = key;
            data.fromday = begin;
            data.today = today;
            int ctmId = -1;
            int user = -1;
            int h = -1;
            
            if (key != null)
            {
                var ctm = sql.finddata2("customers", "where name_ LIKE N'%" + key + "%' OR phone LIKE N'%" + key + "%'");
                if (ctm.Count > 0)
                    ctmId = int.Parse(ctm["id"].ToString());
                var wk = sql.finddata2("workers", "where name_ LIKE N'%" + key + "%' OR username LIKE N'%" + key + "%'");
                if (wk.Count > 0)
                    user = int.Parse(ctm["id"].ToString());
                var whouse = sql.finddata2("warehouse", "where name_ LIKE N'%" + key + "%'");
                if (whouse.Count > 0)
                    h = int.Parse(whouse["id"].ToString());
            }
                string w = "where (idcustomer=" + ctmId + " OR usercreate=" + user + " OR warehouseId=" + h + " OR numberbill LIKE '%" + key + "%')";
                string d = "";
                if (begin != null)
                    d = "datebill>='" + cmd.convertDate(begin) + "'";
                if (today != null)
                {
                    DateTime t = DateTime.Parse(cmd.convertDate(today)).AddDays(1);
                if (d != "")
                    d = d + " AND datebill>='" + t + "'";
                else
                    d = "datebill >= '" + t + "'";
                }
            if (d != "")
                w = w + " AND (" + d + ")";
            if(view=="sale")
                data.data = sql.getdataSQL("billsales", w).OrderByDescending(s => s["id"]).ToList();
            if (view == "return")
                data.data = sql.getdataSQL("billreturns", w).OrderByDescending(s => s["id"]).ToList();
            return View(data);
        }
       

    }
}