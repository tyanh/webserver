﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Webserver.Models;

namespace Webserver.Controllers
{
   [Route("api/[controller]")]
    public class ChatbootApiController : Controller
    {
        [HttpGet("Addkeywork")]
        public string Addkeywork(string key)
        {
            var users = HttpContext.Session.Get<LoginModel>("user");
            string user = users.id;
            Sqlcommon sqlcm = new Sqlcommon();
            sqlchat sql = new sqlchat();
            var keyen = sql.removeVn(key);
            var c = sql.checkdataExit("keyworks", "where keywork='" + keyen + "'");
            if (c)
            {
                Common cmd = new Common();
                string code = cmd.CreatCode("key");
                string str = sqlcm.insertsqlstr("keyworks", new List<string>() { "keywork", "keyworkvn", "code" }, new List<string>() { keyen, key, code }, user);
                sql.runSQL(str);
                return code;
            }
            return "-1";
        }
        [HttpPost("SaveReplay")]
        public string SaveReplay(string key)
        {
            var users = HttpContext.Session.Get<LoginModel>("user");
            string user = users.id;
            Sqlcommon sqlcm = new Sqlcommon();
            sqlchat sql = new sqlchat();
            Common cmd = new Common();
            dynamic d = JsonConvert.DeserializeObject<dynamic>(key);
            string[] k = d.keys.ToString().Split(',');
            string code = cmd.CreatCode("rp");
            string name = d.name.ToString();
            var c_ = sql.finddata2("replys","where name_='"+ d.name.ToString() + "'");
            List<string> lstr = new List<string>();
            string strinsert = "";
            if (c_.Count > 0)
            {
                if (c_["code"].ToString() == d.iddata.ToString())
                {
                    code = c_["code"].ToString();
                    strinsert = sqlcm.updatesqlcolumn("replys",
                    new List<string>() { "contents", "code", "isproduct", "status" },
                    new List<string>() { d.str.ToString(), code, d.isproduct.ToString(), d.status.ToString() },
                    "where code='" + d.iddata.ToString() + "'",
                    user);
                }
                else
                {
                    return "-1";
                }
            }
            else
            {
                strinsert = sqlcm.insertsqlstr("replys", new List<string>() { "name_", "contents", "code", "isproduct" },
                    new List<string>() { d.name.ToString(), d.str.ToString(), code, d.isproduct.ToString() }, user);
            }
                 lstr.Add(strinsert);



            foreach (var i in k)
            {
                string str_ = sqlcm.updatesqlcolumn("keyworks",new List<string>() { "coderl" }
                , new List<string>() { code },"where code='"+i+"'",user
                );
                lstr.Add(str_);
            }
            sql.runSQL(string.Join(";", lstr));
            return code;
        }
    }
}