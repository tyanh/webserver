﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Webserver.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Webserver.Controllers
{
    [Route("api/[controller]")]
    public class ChatsController : Controller
    {
        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        [HttpGet("GetListChat")]
        public string GetListChat()
        {
            ChatServer l = new ChatServer();
            var d = l.getdataSQL("loginserver", "");
            return JsonConvert.SerializeObject(d);
        }
        [HttpGet("ChatJoin")]
        public string ChatJoin(string data)
        {
            ChatServer join = new ChatServer();
            return join.JoinGroup(data).ToString();
        }
        [HttpGet("Chatlogout")]
        public string Chatlogout(string iduser)
        {
            ChatServer c = new ChatServer();
            return c.outGroup(iduser);
        }
        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
