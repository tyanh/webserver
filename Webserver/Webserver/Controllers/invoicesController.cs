﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Webserver.Models;

namespace Webserver.Controllers
{
    public class invoicesController : Controller
    {
        Sqlcommon sql = new Sqlcommon();
        public IActionResult Sales()
        {
            DataList product = new DataList();
            product.data = sql.getdataSQL("products", "where status<>1");
            return View(product);
        }
    }
}