﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Webserver.Models;
using Microsoft.AspNetCore.Session;
using Newtonsoft.Json;
using System.Net;
using Microsoft.AspNetCore.Http;
using DocumentFormat.OpenXml.Drawing.Charts;

namespace Webserver.Controllers
{
    public class HomeController : Controller
    {
        
        Sqlcommon sql = new Sqlcommon();
        public IActionResult Index()
        {
            ReportModel report = new ReportModel();
            string now = DateTime.Now.ToString("dd-MM-yyyy");
            report.initdata(now, now, "");
            return View(report);
        }
        [HttpPost]
        public IActionResult Index(string datefrom, string dateto, string wheahouse)
        {
            ReportModel report = new ReportModel();
            Common cmd = new Common();
            report.initdata(datefrom, dateto, wheahouse);
            return View(report);
        }
            public IActionResult Export()
        {

            return View();
        }
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";
            LoginModel a = new LoginModel();
            a.username = "11";
            a.permistion = "";
            HttpContext.Session.Set("user",a);
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult Login()
        {
            
            HttpContext.Session.Set<LoginModel>("userlogin", null);
           
            FacebookController cmd = new FacebookController();
                string code = HttpContext.Request.Query["code"];
            if (code != null)
            {
                 cmd.GetAccounts(code);
            }
           
            return View();
        }
        [HttpPost]
        public IActionResult Login(LoginModel login)
        {
            Common cmd = new Common();
            string pass = cmd.MD5(login.pass);
            string wherestr = "where username='" + login.username + "' AND pass='" + pass + "' AND (" + cmd.strNotdelete() + ")";
            var lg = sql.finddata2("workers", wherestr);
            if (lg.Count<=0)
            {
                ViewBag.fail = "Username or password wrong";
                return View();
            }
            else
            {
                login.id = lg["id"].ToString();
                login.permistion="";
                login.admin= lg["superadmin"].ToString();
                if(lg["superadmin"].ToString()!="1")
                {
                    var g = sql.finddata2("groupmanager",  "where id ='"+ lg["groups"].ToString() + "'");
                    if(g.Count<=0)
                        return RedirectToAction("Login", "Home");
                    if (g["allpms"].ToString() == "1")
                        login.admin = "1";
                    login.permistion = g["permission"].ToString();
                    login.groupid = g["id"].ToString();
                }
                var iduser_ = "workers" + login.id;
                var groupid = "groupmanager" + login.groupid;
                if (Startup.userupdate.IndexOf(iduser_) != -1 || Startup.userupdate.IndexOf(groupid) != -1)
                {
                    Startup.userupdate.Remove(iduser_);
                    Startup.userupdate.Remove(groupid);
                }
                    HttpContext.Session.Set<LoginModel>("userlogin", login);
                HttpContext.Session.Set<string>("ssinvoice", cmd.CreatCode(""));
                return RedirectToAction("Index", "Home");
            }
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
