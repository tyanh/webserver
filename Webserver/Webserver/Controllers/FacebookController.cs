﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Facebook;
namespace Webserver.Controllers
{
    public class FacebookController : Controller
    {
        private string Url= "https://graph.facebook.com/";
        string PageId = "122825785787006";
        private static readonly HttpClient client = new HttpClient();
        string AppId = "280044195490470";
        string AppSecret = "e129782115f34029ff7582a8a1c9acea";
        public IActionResult FacebookLogin()
        {
    
            var authProperties = new AuthenticationProperties
            {
                RedirectUri = "/"
            };
            return Challenge(authProperties, "Facebook");
        }
        public void GetAccounts(string code)
        {
            var client = new FacebookClient();  // Make the Get request to the facebook

            dynamic result = client.GetDialogUrl("oauth/access_token", new
            {
                client_id =AppId,
                client_secret = AppSecret,
                redirect_uri = "/",
                code= code
            });
            var token = result.access_token as string;
        }
        public static string RefreshTokenAndPostToFacebook(string currentAccessToken)
        {
            string newAccessToken = RefreshAccessToken(currentAccessToken);
            string pageAccessToken = GetPageAccessToken(newAccessToken);
            PostToFacebook(pageAccessToken);
            return newAccessToken; // replace current access token with this
        }
       
        public static string GetPageAccessToken(string userAccessToken)
        {
            FacebookClient fbClient = new FacebookClient();
            fbClient.AppId = "280044195490470";
            fbClient.AppSecret = "e129782115f34029ff7582a8a1c9acea";
            fbClient.AccessToken = userAccessToken;
            Dictionary<string, object> fbParams = new Dictionary<string, object>();
            var a = fbClient.GetDialogUrl("/me/accounts", fbParams);
            JsonObject publishedResponse = null;
            JArray data = JArray.Parse(publishedResponse["data"].ToString());

            foreach (var account in data)
            {
                if (account["name"].ToString().ToLower().Equals("your page name"))
                {
                    return account["access_token"].ToString();
                }
            }

            return String.Empty;
        }

        public static string RefreshAccessToken(string currentAccessToken)
        {
            FacebookClient fbClient = new FacebookClient();
            Dictionary<string, object> fbParams = new Dictionary<string, object>();
            fbParams["client_id"] = "app id";
            fbParams["grant_type"] = "fb_exchange_token";
            fbParams["client_secret"] = "app secret";
            fbParams["fb_exchange_token"] = currentAccessToken;
            var publishedResponse = fbClient.GetDialogUrl("/oauth/access_token", fbParams);
            return "";
        }

        public static void PostToFacebook(string pageAccessToken)
        {
            FacebookClient fbClient = new FacebookClient(pageAccessToken);
            fbClient.AppId = "app id";
            fbClient.AppSecret = "app secret";
            Dictionary<string, object> fbParams = new Dictionary<string, object>();
            fbParams["message"] = "Test message";
            var publishedResponse = fbClient.PostTaskAsync("/your_page_name/feed", fbParams);
        }
    }
}