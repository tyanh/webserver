﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;
using Renci.SshNet.Security.Cryptography;
using Renci.SshNet.Sftp;
using Webdll.Models;
using Webserver.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Webserver.Controllers
{
    [Route("api/[controller]")]
    public class ApiController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        LoginModel user = new LoginModel();
       
        public ApiController(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            user = _session.Get<LoginModel>("userlogin");
        }
        [HttpGet("resetStartusInvoice")]
        public bool resetStartusInvoice()
        {
            Startup.runsql = "";
            return true;
        }
        public string ishack()
        {
            Response.Redirect("/Home/Login");
            return "";
        }
        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        [HttpGet("updateStatusInvoice")]
        public string updateStatusInvoice(string obj,string id,string idstatus,string note)
        {
            Common cmd = new Common();
            ConfigTable config = new ConfigTable();
            config.SetConfigTable(obj,"");
            var cl = config.columnGv.Split(',');
            var typecl = config.typedisplay.Split(',');
            Sqlcommon sql = new Sqlcommon();
            string idrecord = id.Split('_')[2];
            var record = sql.finddata2(obj, "where id=" + idrecord + "");
            var permission = cmd.ispermission(config.idmenu, "view", user);
            var status = sql.getdataSQL("statusinvoice", "");
            if (!permission || record.Count <= 0)
               return ishack();
            string clttinvoice = cl[typecl.IndexOf(typecl.FirstOrDefault(s => s.Contains("statusiv")))];
            if(record[clttinvoice].ToString() != "")
            {
                var stat = status.FirstOrDefault(s => s["id"].ToString() ==record[clttinvoice].ToString())["lock"].ToString();
                if (stat == "1")
                    idstatus= record[clttinvoice].ToString();
            }
            if (user.admin!="1")
            {
                if(record["usercreate"].ToString()!=user.id)
                    return ishack();
            }
            var appctm= status.FirstOrDefault(s => s["id"].ToString() == idstatus);
            if (appctm.Count <= 0)
               return ishack();
            List<string> clup = new List<string>();
            List<string> vlup = new List<string>();
            List<string> sqls = new List<string>();
            for (var i=0;i< typecl.Length; i++)
            {
                if (typecl[i].Contains("statusiv"))
                {
                    clup.Add(cl[i]);
                    vlup.Add(idstatus);
                }
                else if (typecl[i].Contains("notestatusbill"))
                {
                    clup.Add(cl[i]);
                    vlup.Add(note);
                }
            }
            string str = sql.updatesqlOneItemstr(config.name_, clup, vlup,int.Parse(idrecord), user.id);
            sqls.Add(str);
            if (appctm["applycustomer"].ToString() != "")
            {
                List<string> vlbill = new List<string>();
                List<string> clinsertbill = new List<string>();
                var strs = appctm["applycustomer"].ToString().Split('?');
                clinsertbill.Add(strs[0]);
                vlbill.Add(record[strs[0]].ToString());
                var uctm = strs[1].Split('#');
                foreach (var u in uctm)
                {
                    var cal = u.Split('/');
                    clinsertbill.Add(cal[0]);
                    vlbill.Add(record[cal[0]].ToString());
                }
                sqls.Add(sql.updatecusInvoice(appctm["applycustomer"].ToString(), vlbill, clinsertbill, user.id));
            }
                sql.runSQL(string.Join(";", sqls));
            return "0";
        }
        [HttpPost("delete")]
        public string delete(string data)
        {
            dynamic d = JsonConvert.DeserializeObject<dynamic>(data);
            string obj = d.obj.ToString();
            string id = d.id.ToString();
            Sqlcommon sql = new Sqlcommon();
            string str = "DELETE FROM "+obj+" WHERE id="+id+"";
            sql.runSQL(str);
            return "0";
        }
        [HttpPost("RemoveSubData")]
        public string RemoveSubData(string data)
        {
            Sqlcommon sql = new Sqlcommon();
            dynamic o = JsonConvert.DeserializeObject<dynamic>(data);
            string obj = o.obj.ToString();
            string id_ = o.id_.ToString();
            string clremove = o.clremove.ToString();
            string vlremove = o.vlremove.ToString();
            var d = sql.finddata2(obj, "where id=" + id_ + "");
            if (d.Count > 0) {
                var sub = d[clremove].ToString().Split(',').ToList();
                sub.Remove(vlremove);
                var clup = new List<string>() { clremove };
                var vlup = new List<string>() { string.Join(",",sub) };
                Common cmd = new Common();
                string str = sql.updatesqlOneItemstr(obj, clup, vlup, int.Parse(id_), user.id);
                sql.runSQL(str);
                return "0";
            }
            return "-1";
        }
        [HttpPost("AddSubData")]
        public string AddSubData(string data)
        {
            dynamic o = JsonConvert.DeserializeObject<dynamic>(data);
            string tb = o.obj.ToString();
            string valueEdit = o.valueEdit.ToString();
            string[] keys= o.keys.ToString().Split(',');
            string[] valid = o.notnull.ToString().Split(',');
            dynamic dta = JsonConvert.DeserializeObject<dynamic>(o.data.ToString());
            var clumn = JsonConvert.DeserializeObject<List<dynamic>>(dta.cl.ToString());
            var vlues = JsonConvert.DeserializeObject<List<dynamic>>(dta.vl.ToString());
            Sqlcommon sql = new Sqlcommon();
            Common cmd = new Common();
            var clumnkey = "";
            var vlkey = "";
            Dictionary<string, object> dataqr = new Dictionary<string, object>();
            foreach (var i in valid)
            {
                var index=clumn.IndexOf(i);
                if (vlues[index].ToString() == "")
                    return "Vui lòng nhập "+ i;
            }
            if (valueEdit == "-1")
            {
                foreach (var i in keys)
                {
                    var index = clumn.IndexOf(i);
                    var vlchk = vlues[index].ToString();
                    if (vlchk == "")
                        return "Vui lòng nhập " + i;
                    clumnkey = i;
                    vlkey = vlues[index].ToString();
                    dataqr = sql.finddata2(tb, "where " + i + "='" + vlchk + "' AND id <>" + valueEdit + "");
                    if (dataqr.Count > 0)
                        return "Đã tồn tại " + i;
                }
            }
            else
                dataqr = sql.finddata2(tb, "where id="+ valueEdit + "");
            List<string> clinsert = new List<string>();
            List<string> vlinsert = new List<string>();
            for (var i=0;i< clumn.Count; i++)
            {
                clinsert.Add(clumn[i].ToString());
                vlinsert.Add(vlues[i].ToString());
            }
            string ssql = "";
            string rt = "";
            if (valueEdit == "-1")
            {
                ssql = sql.insertsqlstr(tb, clinsert, vlinsert, user.id);
                rt = sql.runSQLAndGetId(ssql, clumnkey, vlkey, tb);
            }
            else
            {
                string clconnect = o.clconnect.ToString();
                List<string> clcnect = new List<string>();
                string cnecttmp = dataqr[clconnect].ToString();
                if(cnecttmp!="")
                    clcnect = cnecttmp.Split(',').ToList();
                int indexcn = clinsert.IndexOf(clconnect);
                string vlcn = vlinsert[indexcn];
                if(clcnect.IndexOf(vlcn)==-1)
                        clcnect.Add(vlcn);
                vlinsert[indexcn] = string.Join(",", clcnect);
                ssql = sql.updatesqlOneItemstr(tb, clinsert, vlinsert, int.Parse(valueEdit),user.id);
                sql.runSQL(ssql);
                rt = valueEdit;
            }
   
            return rt;
        }
        [HttpGet("GetGiftCode")]
        public string GetGiftCode(string code)
        {
            Common cmd = new Common();
            return cmd.GetGiftCode(code);
        }
        [HttpGet("GetDetail")]
        public string GetDetail(string data)
        {
            dynamic o = JsonConvert.DeserializeObject<dynamic>(data);
            Sqlcommon sql = new Sqlcommon();
            string search = o.search.ToString();
            string vl = o.value.ToString();
            string where = "where " + o.objid.ToString() + " like '%" + vl + "%'";
            
            if (search == "in")
            {
                List<string> sub = vl.Split(',').ToList(); 
                where = "where " + o.objid.ToString() + " in (" + string.Join(", ", sub) + ")";
            }
                var datas = sql.getdataSQL(o.obj.ToString(), where);
            return JsonConvert.SerializeObject(datas);
        }
        [HttpGet("Getproduct")]
        public string Getproduct(string code)
        {
            Sqlcommon sql = new Sqlcommon();
             ConfigTable config = new ConfigTable();
             config.SetConfigTable("list_san_pham","");
            var data = sql.getdataSQL("products", "where codedesc='" + code + "'");
            dynamic rs = new System.Dynamic.ExpandoObject();
            rs.title = config.titleGv;
            rs.column = config.columnGv;
            rs.type = config.typedisplay;
            rs.data = JsonConvert.SerializeObject(data);
            return JsonConvert.SerializeObject(rs); 
        }
     
        [HttpPost("SaveImages")]
        public async Task<string> SaveImages(string bag, string name, string folder)
        {

            commondll cmddll = new commondll();
            Common cmd = new Common();
            List<string> lfd = folder.Split(';').ToList();
            List<string> fol = lfd[1].Split(',').ToList();
            string path = lfd[0]+string.Join('\\',fol)+"\\";
            string ps = Startup.wwwroot + path;
            var rs =await cmddll.uploadImg(bag, name, ps, lfd[1],bool.Parse(lfd[2]), "1213");
            string nreturn= string.Join('/', fol) + "/" + name;
            return nreturn;
         }
        [HttpPost("SaveImage")]
        public async Task<string> SaveImage(string bag, string name, string folder)
        {
            Common cmd = new Common();
            string path_for_Uploaded_Files =cmd.hostimg+folder;
            var o = cmd.createFolder(path_for_Uploaded_Files+@"\");
            var img = cmd.GetImgBitmap(bag);
            name =cmd.CreatCode("");
            //var file = cmd.watermarkStronImage("monaco", img, "", "");
            var file = cmd.watermarkBarcodeonImage("", img, "", "");
            //file.Save(path_for_Uploaded_Files + @"\" + name + ".jpg");
            var stream = new System.IO.MemoryStream();
            file.Save(stream, ImageFormat.Jpeg);
            stream.Position = 0;
            await stream.CopyToAsync(new FileStream(path_for_Uploaded_Files + @"\" + name + ".jpg", FileMode.Create));
            stream.Close();
            stream.Dispose();
            return name + ".jpg";
            
        }
        [HttpPost("Postdata")]
        public string Postdata(string data, string type)
        {
            return data;
            return "";
        }
        [HttpPost("PostdataUpdateCtm")]
        public string PostdataUpdateCtm(string data)
        {
            Common cmd = new Common();
            Sqlcommon sql = new Sqlcommon();
            string[] cl = new string[] { "phone", "name", "email", "bithday", "address", "city", "dist", "note" };
            dynamic o = JsonConvert.DeserializeObject<dynamic>(data);
            List<string> cls = new List<string>() { "phone", "name_", "email", "birthday", "address", "city", "distrist", "note" };
            List<string> vl = new List<string>();
            List<string> sqls = new List<string>();
            string p = o["phone"].ToString();
            var obj = sql.finddata2("customers", "where phone='" + p + "' AND id<>" + o["id"] + " AND (" + cmd.strNotdelete() + ")");
            if (obj.Count > 0)
                return "Đã tồn tại số phone này";
            foreach (var i in cl)
            {
                string v = o[i];
                if (i == "bithday")
                    v = cmd.convertDate(v);
                vl.Add(v);
            }
            if (o["id"] != "-1")
            {
                var olddata = sql.finddata2("customers", "where id=" + o["id"] + "");
                string htr = sql.insert_htr_item("customers", o["id"].ToString(), data, JsonConvert.SerializeObject(olddata), "1", "update");
                string update = sql.updatesqlOneItemstr("customers", cls, vl, int.Parse(o["id"].ToString()), "1");
                sqls.Add(htr);
                sqls.Add(update);
            }
            else
            {
                string ins = sql.insertsqlstr("customers", cls, vl, "1");
                sqls.Add(ins);
            }
            sql.runSQL(string.Join(";", sqls));
            return "0";
        }
        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }
        [HttpGet("Gethistoryproduct")]
        public List<Dictionary<string, object>> Gethistoryproduct(string product)
        {

            Sqlcommon sql = new Sqlcommon();
            string str = "select 'sale' AS SiteName,* from detailbillsales where idproduct=" + product + " UNION SELECT 'input' AS SiteName,* FROM detailbillinput where idproduct=" + product + "";
            var o = sql.getdataSQLjoinTable(str);
            return o;
        }
        
        [HttpGet("refeshDataWebsite")]
        public void refeshDataWebsite()
        {
            WebClient wc = new WebClient();
            wc.DownloadString("http://192.168.137.1/api/SystemApi/UpdateData");
        }
        [HttpGet("Editproduct")]
        public string Editproduct(string value, string codeid)
        {
            Common cmd = new Common();
            Sqlcommon sql = new Sqlcommon();
            List<dynamic> values = JsonConvert.DeserializeObject<List<dynamic>>(value);
            var users = HttpContext.Session.Get<LoginModel>("user");
            string user = users.id;
            string barcode = values.FirstOrDefault(s => s.column.ToString() == "barcode")["value"];
            var kt = sql.finddata2("products", "where barcode='" + barcode + "'");
            if (kt.Count > 0)
            {
                if (kt["id"].ToString() != codeid)
                    return "-1";
            }
            List<string> columns = new List<string>();
            List<string> vls = new List<string>();
            foreach (var v in values)
            {
                string column = v.column.ToString();
                columns.Add(column);
                string vl = v.value.ToString();
                vls.Add(vl);
            }
            string str = sql.updatesqlcolumn("products", columns, vls,"where id="+ codeid + "", user);
            sql.runSQL(str);
            return codeid;
        }
        [HttpGet("AddRefen")]
        public string AddRefen(string value)
        {
            Common cmd = new Common();
            Sqlcommon sql = new Sqlcommon();
            dynamic values = JsonConvert.DeserializeObject<dynamic>(value);
            string n_ = values.value.ToString();
            string o_ = values.obj.ToString();
            string clumnrt = values.clreturn.ToString();
            string where = "where name_='" + n_ + "'";
            if (values.vlofname.ToString() != "")
                where = where + " AND val='" + values.vlofname.ToString() + "'";
            var strck = sql.checkdataExit(o_, where);
            if (strck)
            {
               
                string code = cmd.CreatCode(o_);
                var climp = new List<string>() { "name_", clumnrt };
                var vlimp = new List<string>() { n_, code };
                if (values.vlofname.ToString() != "")
                {
                    climp.Add("val");
                    vlimp.Add(values.vlofname.ToString());
                }
                string s_ = sql.insertsqlstr(o_,climp, vlimp,user.id);
                sql.runSQL(s_);
                return code;
            }
            
            return "-1";
        }
        [HttpGet("intserproduct")]
        public string intserproduct(string column,string value, string codeid)
        {
            Common cmd = new Common();
            Sqlcommon sql = new Sqlcommon();
            List<string> sqls = new List<string>();
            List<dynamic> values = JsonConvert.DeserializeObject<List<dynamic>>(value);
            string code = codeid;
            if(code==null)
                code = cmd.CreatCode("pr");
            var users = HttpContext.Session.Get<LoginModel>("user");
            string user = users.id;
            foreach (var v in values) {
                List<string> columns = column.Split(",").ToList();
                columns.Add("codedesc");
             
                v.codedesc = code;
                List<string> vls = new List<string>();
                foreach(var i in columns)
                {
                    vls.Add(v[i].ToString());
                }
                string str = sql.insertsqlstr("products", columns, vls, user);
                sqls.Add(str);
            }
            sql.runSQL(string.Join(";", sqls));
            return code;
        }
        [HttpGet("Getconfigtable")]
        public string Getconfigtable(string table)
        {
             ConfigTable config = new ConfigTable();
             config.SetConfigTable(table,"");
            return JsonConvert.SerializeObject(config);
        }
        [HttpGet("GetDataInvoid")]
        public string GetDataInvoid(string table, string codeid)
        {
            Sqlcommon sql = new Sqlcommon();
            var data = sql.finddata2(table,"where codeid='"+codeid+"'");
            return JsonConvert.SerializeObject(data);
        }
        [HttpGet("GetAllData")]
        public string GetAllData(string obj,string cols,string where)
        {
            Sqlcommon sql = new Sqlcommon();
            string str = "select " + cols + " from " + obj;
            if (where != null)
                str = str + " where '" + where + "'";
            var data = sql.getdataSQLstr(str);

            return JsonConvert.SerializeObject(data);
        }
        [HttpGet("GetData")]
        public string GetData(string type, string page,string number,string cols)
        {
            Sqlcommon sql = new Sqlcommon();
            string str="select top "+number+" "+ cols+" from "+type;
            var data = sql.getdataSQLstr(str);
            return JsonConvert.SerializeObject(data);
        }
        [HttpGet("GetDataWeb")]
        public string GetDataWeb(string type, string page, string number, string cols,string where)
        {
            Sqlcommon sql = new Sqlcommon();
            string a = "select top 1 name_ from attribute_table where frendly_url='"+type+"'";
            var b = sql.finddata(a);
            string now = DateTime.Now.ToShortDateString();
            if (type == "qua_tang")
                where = where + " AND startday<='" + now + "' AND endday>='" + now + "' AND status<>1";
            string str = "select top " + number + " " + cols + " from " + b["name_"]+" where "+where;
            var data = sql.getdataSQLstr(str);
            if (data.Count > 0)
            {
                if (type == "qua_tang")
                {
                    var isunlimit = data[0]["isunlimit"].ToString();
                    if(isunlimit=="0")
                    {
                        var qualityuser= data[0]["qualityuser"].ToString();
                        if (qualityuser == "")
                            qualityuser = "0";
                        var quality = data[0]["quality"].ToString();
                        if (quality == "")
                            quality = "0";
                        if (int.Parse(qualityuser) >= int.Parse(quality))
                            return "[]";
                    }
                }
                foreach (var i in data)
                {
                    try { i["birthday"] = DateTime.Parse(i["birthday"].ToString()).ToString("dd-MM-yyyy"); } catch { }
                }
            }
            return JsonConvert.SerializeObject(data);
        }
        [HttpGet("GetItem")]
        public string GetItem(string code,string obj)
        {
            Sqlcommon sql = new Sqlcommon();
            var i = sql.finddata2(obj, "where barcode='" + code + "'");
            return JsonConvert.SerializeObject(i);
        }
        [HttpGet("GetItemIventory")]
        public string GetItemIventory(string code, string wh)
        {
            Sqlcommon sql = new Sqlcommon();
            Common cmd = new Common();
            var p = sql.finddata2("products", "where barcode='" + code + "'");
            string warehouse = p["inventoryhouse"].ToString();
            List<Dictionary<string, object>> warehouses = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(warehouse);
            var w = warehouses.FirstOrDefault(s => s["id"].ToString() == wh);
            if (w == null || cmd.convertNumberDouble(w["remain"].ToString()) <= 0)
                return "Không đủ số lượng";
            return JsonConvert.SerializeObject(p);
        }
        [HttpGet("logout")]
        public string logout(string session)
        {
            Sqlcommon sql = new Sqlcommon();
            var lg = sql.finddata2("workers", "where session='" + session + "'");
            if (lg.Count > 0)
            {
                string str = "UPDATE workers SET login = 0, session = '',lastupdate='" + DateTime.Now + "' WHERE session='" + session + "'";
                sql.runSQL(str);
            }
            return "";
        }
        [HttpGet("loginAndmenu")]
        public string loginAndmenu(string user, string pass,string inforDevice)
        {
            Common cdm = new Common();
            Sqlcommon sql = new Sqlcommon();
            //try
            //{

                pass = cdm.MD5(pass);
                var lg = sql.finddata2("workers", "where username='" + user + "' AND pass='" + pass + "' AND status<>1");
                if (lg.Count <= 0)
                {
                    return "-1";
                }
                else
                {
                    var menu= sql.getdataSQL("menus", "where status<>1");
                    List<dynamic> rs = new List<dynamic>();
                    List<dynamic> menurt = new List<dynamic>();
                    List<dynamic> menus = JsonConvert.DeserializeObject<List<dynamic>>(lg["privilege"].ToString());
                    foreach(var i in menus)
                    {
                        dynamic o = new System.Dynamic.ExpandoObject();
                           var obj = menu.FirstOrDefault(s => s["id"].ToString() == i["name"].ToString());
                        o.id = i.name.ToString();
                        o.submenu = obj["submenu"].ToString();
                        o.img = obj["link"].ToString();
                        o.view = i.view.ToString();
                        o.add = i.add.ToString();
                        o.remove = i.remove.ToString();
                        o.edit = i.edit.ToString();
                        o.name = obj["name_"].ToString();
                        
                        menurt.Add(o);
                    }
                    dynamic avt = new System.Dynamic.ExpandoObject();
                string codenew = cdm.CreatCode("");
                    avt.img = lg["avatar"];
                if (avt.img.ToString() == "")
                    avt.img = "avatar";
                    avt.id = lg["id"];
                    avt.session = codenew;
                    avt.username = lg["name_"].ToString();
                    rs.Add(avt);
                    rs.Add(menurt);
                    string update = sql.updatesqlOneItemstr("workers", new List<string>() { "oslogin","login", "session" }, new List<string>() { inforDevice,"1", codenew }, int.Parse(lg["id"].ToString()), lg["id"].ToString());
                    sql.runSQL(update);

                 return JsonConvert.SerializeObject(rs);
                }
        //}
        //    catch
        //    {
        //        return "-1";
        //    }
        }
        [HttpGet("saveorder")]
        public string saveorder(string tables,string data,string cl)
        {
            Common cmd = new Common();
            Sqlcommon sql = new Sqlcommon();
            List<string> sqls = new List<string>();
            List<dynamic> o = JsonConvert.DeserializeObject<List<dynamic>>(data);
            foreach (var i in o)
            {
                string s = sql.updatesqlOneItemstr(tables, new List<string>() { cl }, new List<string>() { i.order.ToString() }, int.Parse(i.iddata.ToString()), user.id);
                sqls.Add(s);
            }
            sql.runSQL(string.Join(";", sqls));
            return "0";
        }
        [HttpGet("GetcolumntableOld")]
        public List<string> GetcolumntableOld(string table)
        {
            SqlDataOld sql = new SqlDataOld();
            var cl = sql.GetcolumnTableNames(table);
            return cl;
        }
        [HttpGet("exportExcel")]
        public FileResult exportExcel(string data)
        {
            Sqlcommon sql = new Sqlcommon();
            DataTable table = new DataTable("data_");
            dynamic pare = JsonConvert.DeserializeObject<dynamic>(data);

            var obj = pare.obj.ToString();
            var objtb = pare.objtb.ToString();
            string[] title = pare.titles.ToString().Split(',');
            var type = pare.types.ToString().Split(',');
            var cl = pare.columns.ToString().Split(',');
            string[] noexport = pare.noexport.ToString().Split(',');
            string objdetail = "";
            string[] ttdetail = new string[] { };
            string[] cldetail = new string[] { };
            string[] typedetail = new string[] { };
            List <Dictionary<string, object>> subtable = new List<Dictionary<string, object>>();
            foreach(var i in type)
            {
             
                if (i.Contains("getvlfromkey"))
                {
                    string[] sub_ = i.Split('-');
                    if (subtable.FirstOrDefault(s => s["table"].ToString() == sub_[1]) != null)
                        continue;
                    Dictionary<string, object> s_dt = new Dictionary<string, object>();
                    s_dt["table"] = sub_[1].ToString();
                    s_dt["data"] = sql.getdataSQLstr("select name_,"+ sub_[2] + " from "+ sub_[1] + "");
                    subtable.Add(s_dt);
                }
                
            }
            for (var i=0;i< title.Length; i++)
            {
                string t = type[i];
                if (noexport.ToList().IndexOf(title[i]) != -1)
                    continue;
                if (type[i] != "numbers" || type[i] == "strnumbers")
                    table.Columns.Add(title[i], typeof(string));
                else
                    table.Columns.Add(title[i], typeof(float));
            }
            if(pare.detail.ToString()!="[]")
            {
                dynamic epdt = JsonConvert.DeserializeObject<dynamic>(pare.detail.ToString());
                ttdetail = epdt.ttdetail.ToString().Split(',');
                cldetail = epdt.cldetail.ToString().Split(',');
                typedetail = epdt.typedetail.ToString().Split(',');
                objdetail = epdt.obj.ToString();
                List<string> newcldt = new List<string>();
                List<int> newtypedt = new List<int>();
                for (var i = 0; i < ttdetail.Length; i++)
                {
                    if (typedetail[i] == "sott" || noexport.IndexOf(ttdetail[i]) != -1)
                    {
                        
                        newcldt.Add(cldetail[i]);
                        newtypedt.Add(i);
                        continue;
                    }
                    if (title.IndexOf(ttdetail[i]) != -1)
                        ttdetail[i] = ttdetail[i] + " sp";
                    if (typedetail[i] != "numbers" || typedetail[i] == "strnumbers")
                        table.Columns.Add(ttdetail[i], typeof(string));
                    else
                        table.Columns.Add(ttdetail[i], typeof(float));
                }

                cldetail = cldetail.Except(newcldt).ToArray();
                
                   typedetail = typedetail.Where((source, index) => newtypedt.IndexOf(index) == -1).ToArray();
               
            }
           
            Common cmd = new Common();
            var dt = new List<Dictionary<string,object>>();
            var stt = 1;
            foreach (var j in dt)
            {
                List<object> ldata = new List<object>();
                for (var k=0;k< cl.Length; k++)
                {
                    var vlue = j[cl[k]].ToString();
                    if (noexport.ToList().IndexOf(title[k]) != -1)
                        continue;
                    if (type[k] == "sott")
                    {
                        vlue = stt;
                    }
                    else if (type[k] == "numbers" || type[k] == "strnumbers")
                        vlue = cmd.convertNumberDouble(vlue);
                    else if (type[k].Contains("connectcl-"))
                    {
                        string[] connect = type[k].Split('-')[1].Split('/');
                        List<dynamic> jdata = JsonConvert.DeserializeObject<List<dynamic>>(vlue);
                        List<string> lvl_ = new List<string>();
                        foreach (var pa in connect)
                        {

                            lvl_.Add(jdata.FirstOrDefault(s => s.cl.ToString() == pa).vl.ToString());
                        }
                        vlue = string.Join(" ", lvl_);

                    }
                    else if (type[k].Contains("getvlfromkey"))
                    {
                        if (vlue != "")
                        {
                            try
                            {
                                string[] l_ = type[k].Split('-');
                                string tb = l_[1];
                                string clget = l_[2];
                                var tmp = (List<Dictionary<string, object>>)subtable.FirstOrDefault(s => s["table"].ToString() == tb)["data"];
                                vlue = tmp.FirstOrDefault(s => s[clget].ToString() == vlue)["name_"].ToString();
                            }
                            catch { }
                        }
                    }

                    ldata.Add(vlue);
                    
                }
                if (objdetail != "")
                {
                    
                    List<dynamic> odetail =JsonConvert.DeserializeObject<List<dynamic>>(j[objdetail].ToString());
                    
                    foreach (var d in odetail)
                    {
                        List<object> ldatadt = new List<object>();
                        ldatadt.AddRange(ldata);
                        for (var m= 0;m < cldetail.Length;m++)
                        {
                            var vldt = d[cldetail[m]].ToString();
                            if(typedetail[m] == "numbers" || typedetail[m] == "strnumbers")
                                vldt = cmd.convertNumberDouble(vldt);
                            ldatadt.Add(vldt);
                           
                        }
                        table.Rows.Add(ldatadt.ToArray());
                    }
                }
                else
                {
                    table.Rows.Add(ldata.ToArray());
                }
                stt++;
               

            }
           
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(table);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", obj + ".xlsx");
                }
            }
        }
        [HttpGet("exportOld")]
        public async Task<FileResult> exportOld(string tables,string listcolumn)
        {
            DataTable table = new DataTable("dataexport");
            List<dynamic> cl = JsonConvert.DeserializeObject<List<dynamic>>(listcolumn);
            foreach(var i in cl)
            {
                var clu = i.column.ToString();
                table.Columns.Add(clu, typeof(string));
            }
            SqlDataOld sql = new SqlDataOld();
            var dt = sql.getdataSQL(tables, "");
            foreach (var j in dt)
            {
                List<string> lcl = new List<string>();
                foreach(var k in cl)
                {
                    var clu = k.column.ToString();
                    lcl.Add(j[clu].ToString());
                }
                table.Rows.Add(lcl.ToArray());

            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(table);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", tables+".xlsx");
                }
            }
        }
       
        [HttpGet("GetProFile")]
        public string GetProFile(string user, string pass)
        {
            Common cdm = new Common();
            Sqlcommon sql = new Sqlcommon();
            try
            {

                pass = cdm.MD5(pass);
                var lg = sql.finddata2("workers", "where username='" + user + "' AND pass='" + pass + "' AND status<>1");
                if (lg.Count <= 0)
                {
                    return "-1";
                }
                else
                {
                    List<dynamic> l=new List<dynamic>();
                    
                    string[] title = new string[] {"avatar","fullname(*)", "user name(*)", "password(*)", "password again(*)", "email(*)", "phone(*)" };
                    string[] datatype = new string[] { "img", "text", "text", "password", "passwordagain", "email", "text" };
                    string[] validate = new string[] { "false", "true", "true", "true", "true", "false", "false" };
                    string[] cldata = new string[] { "avatar", "name_", "username", "pass", "pass", "email", "phone" };
                    string[] errorr = new string[] { "Vui lòng chọn avatar", "Vui lòng nhập họ tên", "Vui lòng nhập usernae", "Vui lòng nhập password", "Vui lòng xác nhận password", "Vui lòng nhập email", "Vui lòng nhập phone" };
                    for (var i=0;i< title.Length;i++)
                    {
                        dynamic p = new System.Dynamic.ExpandoObject();
                        p.title = title[i];
                        p.type = datatype[i];
                        p.valid = validate[i];
                        p.column = cldata[i];
                        p.data = lg[cldata[i]].ToString();
                        p.note= errorr[i];
                        l.Add(p);
                    }
                    return JsonConvert.SerializeObject(l);
                }
            }
            catch
            {
                return "-1";
            }
        }
        [HttpGet("GetTodolist")]
        public string GetTodolist(string iduser)
        {
            Common cdm = new Common();
            Sqlcommon sql = new Sqlcommon();
            try
            {

                string now = DateTime.Now.ToShortDateString();
                var todo = sql.getdataSQL("todolist", "where datealert>='"+now+ "' AND (status<>1 OR status is null)");
                List<dynamic> rs = new List<dynamic>();
                if(todo.Count<=0)
                    return "0";
                foreach(var i in todo)
                {
                    string[] u = i["useraccess"].ToString().Split(",");
                    if (u.Contains(iduser) == true)
                        rs.Add(i);
                }
               
                    return JsonConvert.SerializeObject(rs);
                
            }
            catch
            {
                return "-1";
            }
        }
        [HttpGet("login")]
        public string login(string user,string pass)
        {
            Common cdm = new Common();
            Sqlcommon sql = new Sqlcommon();
            try
            {
                
                pass = cdm.MD5(pass);
                var lg = sql.finddata2("workers", "where username='" + user + "' AND pass='" + pass + "' AND status<>1");
                if (lg.Count <= 0)
                {
                    return sql.cnn;
                }
                else
                    return sql.cnn;
            }
            catch
            {
                return sql.cnn;
            }
        }
        // POST api/<controller>
        [HttpPost("uploadImg")]
        public void uploadImg()
        {
           
        long uploaded_size = 0;
            string path_for_Uploaded_Files = "wwwroot/images/";
            var uploaded_files = Request.Form.Files;
            int iCounter = 0;

            string sFiles_uploaded = "";
         
            foreach (var uploaded_file in uploaded_files)
            {
                //----< Uploaded File >----

                iCounter++;

                uploaded_size += uploaded_file.Length;

                sFiles_uploaded += "\n" + uploaded_file.FileName;

                //< Filename >

                string uploaded_Filename = uploaded_file.FileName;

                string new_Filename_on_Server = path_for_Uploaded_Files + "\\" + uploaded_Filename;
                using (FileStream stream = new FileStream(new_Filename_on_Server, FileMode.Create))
                {

                    uploaded_file.CopyTo(stream);
               
                }

            }
        }
        [HttpPost("uploadImgName")]
        public void uploadImgName(string name,string began,string folder)
        {
            long uploaded_size = 0;
            string path_for_Uploaded_Files = "wwwroot/images/";
            var uploaded_files = Request.Form.Files;
            int iCounter = 0;
            int iCounter_ = int.Parse(began);
            string sFiles_uploaded = "";
            foreach (var uploaded_file in uploaded_files)
            {
                //----< Uploaded File >----

                iCounter++;

                uploaded_size += uploaded_file.Length;

                sFiles_uploaded += "\n" + uploaded_file.FileName;

                //< Filename >

                string uploaded_Filename = uploaded_file.FileName;

                string new_Filename_on_Server = path_for_Uploaded_Files + "\\" + uploaded_Filename;
                string ext = Path.GetExtension(new_Filename_on_Server);
                string new_Filename_ = path_for_Uploaded_Files+"\\"+folder + "\\" + name+"_"+ iCounter_ + ext;
                if(name== "undefined")
                    new_Filename_ = path_for_Uploaded_Files + "\\" + folder + "\\" + uploaded_file.FileName;
                using (FileStream stream = new FileStream(new_Filename_, FileMode.Create))
                {
                    
                    uploaded_file.CopyTo(stream);

                }
                iCounter_++;
            }
        }
        [HttpPost("UploadProfile")]
        public string UploadProfile(List<IFormFile> files,string data,string idprofile)
        {
            Common cmd = new Common();
            Sqlcommon sql = new Sqlcommon();
            List<string> l_name = new List<string>();
            var u = sql.finddata2("workers", "where id=" + idprofile + " AND status<>1");
            if (u.Count <= 0)
                return "Không tồn tại";
            List<dynamic> infor = JsonConvert.DeserializeObject<List<dynamic>>(data);
            string username = infor.FirstOrDefault(s => s["column"].ToString() == "username")["data"].ToString();
            var ex = sql.checkdataExit("workers", "where username='" + username + "' AND id<>" + idprofile + "");
            if(!ex)
                return "SELECT id FROM workers where username='" + username + "' AND id<>" + idprofile + "";
            string FolderSource = Path.Combine(@"wwwroot\images\");
            if (files.Count > 0)
            {
                foreach (IFormFile file in files)
                {
                    if (file == null || file.Length == 0)
                        return "";
                    var path = Path.Combine(FolderSource, "", file.FileName);
                    FileStream fileStream = new FileStream(path, FileMode.Create);
                    file.CopyTo(fileStream);
                    fileStream.Dispose();
                    l_name.Add(file.FileName);
                }
            }

           
            List<string> column = new List<string>();
            List<string> value = new List<string>();
            foreach (var i in infor)
            {
                string cl = i.column.ToString();
                string vl = i.data.ToString();
                if (cl == "avatar")
                {
                    if (l_name.Count > 0)
                    {
                        column.Add(cl);
                        value.Add(string.Join(";", l_name));
                    }
                }
                else if (cl == "pass")
                {
                    if (column.IndexOf(cl) == -1)
                    {
                        if(u[cl].ToString()!=vl)
                        {
                            column.Add(cl);
                            value.Add(cmd.MD5(vl));
                        }
                    }
                }
                else
                {
                    if (vl != "")
                    {
                        column.Add(cl);
                        value.Add(vl);
                    }
                }
            }

            string htr = sql.insert_htr_item("workers", idprofile, data, JsonConvert.SerializeObject(u), idprofile, "Edit worker");
            string newstr = sql.updatesqlOneItemstr("workers", column, value,int.Parse(idprofile), idprofile);
            List<string> qr = new List<string>() { htr, newstr };
            sql.runSQL(string.Join(";", qr));
            return idprofile;

        }
        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
        [HttpPost("CheckDataKey")]
        public string CheckDataKey(string data)
        {
            dynamic o = JsonConvert.DeserializeObject<dynamic>(data);
            string obj = o.obj.ToString();
            string id = o.id.ToString();
            string clkey = o.getdata.ToString();
            List<string> getdata = clkey.Split(' ').ToList();
            List<dynamic> keytxts = JsonConvert.DeserializeObject<List<dynamic>>(o.keytxts.ToString());
            List<string> key1 = getdata[0].Split('-').ToList();
            string where = "where "+ key1[0] + "=N'"+ keytxts.FirstOrDefault(s=>s["name"].ToString()== key1[0]).vl + "'";
            if (getdata.Count > 1)
                where = where + " " + key1[1];
            foreach (var w in getdata.Skip(1))
            {
                var w_ = w.Split('-');
                where= where+" "+w_[0]+ "=N'" + keytxts.FirstOrDefault(s => s["name"].ToString() == w_[0]).vl + "'";
                if (w_.Count() > 1)
                    where = where + " " + w_[1];
            }
            Sqlcommon sql = new Sqlcommon();
            string str = "select * from " + obj+ " "+where;
            
            var datas = sql.finddata(str);
            if (datas.Count <=0 || datas["id"].ToString() == id)
                return "0";
            else
                return "-1";

        }
        [HttpGet("CheckData")]
        public string CheckData(string type, string where, string cols)
        {
            Sqlcommon sql = new Sqlcommon();
            string str = "select top 1 " + cols + " from " + type + " "+where;
            var data = sql.getdataSQLstr(str);
            if (data.Count <= 0)
                return "0";
            else
                return "1";
          
        }
        [HttpPost("saveSubDataInvoice")]
        public string saveSubDataInvoice(string data)
        {
            var o = JsonConvert.DeserializeObject<dynamic>(data);
            var datas = o.data;
            var key = o.key.ToString();
            List<string> cl = new List<string>();
            List<string> vl = new List<string>();
            string vlkey = "";
            foreach(var i in datas)
            {
                if (i.name.ToString() == key)
                    vlkey = i.vl.ToString();
                cl.Add(i.name.ToString());
                vl.Add(i.vl.ToString());
            }
            Sqlcommon sql = new Sqlcommon();
            Common cmd = new Common();
            string str = sql.insertsqlstr(o.obj.ToString(), cl, vl, user.id);
            sql.runSQL(str);
            return "0";

        }
    }
}
