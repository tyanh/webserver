﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Webserver.Models;

namespace Webserver.Controllers
{
    public class ChatbootController : Controller
    {
        sqlchat sql = new sqlchat();
        public IActionResult listreplaycontent()
        {
            var data = sql.getdataSQL("replys", "");
            var key= sql.getdataSQL("keyworks", "");
            foreach(var i in data)
            {
                var s_ = key.Where(s => s["coderl"].ToString() == i["code"].ToString()).Select(s=>s["keyworkvn"].ToString()).ToList() ;
                i["usercreate"] = string.Join("<br>", s_);
            }
            return View(data);
        }
        public IActionResult replaycontent()
        {
            var code = Request.Query["code"].ToString();
            ChatModel m = new ChatModel();
            m.setkeyall(code);
            m.code = code;
            if (code != "")
            {
                Dictionary<string, object> obj = new Dictionary<string, object>();
                obj = sql.finddata2("replys", "where code='" + code + "'");
                m.contents = obj["contents"].ToString();
                m.isproduct = obj["isproduct"].ToString();
                m.name = obj["name_"].ToString();
                m.status= obj["status"].ToString();
            }
           
           return View(m);
        }
    }
    
}