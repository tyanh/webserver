#pragma checksum "D:\webserver\Webserver\Webserver\Views\UserControls\ViewMapDataTable.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "938613a8baebb7c805109fef467b82b0e0af51d7"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_UserControls_ViewMapDataTable), @"mvc.1.0.view", @"/Views/UserControls/ViewMapDataTable.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/UserControls/ViewMapDataTable.cshtml", typeof(AspNetCore.Views_UserControls_ViewMapDataTable))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\webserver\Webserver\Webserver\Views\_ViewImports.cshtml"
using Webserver;

#line default
#line hidden
#line 2 "D:\webserver\Webserver\Webserver\Views\_ViewImports.cshtml"
using Webserver.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"938613a8baebb7c805109fef467b82b0e0af51d7", @"/Views/UserControls/ViewMapDataTable.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7ca7a4d9f4b9720a07fd89f88bae3df4f7552121", @"/Views/_ViewImports.cshtml")]
    public class Views_UserControls_ViewMapDataTable : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewMapDataTable.cshtml"
  
    var column = Model["column"].ToString();
    var title = Model["title"].ToString();
    var value = Model["value"].ToString().Split(',');
    var from_ = Model["from"].ToString();
   

#line default
#line hidden
            BeginContext(200, 208, true);
            WriteLiteral("<script>\r\n    function addmapcolumn() {\r\n        var p = \"<p class=\'cl-sql\' ondblclick=\'$(this).remove()\'>\" + $(\"#mapcolumn\").val() + \"</p>\";\r\n        $(p).appendTo(\'.mapcolumntables\');\r\n    }\r\n</script>\r\n<p>");
            EndContext();
            BeginContext(409, 5, false);
#line 14 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewMapDataTable.cshtml"
Write(title);

#line default
#line hidden
            EndContext();
            BeginContext(414, 92, true);
            WriteLiteral(" <span class=\"span-add\" data-toggle=\"modal\" data-target=\"#mapcolumntable\">+</span></p>\r\n<div");
            EndContext();
            BeginWriteAttribute("class", " class=\"", 506, "\"", 536, 2);
            WriteAttributeValue("", 514, "mapcolumntables", 514, 15, true);
#line 15 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewMapDataTable.cshtml"
WriteAttributeValue(" ", 529, from_, 530, 6, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(537, 19, true);
            WriteLiteral(" style=\"clear:both\"");
            EndContext();
            BeginWriteAttribute("name", " name=\"", 556, "\"", 570, 1);
#line 15 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewMapDataTable.cshtml"
WriteAttributeValue("", 563, column, 563, 7, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(571, 3, true);
            WriteLiteral(">\r\n");
            EndContext();
#line 16 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewMapDataTable.cshtml"
     foreach (var i in value)
    {
        if (i != "")
        {

#line default
#line hidden
            BeginContext(645, 60, true);
            WriteLiteral("            <p class=\'cl-sql\' ondblclick=\'$(this).remove()\'>");
            EndContext();
            BeginContext(706, 11, false);
#line 20 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewMapDataTable.cshtml"
                                                       Write(Html.Raw(i));

#line default
#line hidden
            EndContext();
            BeginContext(717, 6, true);
            WriteLiteral("</p>\r\n");
            EndContext();
#line 21 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewMapDataTable.cshtml"
        }
   
    }

#line default
#line hidden
            BeginContext(746, 788, true);
            WriteLiteral(@"
</div>
    <div class=""modal"" id=""mapcolumntable"">
        <div class=""modal-dialog"">
            <div class=""modal-content"">
                <!-- Modal body -->
                <div class=""modal-body bdtutorial"">
                    <p>
                        column cần map - table map - giá trị ss - cl so sánh trong table map - cl muốn lấy trong table map
                    </p>
                    <input type=""text"" id=""mapcolumn"" style=""width:100%"" />
                </div>
                <!-- Modal footer -->
                <div class=""modal-footer"">

                    <button type=""button"" class=""btn btn-danger"" onclick=""addmapcolumn()"">Save</button>
                </div>

            </div>
        </div>
    </div>
<p style=""clear:both""></p>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
