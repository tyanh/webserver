#pragma checksum "D:\webserver\Webserver\Webserver\Views\UserControls\ViewPrintAgain.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8b067c96ca40ce55c27e7114e5b2bd5dc709374e"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_UserControls_ViewPrintAgain), @"mvc.1.0.view", @"/Views/UserControls/ViewPrintAgain.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/UserControls/ViewPrintAgain.cshtml", typeof(AspNetCore.Views_UserControls_ViewPrintAgain))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\webserver\Webserver\Webserver\Views\_ViewImports.cshtml"
using Webserver;

#line default
#line hidden
#line 2 "D:\webserver\Webserver\Webserver\Views\_ViewImports.cshtml"
using Webserver.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8b067c96ca40ce55c27e7114e5b2bd5dc709374e", @"/Views/UserControls/ViewPrintAgain.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7ca7a4d9f4b9720a07fd89f88bae3df4f7552121", @"/Views/_ViewImports.cshtml")]
    public class Views_UserControls_ViewPrintAgain : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Dictionary<string, object>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/jquery-barcode.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/jquery-barcode.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/CreateBarcode.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewPrintAgain.cshtml"
  
    Sqlcommon sql = new Sqlcommon();
    var headprint = sql.finddata2("headerinvoice", "where id=2");
    var discount = headprint["isdiscount"].ToString();
    var giftcode = headprint["giftcode"].ToString();
    var payment = headprint["payment"].ToString();
    string[] tprint = headprint["clprint"].ToString().Split('?');
    string[] tprintdt = headprint["clprintdetail"].ToString().Split('?');
    var inforcpn = sql.finddata2("inforcompany", "where id<>-1");



#line default
#line hidden
            BeginContext(522, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(524, 46, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "f2356c4c2a4843eaaf00b2efe0e903e5", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(570, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(572, 50, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "561f5b14094b4a27b035d972d11ed9fb", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(622, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(624, 45, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a20b01fdef4b4fbe8554caa2612a560c", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(669, 384, true);
            WriteLiteral(@"

<div class=""modal data_sub_popup"" id=""print-invoice"">
    <div class=""modal-dialog"">
        <div class=""modal-content"">

            <!-- Modal body -->
            <div class=""modal-body"" id=""body_print"" style=""max-height: 500px;overflow: auto; padding: 0 10px;"">
                <header class=""clearfix"">
                    <div id=""logo"">
                        <img");
            EndContext();
            BeginWriteAttribute("src", " src=\"", 1053, "\"", 1084, 2);
            WriteAttributeValue("", 1059, "/imgWeb/", 1059, 8, true);
#line 27 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewPrintAgain.cshtml"
WriteAttributeValue("", 1067, inforcpn["logo"], 1067, 17, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1085, 112, true);
            WriteLiteral(">\r\n                    </div>\r\n                    <div id=\"company\">\r\n                        <h2 class=\"name\">");
            EndContext();
            BeginContext(1198, 17, false);
#line 30 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewPrintAgain.cshtml"
                                    Write(inforcpn["name_"]);

#line default
#line hidden
            EndContext();
            BeginContext(1215, 36, true);
            WriteLiteral("</h2>\r\n                        <div>");
            EndContext();
            BeginContext(1252, 19, false);
#line 31 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewPrintAgain.cshtml"
                        Write(inforcpn["address"]);

#line default
#line hidden
            EndContext();
            BeginContext(1271, 37, true);
            WriteLiteral("</div>\r\n                        <div>");
            EndContext();
            BeginContext(1309, 17, false);
#line 32 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewPrintAgain.cshtml"
                        Write(inforcpn["phone"]);

#line default
#line hidden
            EndContext();
            BeginContext(1326, 413, true);
            WriteLiteral(@"</div>
                        <div><a href=""mailto:company@example.com"">company@example.com</a></div>
                    </div>
                </header>
                <main>
                    <div id=""details"" class=""clearfix"">
                        <h1 id=""barcodeTarget"" class=""tprint barcodeTarget"" getdata=""invoicenumber"" style=""margin:0 auto""></h1>
                        <div id=""client"">
");
            EndContext();
#line 40 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewPrintAgain.cshtml"
                             foreach (var i in tprint)
                            {
                                string[] s_ = i.Split("*");

#line default
#line hidden
            BeginContext(1887, 53, true);
            WriteLiteral("                                <div class=\"tprints\">");
            EndContext();
            BeginContext(1941, 5, false);
#line 43 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewPrintAgain.cshtml"
                                                Write(s_[0]);

#line default
#line hidden
            EndContext();
            BeginContext(1946, 22, true);
            WriteLiteral(": <span class=\"tprint\"");
            EndContext();
            BeginWriteAttribute("getdata", " getdata=\"", 1968, "\"", 1984, 1);
#line 43 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewPrintAgain.cshtml"
WriteAttributeValue("", 1978, s_[1], 1978, 6, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1985, 17, true);
            WriteLiteral("></span> </div>\r\n");
            EndContext();
#line 44 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewPrintAgain.cshtml"
                            }

#line default
#line hidden
            BeginContext(2033, 231, true);
            WriteLiteral("\r\n                        </div>\r\n\r\n                    </div>\r\n                    <table class=\"responsive-table tbleprint\" style=\"margin:0;margin-bottom:10px\">\r\n                        <thead>\r\n                            <tr>\r\n");
            EndContext();
#line 52 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewPrintAgain.cshtml"
                                 foreach (var i in tprintdt)
                                {
                                    string[] s_ = i.Split("*");

#line default
#line hidden
            BeginContext(2426, 57, true);
            WriteLiteral("                                    <th class=\"th-detail\"");
            EndContext();
            BeginWriteAttribute("getdata", " getdata=\"", 2483, "\"", 2499, 1);
#line 55 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewPrintAgain.cshtml"
WriteAttributeValue("", 2493, s_[1], 2493, 6, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2500, 1, true);
            WriteLiteral(">");
            EndContext();
            BeginContext(2502, 5, false);
#line 55 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewPrintAgain.cshtml"
                                                                      Write(s_[0]);

#line default
#line hidden
            EndContext();
            BeginContext(2507, 7, true);
            WriteLiteral("</th>\r\n");
            EndContext();
#line 56 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewPrintAgain.cshtml"

                                }

#line default
#line hidden
            BeginContext(2551, 296, true);
            WriteLiteral(@"                            </tr>
                        </thead>
                        <tbody id=""print-detail"">
                        </tbody>

                    </table>
                    <div class=""div-sum""><span>Tiền hàng: </span><div class=""sum"" id=""summoney""></div></div>
");
            EndContext();
#line 65 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewPrintAgain.cshtml"
                     if (discount == "1")
                    {

#line default
#line hidden
            BeginContext(2913, 165, true);
            WriteLiteral("                        <div class=\"div-sum cDiscount\"><span>Chiết khấu <span id=\"discountprint\"></span> </span>:<div class=\"sum\" id=\"discountvlprint\"></div></div>\r\n");
            EndContext();
#line 68 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewPrintAgain.cshtml"
                    }

#line default
#line hidden
            BeginContext(3101, 20, true);
            WriteLiteral("                    ");
            EndContext();
#line 69 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewPrintAgain.cshtml"
                     if (giftcode == "1")
                    {

#line default
#line hidden
            BeginContext(3167, 131, true);
            WriteLiteral("                        <div class=\"div-sum cgiftcode\"><span id=\"ctgift\"></span>:<div class=\"sum\" id=\"giftcodeprint\"></div></div>\r\n");
            EndContext();
#line 72 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewPrintAgain.cshtml"
                    }

#line default
#line hidden
            BeginContext(3321, 114, true);
            WriteLiteral("                    <div class=\"div-sum\"><span>Thành tiền: </span><div class=\"sum\" id=\"summoneyend\"></div></div>\r\n");
            EndContext();
#line 74 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewPrintAgain.cshtml"
                     if (payment == "1")
                    {

#line default
#line hidden
            BeginContext(3500, 249, true);
            WriteLiteral("                        <div class=\"div-sum cpay\"><span>Thanh toán:</span> <div class=\"sum\" id=\"payvlprint\"></div></div>\r\n                        <div class=\"div-sum cpay creturn\"><span>Tiền dư:</span> <div class=\"sum\" id=\"payrtprint\"></div></div>\r\n");
            EndContext();
#line 78 "D:\webserver\Webserver\Webserver\Views\UserControls\ViewPrintAgain.cshtml"
                    }

#line default
#line hidden
            BeginContext(3772, 304, true);
            WriteLiteral(@"                </main>
            </div>

            <!-- Modal footer -->
            <div class=""modal-footer"">
                <button type=""button"" class=""btn btn-danger"" id=""btn-print"" onclick=""printData('body_print')"">print</button>
            </div>

        </div>
    </div>
</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Dictionary<string, object>> Html { get; private set; }
    }
}
#pragma warning restore 1591
