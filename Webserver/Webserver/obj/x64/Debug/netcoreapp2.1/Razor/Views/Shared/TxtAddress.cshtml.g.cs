#pragma checksum "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "f0c19d2d9f1bb97f8ed8e5cd783fdcae760e2c00"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared_TxtAddress), @"mvc.1.0.view", @"/Views/Shared/TxtAddress.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Shared/TxtAddress.cshtml", typeof(AspNetCore.Views_Shared_TxtAddress))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\webserver\Webserver\Webserver\Views\_ViewImports.cshtml"
using Webserver;

#line default
#line hidden
#line 2 "D:\webserver\Webserver\Webserver\Views\_ViewImports.cshtml"
using Webserver.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f0c19d2d9f1bb97f8ed8e5cd783fdcae760e2c00", @"/Views/Shared/TxtAddress.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7ca7a4d9f4b9720a07fd89f88bae3df4f7552121", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared_TxtAddress : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Dictionary<string, object>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
   
    Sqlcommon sql = new Sqlcommon();
    string[] cldata = Model["datatype"].ToString().Split('-');
    var from_ = Model["from"].ToString();
    var subclass = Model["subclass"].ToString();
    string strcity = "select name_,code,id from citys";
    var city = sql.getdataSQLstr(strcity);

    string strwards = "select name_,code,id,codecity from wards";
    var wards = sql.getdataSQLstr(strwards);

    string strdistrists = "select name_,code,id,codecity,codeward from distrists";
    var distrists = sql.getdataSQLstr(strdistrists);
    string address = "";
    string namecity = "";
    string nameward = "";
    string namedts = "";

    string codecity = "";
    string codeward = "";
    string codedts = "";
    if (Model["value"].ToString() != "")
    {
        var value = Model["value"].ToString().Split('-');
        address = value[0];
        codecity = value[1];
        codeward = value[2];
        codedts = value[3];
        try { namecity = city.FirstOrDefault(s => s["code"].ToString() == value[1])["name_"].ToString(); } catch { }
        try { nameward = wards.FirstOrDefault(s => s["code"].ToString() == value[2])["name_"].ToString(); } catch { }
        try { namedts = distrists.FirstOrDefault(s => s["code"].ToString() == value[3])["name_"].ToString(); } catch { }


    }

#line default
#line hidden
            BeginContext(1378, 505, true);
            WriteLiteral(@"    <script>
        function getcodeaddress(obj) {
            $(obj).attr(""idvl"", """");
            var data = $(""."" + $(obj).attr(""obj"")).attr(""data-sql"");
            var j = JSON.parse(data);
            $.each(j, function (index, value) {
                if (value.name_ == $(obj).val()) {
                    $(obj).attr(""idvl"", value.code);
                    return false;
                }
            });
            
        }
         $(function () {

            var vlcity = ");
            EndContext();
            BeginContext(1884, 59, false);
#line 52 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
                    Write(Html.Raw(Newtonsoft.Json.JsonConvert.SerializeObject(city)));

#line default
#line hidden
            EndContext();
            BeginContext(1943, 179, true);
            WriteLiteral(";\r\n                var dcity = [];\r\n            for (var i = 0; i < vlcity.length; i++) {\r\n                dcity.push(vlcity[i].name_);\r\n\r\n            }\r\n            var vlward = ");
            EndContext();
            BeginContext(2123, 60, false);
#line 58 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
                    Write(Html.Raw(Newtonsoft.Json.JsonConvert.SerializeObject(wards)));

#line default
#line hidden
            EndContext();
            BeginContext(2183, 183, true);
            WriteLiteral(";\r\n             var dward = [];\r\n             for (var i = 0; i < vlward.length; i++) {\r\n                 dward.push(vlward[i].name_);\r\n\r\n            }\r\n            var vldistrists = ");
            EndContext();
            BeginContext(2367, 64, false);
#line 64 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
                         Write(Html.Raw(Newtonsoft.Json.JsonConvert.SerializeObject(distrists)));

#line default
#line hidden
            EndContext();
            BeginContext(2431, 204, true);
            WriteLiteral(";\r\n             var ddistrists = [];\r\n             for (var i = 0; i < vldistrists.length; i++) {\r\n                 ddistrists.push(vldistrists[i].name_);\r\n\r\n             }\r\n             $(\'.address-city-");
            EndContext();
            BeginContext(2636, 5, false);
#line 70 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
                         Write(from_);

#line default
#line hidden
            EndContext();
            BeginContext(2641, 75, true);
            WriteLiteral("\').attr(\"data-sql\",JSON.stringify(vlcity));\r\n             $(\'.address-ward-");
            EndContext();
            BeginContext(2717, 5, false);
#line 71 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
                         Write(from_);

#line default
#line hidden
            EndContext();
            BeginContext(2722, 80, true);
            WriteLiteral("\').attr(\"data-sql\", JSON.stringify(vlward));\r\n             $(\'.address-district-");
            EndContext();
            BeginContext(2803, 5, false);
#line 72 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
                             Write(from_);

#line default
#line hidden
            EndContext();
            BeginContext(2808, 90, true);
            WriteLiteral("\').attr(\"data-sql\", JSON.stringify(vldistrists));\r\n            autocomplet(\".data-address-");
            EndContext();
            BeginContext(2899, 5, false);
#line 73 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
                                  Write(from_);

#line default
#line hidden
            EndContext();
            BeginContext(2904, 58, true);
            WriteLiteral(" .city\", dcity);\r\n             autocomplet(\".data-address-");
            EndContext();
            BeginContext(2963, 5, false);
#line 74 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
                                   Write(from_);

#line default
#line hidden
            EndContext();
            BeginContext(2968, 58, true);
            WriteLiteral(" .ward\", dward);\r\n             autocomplet(\".data-address-");
            EndContext();
            BeginContext(3027, 5, false);
#line 75 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
                                   Write(from_);

#line default
#line hidden
            EndContext();
            BeginContext(3032, 59, true);
            WriteLiteral(" .district\", ddistrists);\r\n        });\r\n    </script>\r\n<div");
            EndContext();
            BeginWriteAttribute("class", " class=\"", 3091, "\"", 3118, 2);
            WriteAttributeValue("", 3099, "address-city-", 3099, 13, true);
#line 78 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue("", 3112, from_, 3112, 6, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(3119, 25, true);
            WriteLiteral(" data-sql=\"\"></div>\r\n<div");
            EndContext();
            BeginWriteAttribute("class", " class=\"", 3144, "\"", 3171, 2);
            WriteAttributeValue("", 3152, "address-ward-", 3152, 13, true);
#line 79 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue("", 3165, from_, 3165, 6, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(3172, 25, true);
            WriteLiteral(" data-sql=\"\"></div>\r\n<div");
            EndContext();
            BeginWriteAttribute("class", " class=\"", 3197, "\"", 3228, 2);
            WriteAttributeValue("", 3205, "address-district-", 3205, 17, true);
#line 80 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue("", 3222, from_, 3222, 6, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(3229, 54, true);
            WriteLiteral(" data-sql=\"\"></div>\r\n<div style=\"display:inline-block\"");
            EndContext();
            BeginWriteAttribute("class", " class=\"", 3283, "\"", 3330, 4);
            WriteAttributeValue("", 3291, "data-address", 3291, 12, true);
            WriteAttributeValue(" ", 3303, "data-address-", 3304, 14, true);
#line 81 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue("", 3317, from_, 3317, 6, false);

#line default
#line hidden
#line 81 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue(" ", 3323, from_, 3324, 6, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(3331, 62, true);
            WriteLiteral(">\r\n    <div>\r\n       địa chỉ<br />\r\n        <input type=\"text\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 3393, "\"", 3409, 1);
#line 84 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue("", 3401, address, 3401, 8, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginWriteAttribute("class", " class=\"", 3410, "\"", 3455, 4);
            WriteAttributeValue("", 3418, "dataaddress_", 3418, 12, true);
            WriteAttributeValue(" ", 3430, "address", 3431, 8, true);
            WriteAttributeValue(" ", 3438, "addstr", 3439, 7, true);
#line 84 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue(" ", 3445, subclass, 3446, 9, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginWriteAttribute("name", " name=\"", 3456, "\"", 3473, 1);
#line 84 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue("", 3463, cldata[1], 3463, 10, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginWriteAttribute("from", " from=\"", 3474, "\"", 3487, 1);
#line 84 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue("", 3481, from_, 3481, 6, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(3488, 132, true);
            WriteLiteral(" style=\"width:100%\" />\r\n    </div>\r\n    <div class=\"div-one-line\">\r\n        Thành phố<br />\r\n        <input type=\"text\" list-data=\"\"");
            EndContext();
            BeginWriteAttribute("idvl", " idvl=\"", 3620, "\"", 3636, 1);
#line 88 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue("", 3627, codecity, 3627, 9, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(3637, 30, true);
            WriteLiteral(" onblur=\"getcodeaddress(this)\"");
            EndContext();
            BeginWriteAttribute("obj", " obj=\"", 3667, "\"", 3692, 2);
            WriteAttributeValue("", 3673, "address-city-", 3673, 13, true);
#line 88 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue("", 3686, from_, 3686, 6, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginWriteAttribute("value", " value=\"", 3693, "\"", 3710, 1);
#line 88 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue("", 3701, namecity, 3701, 9, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginWriteAttribute("class", " class=\"", 3711, "\"", 3760, 5);
            WriteAttributeValue("", 3719, "dataaddress_", 3719, 12, true);
            WriteAttributeValue(" ", 3731, "address", 3732, 8, true);
            WriteAttributeValue(" ", 3739, "city", 3740, 5, true);
            WriteAttributeValue(" ", 3744, "w-200", 3745, 6, true);
#line 88 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue(" ", 3750, subclass, 3751, 9, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginWriteAttribute("name", " name=\"", 3761, "\"", 3778, 1);
#line 88 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue("", 3768, cldata[2], 3768, 10, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginWriteAttribute("from", " from=\"", 3779, "\"", 3792, 1);
#line 88 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue("", 3786, from_, 3786, 6, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(3793, 101, true);
            WriteLiteral(" />\r\n    </div>\r\n    <div class=\"div-one-line\">\r\n        Quận huyện<br />\r\n        <input type=\"text\"");
            EndContext();
            BeginWriteAttribute("idvl", " idvl=\"", 3894, "\"", 3910, 1);
#line 92 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue("", 3901, codeward, 3901, 9, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(3911, 30, true);
            WriteLiteral(" onblur=\"getcodeaddress(this)\"");
            EndContext();
            BeginWriteAttribute("obj", " obj=\"", 3941, "\"", 3966, 2);
            WriteAttributeValue("", 3947, "address-ward-", 3947, 13, true);
#line 92 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue("", 3960, from_, 3960, 6, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginWriteAttribute("value", " value=\"", 3967, "\"", 3984, 1);
#line 92 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue("", 3975, nameward, 3975, 9, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginWriteAttribute("class", " class=\"", 3985, "\"", 4031, 5);
            WriteAttributeValue("", 3993, "databade_", 3993, 9, true);
            WriteAttributeValue(" ", 4002, "address", 4003, 8, true);
            WriteAttributeValue(" ", 4010, "ward", 4011, 5, true);
            WriteAttributeValue(" ", 4015, "w-150", 4016, 6, true);
#line 92 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue(" ", 4021, subclass, 4022, 9, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginWriteAttribute("name", " name=\"", 4032, "\"", 4049, 1);
#line 92 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue("", 4039, cldata[3], 4039, 10, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginWriteAttribute("from", " from=\"", 4050, "\"", 4063, 1);
#line 92 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue("", 4057, from_, 4057, 6, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(4064, 100, true);
            WriteLiteral(" />\r\n    </div>\r\n    <div class=\"div-one-line\">\r\n        phường xã<br />\r\n        <input type=\"text\"");
            EndContext();
            BeginWriteAttribute("idvl", " idvl=\"", 4164, "\"", 4179, 1);
#line 96 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue("", 4171, codedts, 4171, 8, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(4180, 30, true);
            WriteLiteral(" onblur=\"getcodeaddress(this)\"");
            EndContext();
            BeginWriteAttribute("obj", " obj=\"", 4210, "\"", 4239, 2);
            WriteAttributeValue("", 4216, "address-district-", 4216, 17, true);
#line 96 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue("", 4233, from_, 4233, 6, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginWriteAttribute("value", " value=\"", 4240, "\"", 4256, 1);
#line 96 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue("", 4248, namedts, 4248, 8, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginWriteAttribute("class", " class=\"", 4257, "\"", 4307, 5);
            WriteAttributeValue("", 4265, "databade_", 4265, 9, true);
            WriteAttributeValue(" ", 4274, "address", 4275, 8, true);
            WriteAttributeValue(" ", 4282, "district", 4283, 9, true);
            WriteAttributeValue(" ", 4291, "w-150", 4292, 6, true);
#line 96 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue(" ", 4297, subclass, 4298, 9, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginWriteAttribute("name", " name=\"", 4308, "\"", 4325, 1);
#line 96 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue("", 4315, cldata[4], 4315, 10, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginWriteAttribute("from", " from=\"", 4326, "\"", 4339, 1);
#line 96 "D:\webserver\Webserver\Webserver\Views\Shared\TxtAddress.cshtml"
WriteAttributeValue("", 4333, from_, 4333, 6, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(4340, 23, true);
            WriteLiteral(" />\r\n    </div>\r\n</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Dictionary<string, object>> Html { get; private set; }
    }
}
#pragma warning restore 1591
