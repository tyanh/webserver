#pragma checksum "D:\webserver\Webserver\Webserver\Views\Shared\ScriptCreateLineItem.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "919420990e89ebcd52c26364b4d4001cdfc2a6f3"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared_ScriptCreateLineItem), @"mvc.1.0.view", @"/Views/Shared/ScriptCreateLineItem.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Shared/ScriptCreateLineItem.cshtml", typeof(AspNetCore.Views_Shared_ScriptCreateLineItem))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\webserver\Webserver\Webserver\Views\_ViewImports.cshtml"
using Webserver;

#line default
#line hidden
#line 2 "D:\webserver\Webserver\Webserver\Views\_ViewImports.cshtml"
using Webserver.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"919420990e89ebcd52c26364b4d4001cdfc2a6f3", @"/Views/Shared/ScriptCreateLineItem.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7ca7a4d9f4b9720a07fd89f88bae3df4f7552121", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared_ScriptCreateLineItem : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(124, 6998, true);
            WriteLiteral(@"<script>
    function calculaInvoice() {
        sumInvoice();
        sumFinishInvoice();
    }
    function CreateLintProcduct(obj) {
        var tt_price = ""Giá bán:"";
        var price_ = converToString(obj.pricesale);
        var action = $(""#tb_bill"").attr(""actions"");
        if (action == ""input"") {
            var tt_price = ""Giá nhập:"";
            var price_ = converToString(obj.priceinput);
        }
        var imgs = obj.photo.toString().split(',');
        var img = ""/images/"" + imgs[0];
        var a = '<tr code=""' + obj.barcode +'"" class=""It_invoice"" onclick=""updateqlt($(this))"">'+
                '<td><span class=""cricleavt"" style=""background:url('+img+');""></span></td>'+
                '<td>'+
            '<p class=""name_"">' + obj.barcode +'</p>'+
            '<p class=""sub_"">' + obj.name_ + '</p>' +
            '<p class=""sub_"">' + tt_price + '<span class=""price_iv"">' + price_+'</span></p>'+
                '</td>'+
                '<td class=""qlt"">1</td>'+
         ");
            WriteLiteral(@"       '<td class=""money"">'+
            '<p class=""sub_dt"">$<span class=""t_tien"">' + price_ +'</span></p>'+
                '</td>'
        '</tr>';
        return a;
    }
    function updateqlt(o) {
        $(""#InputQlt"").attr(""code"", """");
        $("".edit_"").empty();
        var qtls = $(o).find("".qlt"").html();
        var codes = $(o).attr(""code"");
        $(""#InputQlt"").attr(""code"", codes);
        var obj = $(o).clone();
        $(obj).appendTo("".edit_"");
        $(""#Qlt_product"").val(qtls);
        $(""#InputQlt"").modal('toggle');
        $(""#Qlt_product"").focus();
    }
    function RemoveIt() {
        var c = $(""#InputQlt"").attr(""code"");
        $(""#tb_bill .It_invoice"").each(function (index) {
            var code = $(this).attr(""code"");
            if (code === c) {
                $(this).remove();
                return false;

            }
        });
        calculaInvoice();
        $(""#InputQlt"").modal('hide');
    }
    function UpQlt() {
        var c = $(""");
            WriteLiteral(@"#InputQlt"").attr(""code"");
        $(""#tb_bill .It_invoice"").each(function (index) {
            var code = $(this).attr(""code"");
            if (code == c) {
                var money = convertonumber($(this).find("".price_iv"").html());
                var qlt = convertonumber($(""#Qlt_product"").val());
                var sum = money * qlt;
                $(this).find("".t_tien"").html(converToString(sum));
                $(this).find("".qlt"").html(converToString(qlt));
                return false;

            }
        });
        calculaInvoice();
        $(""#InputQlt"").modal('hide');
    }
    function addPro() {
        $('#code_product').val('');
        $(""#InputCode"").modal('toggle');
    }
    function checkDupliPr(c) {
        var rs = true;
        $(""#tb_bill .It_invoice"").each(function (index) {
            var code = $(this).attr(""code"");
            if (code == c) {
               
                var money = convertonumber($(this).find("".price_iv"").html());
          ");
            WriteLiteral(@"      var qlt =parseFloat(convertonumber($(this).find("".qlt"").html()))+1;
                var sum = money * qlt;
                $(this).find("".t_tien"").html(converToString(sum));
                $(this).find("".qlt"").html(converToString(qlt));
                rs = false;
                return false;
              }
        });
        calculaInvoice();
        $(""#InputQlt"").modal('hide');
        return rs;
    }
    function InsertPro(code) {
        var rs = checkDupliPr(code);
        if (rs) {
            openPopupLoader();
            //Html.RenderPartial(""~/Views/Shared/LineProductMb.cshtml"", new ViewDataDictionary(this.ViewData) { { ""param1"", o } });
            var where = ""barcode='"" + code + ""'"";
            $.get(""/api/API/GetDataWeb?type=san_pham&page=0&number=1&cols=*&where="" + where + """", { async: false }, function (data) {
                if (data == ""[]"") {
                    openAlert(""Sản phẩm không tồn tại"");
                }
                else {
               ");
            WriteLiteral(@"    
                    var j = JSON.parse(data);
                    var a = CreateLintProcduct(j[0]);
                    $(a).appendTo(""#tb_bill"");
                    calculaInvoice();
                }
                offPopupLoader();
            });
            

        }
        $(""#InputCode"").modal('hide');
    }
    function sumInvoice() {
        var sum = 0;
        $(""#tb_bill .It_invoice"").each(function (index) {
            var money = convertonumber($(this).find("".money .t_tien"").html());
            sum = sum + parseInt(money);
        });

        $(""#money_invoice"").html(converToString(sum));
    }
    function sumFinishInvoice() {
        var sum = convertonumber($(""#money_invoice"").html());
        var discount = convertonumber($("".down_price"").html());
        var o = parseInt(sum) - parseInt(discount);
        $("".total_price"").html(converToString(o));
    }
</script>
<div id=""InputCode"" class=""modal fade"" role=""dialog"">
    <div class=""modal-dialog"">
");
            WriteLiteral(@"
        <!-- Modal content-->
        <div class=""modal-content"">
            <div class=""modal-header"">
                <button type=""button"" class=""close"" data-dismiss=""modal"">&times;</button>
                <h4 class=""modal-title""><span class=""cricleavt"" style=""background:url('/images/mobileicon/Barcode-icon.png');""></span></h4>
            </div>
            <div class=""modal-body"">
                <p class=""p_search"">
                    <span>Code sản phẩm</span>
                    <input type=""text"" id=""code_product"" style=""text-align: right;padding-right: 30px;""/>
                </p>

            </div>
            <div class=""modal-footer"">
                <button type=""button"" class=""btn btn-default"" onclick=""InsertPro($('#code_product').val())"">Select</button>
            </div>
        </div>

    </div>
</div>

<div id=""InputQlt"" class=""modal fade"" role=""dialog"" code="""">
    <div class=""modal-dialog"">

        <!-- Modal content-->
        <div class=""modal-content"">");
            WriteLiteral(@"
            <div class=""modal-header"">
                <button type=""button"" class=""close"" data-dismiss=""modal"">&times;</button>
                <table class=""line_it edit_""></table>
            </div>
            <div class=""modal-body"">
                <p class=""p_search"">
                    <span>Số lượng sản phẩm</span>
                    <input type=""text"" class=""input_number"" id=""Qlt_product"" style=""text-align: right;padding-right: 30px;"" />
                </p>
               
            </div>
            <div class=""modal-footer"">
                <button type=""button"" class=""btn btn-default"" style=""float:left"" onclick=""UpQlt()"">Update</button>
                <button type=""button"" class=""btn btn-default"" style=""float:right"" onclick=""RemoveIt()"">Remove</button>
            </div>
        </div>

    </div>
</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
