#pragma checksum "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8600f57b1a29f4026a7779015353c748318232c7"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Action_CreateInvoice), @"mvc.1.0.view", @"/Views/Action/CreateInvoice.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Action/CreateInvoice.cshtml", typeof(AspNetCore.Views_Action_CreateInvoice))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\webserver\Webserver\Webserver\Views\_ViewImports.cshtml"
using Webserver;

#line default
#line hidden
#line 2 "D:\webserver\Webserver\Webserver\Views\_ViewImports.cshtml"
using Webserver.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8600f57b1a29f4026a7779015353c748318232c7", @"/Views/Action/CreateInvoice.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7ca7a4d9f4b9720a07fd89f88bae3df4f7552121", @"/Views/_ViewImports.cshtml")]
    public class Views_Action_CreateInvoice : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Dictionary<string, object>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/invoice.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
  
    ViewData["Title"] = "CreateInvoice";
    Layout = "~/Views/Shared/_Layout.cshtml";
    Common cmd = new Common();
    List<string> type = (List<string>)Model["type"];
    Dictionary<string, object> models = new Dictionary<string, object>();
    var company = (Dictionary<string, object>)Model["company"];
    var dtail = (Dictionary<string, object>)Model["detail"];
    string[] title = dtail["titles"].ToString().Split(',');
    string[] types = dtail["atr_"].ToString().Split(',');
    string[] column = dtail["columns"].ToString().Split(',');

    Dictionary<string, object> templeatItem = new Dictionary<string, object>();
    foreach (var t in column)
    {
        templeatItem[t] = "";
    }
    string[] notnull = dtail["valid"].ToString().Split(',');
    string[] addmore = dtail["key_"].ToString().Split('-');
    string[] columnsum = Model["columnsum"].ToString().Split(',');
    string[] sumend = Model["sumend"].ToString().Split('-');

    string headnotnull = Model["headnotnull"].ToString();
    List<string> ttsum = new List<string>();
    string tablemapcolumn = Model["tablemapcolumn"].ToString();
    string tabledata = Model["tabledata"].ToString();
    string datamap = Model["datamap"].ToString();
    string tabletotable = Model["tabletotable"].ToString();
    string[] keysnotsame = Model["keysnotsame"].ToString().Split(',');
    int j = 0;
    var modelinvoice = new Dictionary<string, object>();
    modelinvoice["company"] = company;
    modelinvoice["invoicenum"] = Model["invoicenum"];
    modelinvoice["datebill"] = DateTime.Now.ToString("dd/MM/yyyy");
    modelinvoice["titelinvoice"] = Model["tinvoice"];
    var bill_ = Newtonsoft.Json.JsonConvert.SerializeObject(Model["dataroot"]);

#line default
#line hidden
            BeginContext(1799, 616, true);
            WriteLiteral(@"    <script>
        var datas = [];
        var indexsum = [];
        var classindexsum = [];
        var valuesum = [];
        function loadbill() {
            var dtail = JSON.parse($(""#loadbill"").attr(""data""));
            for (var i = 0; i < dtail.length; i++) {
                $("".codeitem"").val(dtail[i].code);
                $("".codeitem"").focus();
                $(""#trheader .qlt"").val(dtail[i].qlt);
                $(""#trheader .qlt"").focus();
                AddRowBill($(""#trheader .qlt""));
            }
        }
        
    $(document).ready(function () {
        var bill_ =");
            EndContext();
            BeginContext(2416, 15, false);
#line 56 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
              Write(Html.Raw(bill_));

#line default
#line hidden
            EndContext();
            BeginContext(2431, 283, true);
            WriteLiteral(@";
        if (bill_ != """") {
            $("".warehouse option:eq(1)"").prop(""selected"", true);
            $("".phone"").focus();
            $("".phone"").val(bill_.phone);
            $(""#loadbill"").attr(""data"", bill_.detailbill);
           
        }
       
        datas = ");
            EndContext();
            BeginContext(2715, 19, false);
#line 65 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
           Write(Html.Raw(tabledata));

#line default
#line hidden
            EndContext();
            BeginContext(2734, 26, true);
            WriteLiteral(";\r\n        var datamap = \'");
            EndContext();
            BeginContext(2761, 17, false);
#line 66 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
                  Write(Html.Raw(datamap));

#line default
#line hidden
            EndContext();
            BeginContext(2778, 93, true);
            WriteLiteral("\';\r\n        var datamaptb = $(\"#row-data\").attr(\"tablemapcls\");\r\n        var tabletotable = \'");
            EndContext();
            BeginContext(2872, 22, false);
#line 68 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
                       Write(Html.Raw(tabletotable));

#line default
#line hidden
            EndContext();
            BeginContext(2894, 2192, true);
            WriteLiteral(@"';

        var datamaps = datamap.split(',');

        var datamaptbs = datamaptb.split(',');
        var tabletotables = tabletotable.split(',');
        var map = [];
        for (var i = 0; i < datamaps.length; i++) {
            var d = datamaps[i].split('-');
            $("".binddata"").each(function () {
                if (d[0]==$(this).attr(""col"")){
                    $(this).addClass(""mapdata"");
                    $(this).removeClass(""onlybind"");
                    $(this).attr(""clvalue"", d[1]);
                    return false;
                }
            })
        }
        for (var i = 0; i < tabletotables.length; i++) {
            var d = tabletotables[i].split('-');
            $("".binddata"").each(function () {
                if (d[0] == $(this).attr(""col"")) {
                    $(this).removeClass(""onlybind"");
                    $(this).addClass(""tabletotable"");
                    $(this).attr(""datamaptb"", tabletotables[i]);
                    return false;");
            WriteLiteral(@"
                }
            })
        }

        for (var i = 0; i < datamaptbs.length; i++) {
            var d = datamaptbs[i].split('-');
            var ms = d[0].split('&')
            $("".binddata"").each(function () {

                if (ms[0]==$(this).attr(""col"")) {
                    $(this).removeClass(""onlybind"");
                    $(this).addClass(""mapdatacolumn"");
                    $(this).attr(""datamap"",datamaptbs[i]);
                    return false;
                }
            })
        }
        $(""#tb-data thead th"").each(function (index) {
            if ($(this).hasClass('sumdata')) {
                valuesum.push(0);
                indexsum.push(index);
                classindexsum.push($(this).attr(""col""));
            }
        })
        $(""#trheader .calculator"").each(function (index) {
            var t = $(this).attr(""types"").split('?')[1];
            $(""#trheader .binddata"").each(function (index) {
                if (t.search($(this).attr");
            WriteLiteral("(\"col\"))!=-1) {\r\n                    $(this).addClass(\"idcalculator\");\r\n                }\r\n            })\r\n        })\r\n    })\r\n\r\n    </script>\r\n");
            EndContext();
            BeginContext(5086, 39, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "53105ef181b64d2484cb0dabd200bc5d", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(5125, 928, true);
            WriteLiteral(@"

<style>
    .content-wrapper {
        padding: 10px
    }
    .sp-pay{
        float:right;
    }
    .sp-money-pay{
float:right
    }
    .paymentbill {
        max-width: 100% !important
    }
    .table-total td {
        padding: 0
    }
    .td-total{
        width:20%
    }
    .remove-item {
        color: red;
        cursor: pointer
    }

    .d_left {
        float: left;
        min-width: 100px;
    }

    .sumdata {
        position: absolute;
        top: -30px;
        color: red;
    }

    .input_number {
        max-width: 50px;
        text-align: center;
    }

    .payment {
        border-radius: 6px;
        border: 1px solid;
        padding: 10px;
        margin-top: 10px;
    }
</style>
<input type=""button"" style=""display:none"" id=""loadbill"" data="""" value=""load bill"" onclick=""loadbill()"" />
<main>
    <div id=""details"" class=""clearfix""");
            EndContext();
            BeginWriteAttribute("datata", " datata=\"", 6053, "\"", 6125, 1);
#line 180 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
WriteAttributeValue("", 6062, Newtonsoft.Json.JsonConvert.SerializeObject(Model["dataroot"]), 6062, 63, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(6126, 30, true);
            WriteLiteral(">\r\n        <div id=\"client\">\r\n");
            EndContext();
#line 182 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
             foreach (var i in (List<string>)Model["titlehead"])
            {
                var t = type[j];
                models["type"] = t;

#line default
#line hidden
            BeginContext(6308, 82, true);
            WriteLiteral("                <div style=\"clear:both\">\r\n                    <div class=\"d_left\">");
            EndContext();
            BeginContext(6391, 1, false);
#line 187 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
                                   Write(i);

#line default
#line hidden
            EndContext();
            BeginContext(6392, 9, true);
            WriteLiteral(":</div>\r\n");
            EndContext();
#line 188 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
                     if (t.Contains("dropdowbinddata"))
                    {
                        var ts = t.Split('-');
                        models["from"] = "headbill " + ts[1];
                        Html.RenderPartial("~/Views/UserControls/DropDataAndBind.cshtml", models);
                    }
                    else if (t.Contains("twodropdowdata"))
                    {
                        t = System.Web.HttpUtility.HtmlDecode(t);
                        var ts = t.ToString().Split('-');
                        models["table"] = ts[1];
                        var lsub = ts[3].Split('/');
                        models["title"] = lsub[0].ToString();
                        models["columnassign"] = Html.Raw(lsub[1].ToString());
                        models["subclass"] = "notnull";
                        Html.RenderPartial("~/Views/UserControls/ViewTwoDrop.cshtml", models);
                    }
                    else if (t.Contains("dropdowdata"))
                    {
                        var ts = t.Split('-');
                        models["value"] = "";
                        models["title"] = "";
                        models["columnassign"] = "";
                        models["table"] = ts[1];
                        models["columnget"] = "id";
                        models["errnote"] = "";
                        models["subclass"] = "";
                        models["from"] = "headbill " + ts[1];
                        models["where"] = ts[3];
                        models["keyevent"] = "";
                        Html.RenderPartial("~/Views/Shared/DropdowData.cshtml", models);
                    }
                    else if (t.Contains("txtbind"))
                    {
                        string[] para = t.Split('-');

#line default
#line hidden
            BeginContext(8232, 28, true);
            WriteLiteral("                        <div");
            EndContext();
            BeginWriteAttribute("class", " class=\"", 8260, "\"", 8285, 2);
            WriteAttributeValue("", 8268, "bindding", 8268, 8, true);
#line 223 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
WriteAttributeValue(" ", 8276, para[1], 8277, 8, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(8286, 9, true);
            WriteLiteral("></div>\r\n");
            EndContext();
#line 224 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
                    }
                    else if (t.Contains("textbind"))
                    {
                        string[] para = t.Split('-');

#line default
#line hidden
            BeginContext(8450, 42, true);
            WriteLiteral("                        <input type=\"text\"");
            EndContext();
            BeginWriteAttribute("class", " class=\"", 8492, "\"", 8517, 2);
            WriteAttributeValue("", 8500, "bindding", 8500, 8, true);
#line 228 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
WriteAttributeValue(" ", 8508, para[1], 8509, 8, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(8518, 5, true);
            WriteLiteral(" />\r\n");
            EndContext();
#line 229 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
                    }
                    else if (t == "strings")
                    {

#line default
#line hidden
            BeginContext(8615, 60, true);
            WriteLiteral("                        <input type=\"text\" style=\"width:70%\"");
            EndContext();
            BeginWriteAttribute("class", " class=\"", 8675, "\"", 8730, 2);
            WriteAttributeValue("", 8683, "headbill", 8683, 8, true);
#line 232 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
WriteAttributeValue(" ", 8691, cmd.RemoveUnicode(i).Replace(" ", ""), 8692, 38, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(8731, 5, true);
            WriteLiteral(" />\r\n");
            EndContext();
#line 233 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
                    }
                    else if (t.Contains("address-"))
                    {
                        Dictionary<string, object> md = new Dictionary<string, object>();
                        md["datatype"] = t;
                        md["from"] = "invoice";
                        md["subclass"] = "notnull";
                        md["value"] = "";
                        Html.RenderPartial("~/Views/UserControls/AddressInvoice.cshtml", md);
                    }
                    else if (t.Contains("inputandbind"))
                    {
                        Html.RenderPartial("~/Views/UserControls/InputAndBind.cshtml", t);
                    }

#line default
#line hidden
            BeginContext(9431, 24, true);
            WriteLiteral("                </div>\r\n");
            EndContext();
#line 248 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
                j++;
            }

#line default
#line hidden
            BeginContext(9492, 97, true);
            WriteLiteral("        </div>\r\n  \r\n        <div id=\"invoice\">\r\n            <h1 style=\"text-transform:uppercase\">");
            EndContext();
            BeginContext(9590, 17, false);
#line 253 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
                                            Write(Model["tinvoice"]);

#line default
#line hidden
            EndContext();
            BeginContext(9607, 74, true);
            WriteLiteral("</h1>\r\n            <div class=\"date\">Ngày hóa đơn:<span class=\"datebill\"> ");
            EndContext();
            BeginContext(9682, 35, false);
#line 254 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
                                                              Write(DateTime.Now.ToString("dd/MM/yyyy"));

#line default
#line hidden
            EndContext();
            BeginContext(9717, 82, true);
            WriteLiteral("</span></div>\r\n            <div class=\"date\">Số hóa đơn: <span class=\"invoicenum\">");
            EndContext();
            BeginContext(9800, 19, false);
#line 255 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
                                                              Write(Model["invoicenum"]);

#line default
#line hidden
            EndContext();
            BeginContext(9819, 79, true);
            WriteLiteral("</span></div>\r\n            <div class=\"date\">Nhân viên: <span class=\"employer\">");
            EndContext();
            BeginContext(9899, 17, false);
#line 256 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
                                                           Write(Model["username"]);

#line default
#line hidden
            EndContext();
            BeginContext(9916, 90, true);
            WriteLiteral("</span></div>\r\n        </div>\r\n    </div>\r\n    <div class=\"table-responsive\" id=\"row-data\"");
            EndContext();
            BeginWriteAttribute("checkiv", " checkiv=\"", 10006, "\"", 10037, 1);
#line 259 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
WriteAttributeValue("", 10016, Model["checkinvent"], 10016, 21, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginWriteAttribute("keyinfor", " keyinfor=\"", 10038, "\"", 10067, 1);
#line 259 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
WriteAttributeValue("", 10049, Model["keyinfor"], 10049, 18, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginWriteAttribute("keyadd", " keyadd=\"", 10068, "\"", 10093, 1);
#line 259 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
WriteAttributeValue("", 10077, Model["keyadd"], 10077, 16, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(10094, 26, true);
            WriteLiteral(" style=\"max-height:300px;\"");
            EndContext();
            BeginWriteAttribute("tablemapcls", " tablemapcls=\"", 10120, "\"", 10159, 1);
#line 259 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
WriteAttributeValue("", 10134, Html.Raw(tablemapcolumn), 10134, 25, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(10160, 3, true);
            WriteLiteral(">\r\n");
            EndContext();
#line 260 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
          
            Dictionary<string, object> modeltable = new Dictionary<string, object>();
            modeltable["title"] = string.Join(",", title);
            modeltable["column"] = string.Join(",", column);
            modeltable["columnsum"] = string.Join(",", columnsum);
            modeltable["sumend"] = string.Join(",", sumend);
            modeltable["types"] = string.Join(",", types);
            modeltable["keysnotsame"] = string.Join(",", keysnotsame);
            modeltable["notnull"] = string.Join(",", notnull);
            modeltable["addmore"] = string.Join(",", addmore);
            for (var i = 0; i < title.Length; i++)
            {
                if (columnsum.Contains(column[i]))
                {
                    ttsum.Add(title[i]);
                }
            }
            if (!cmd.ismobile(Context.Request.Headers["User-Agent"].ToString().ToLower())) {
                Html.RenderPartial("~/Views/Shared/TbInvoiceDesktop.cshtml", modeltable);
            }
            else
            {
                Html.RenderPartial("~/Views/Shared/TbInvoiceMobile.cshtml", modeltable);
            }
        

#line default
#line hidden
            BeginContext(11337, 59, true);
            WriteLiteral("      \r\n    </div>\r\n    <table class=\"table table-total\">\r\n");
            EndContext();
#line 288 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
         for (var i = 0; i < ttsum.Count; i++)
        {


#line default
#line hidden
            BeginContext(11457, 65, true);
            WriteLiteral("            <tr>\r\n\r\n                <td style=\"text-align:right\">");
            EndContext();
            BeginContext(11523, 8, false);
#line 293 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
                                        Write(ttsum[i]);

#line default
#line hidden
            EndContext();
            BeginContext(11531, 26, true);
            WriteLiteral("</td>\r\n                <td");
            EndContext();
            BeginWriteAttribute("id", " id=\"", 11557, "\"", 11575, 2);
            WriteAttributeValue("", 11562, "sum-", 11562, 4, true);
#line 294 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
WriteAttributeValue("", 11566, ttsum[i], 11566, 9, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(11576, 52, true);
            WriteLiteral(" class=\"div-sum td-total\"></td>\r\n            </tr>\r\n");
            EndContext();
#line 296 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
        }

#line default
#line hidden
            BeginContext(11639, 8, true);
            WriteLiteral("        ");
            EndContext();
#line 297 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
         if (Model["isdiscount"].ToString() == "1")
        {

#line default
#line hidden
            BeginContext(11703, 458, true);
            WriteLiteral(@"            <tr>
                <td style=""text-align:right"">Chiết khấu %:</td>
                <td class=""td-total"">
                    <input onkeyup=""sumends()"" type=""text"" maxlength=""3"" class=""input_number discountpercent"" />
                </td>
            </tr>
            <tr>
                <td style=""text-align:right"">Tiền chiết khấu::</td>
                <td class=""moneydisc td-total"">
                </td>

            </tr>
");
            EndContext();
#line 311 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
        }

#line default
#line hidden
            BeginContext(12172, 8, true);
            WriteLiteral("        ");
            EndContext();
#line 312 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
         if (Model["giftcode"].ToString() == "1")
        {

#line default
#line hidden
            BeginContext(12234, 527, true);
            WriteLiteral(@"            <tr>
                <td style=""text-align:right"">Mã quà tặng:</td>
                <td class=""td-total"">
                    <input type=""text"" onchange=""getgifcode(this)"" class=""gifcode"" /><br /><span class=""ctgift""></span>
                </td>

            </tr>
            <tr>
                <td style=""text-align:right"">Giá trị:</td>
                <td class=""td-total"">
                    <span class=""moneygift""></span><span class=""unitg""></span>
                </td>

            </tr>
");
            EndContext();
#line 328 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
        }

#line default
#line hidden
            BeginContext(12772, 100, true);
            WriteLiteral("        <tr>\r\n\r\n            <td style=\"text-align:right\">Sum total</td>\r\n            <td id=\"sumend\"");
            EndContext();
            BeginWriteAttribute("datasum", " datasum=\"", 12872, "\"", 12906, 1);
#line 332 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
WriteAttributeValue("", 12882, string.Join(",",sumend), 12882, 24, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(12907, 71, true);
            WriteLiteral(" class=\"div-sum td-total\"></td>\r\n        </tr>\r\n    </table>\r\n   \r\n    ");
            EndContext();
            BeginContext(12978, 3185, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "f4e11aa5b5274390876547a4f7f032c2", async() => {
                BeginContext(12998, 134, true);
                WriteLiteral("\r\n\r\n        <input type=\"hidden\" id=\"txt-bill\" name=\"txt_bill\" />\r\n        <input type=\"hidden\" id=\"txt-detail\" name=\"txt_detail\" />\r\n");
                EndContext();
#line 340 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
         if (Model["payment"].ToString() == "1")
        {

#line default
#line hidden
                BeginContext(13193, 2215, true);
                WriteLiteral(@"            <div class=""modal"" id=""mypayment"">
                <div class=""modal-dialog"">
                    <div class=""modal-content"">

                        <!-- Modal Header -->
                        <div class=""modal-header"">
                            <h4 class=""modal-title"">Hình thức thanh toán</h4>
                            <button type=""button"" class=""close"" data-dismiss=""modal"">&times;</button>
                        </div>
                        <!-- Modal body -->
                        <div class=""modal-body"">
                            <table class=""table-total"">
                                <tr>
                                    <td>Tiền mặt</td>
                                    <td><input type=""text"" class=""input_number paymentbill paymentwidthmoney"" /></td>
                                </tr>
                                <tr>
                                    <td>Cà thẻ</td>
                                    <td><input type=""text"" class=""input_nu");
                WriteLiteral(@"mber paymentbill paymentwidthcard"" /></td>
                                </tr>
                                <tr>
                                    <td>Chuyển khoản</td>
                                    <td><input type=""text"" class=""input_number paymentbill paymentwidthbank"" /></td>
                                </tr>
                                <tr>
                                    <td>Giao hàng Tk</td>
                                    <td><input type=""text"" class=""input_number paymentbill paymentwidthghtk"" /></td>
                                </tr>
                             
                            </table>
                           
                            <p><span>tiền hàng</span><span class=""moneyinvoice sp-money-pay""></span></p>
                            <p><span>tiền thanh toán</span><span class=""moneypaycard sp-money-pay""></span></p>
                            <p><span>tiền dư</span><span class=""moneyreturn sp-money-pay""></span></p>
              ");
                WriteLiteral("          </div>\r\n\r\n                        <!-- Modal footer -->\r\n                        <div class=\"modal-footer\">\r\n                            <input type=\"submit\"");
                EndContext();
                BeginWriteAttribute("notforlack", " notforlack=\"", 15408, "\"", 15441, 1);
#line 380 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
WriteAttributeValue("", 15421, Model["notforlack"], 15421, 20, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(15442, 14, true);
                WriteLiteral(" id=\"savebill\"");
                EndContext();
                BeginWriteAttribute("templeat", " templeat=\"", 15456, "\"", 15525, 1);
#line 380 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
WriteAttributeValue("", 15467, Newtonsoft.Json.JsonConvert.SerializeObject(templeatItem), 15467, 58, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginWriteAttribute("datanonull", " datanonull=\"", 15526, "\"", 15551, 1);
#line 380 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
WriteAttributeValue("", 15539, headnotnull, 15539, 12, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginWriteAttribute("datamap", " datamap=\"", 15552, "\"", 15583, 1);
#line 380 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
WriteAttributeValue("", 15562, Model["headmapdata"], 15562, 21, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(15584, 276, true);
                WriteLiteral(@" value=""Save"" />
                        </div>

                    </div>
                </div>
            </div>
            <p style=""float:right"">
                <input type=""button"" id=""payment"" value=""thanh toán"" onclick=""openpayment()"" />
            </p>
");
                EndContext();
#line 389 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
        }
        else
        {

#line default
#line hidden
                BeginContext(15896, 79, true);
                WriteLiteral("    <p style=\"float:right\">\r\n                <input type=\"submit\" id=\"savebill\"");
                EndContext();
                BeginWriteAttribute("templeat", " templeat=\"", 15975, "\"", 16044, 1);
#line 393 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
WriteAttributeValue("", 15986, Newtonsoft.Json.JsonConvert.SerializeObject(templeatItem), 15986, 58, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginWriteAttribute("datanonull", " datanonull=\"", 16045, "\"", 16070, 1);
#line 393 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
WriteAttributeValue("", 16058, headnotnull, 16058, 12, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginWriteAttribute("datamap", " datamap=\"", 16071, "\"", 16102, 1);
#line 393 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
WriteAttributeValue("", 16081, Model["headmapdata"], 16081, 21, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(16103, 36, true);
                WriteLiteral(" value=\"Save\" />\r\n            </p>\r\n");
                EndContext();
#line 395 "D:\webserver\Webserver\Webserver\Views\Action\CreateInvoice.cshtml"
        }

#line default
#line hidden
                BeginContext(16150, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(16163, 15, true);
            WriteLiteral("\r\n</main>\r\n\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Dictionary<string, object>> Html { get; private set; }
    }
}
#pragma warning restore 1591
