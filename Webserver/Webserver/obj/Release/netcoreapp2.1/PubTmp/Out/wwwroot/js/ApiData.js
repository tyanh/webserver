﻿var head =[];
var clmb =[];
var typemb =[];
var titlemb =[];
var oneline = [];
var pathimg = "";
$(document).ready(function () {
    //$(".keyevent").change(function () {
    //    checkKeysData($(this));
    //});
});
function alertpoup(o) {
    $("#body-alert").html(o);
    $("#note-alert").modal("show");
}
function savestatusInvoice() {
    var namett = $("#statusIv").find(":selected").text();
    var idtt = $("#statusIv").find(":selected").val();
    var notett = $("#notestatus").val();
    var idrow = $("#savestatus").attr("idrow");
    var classtd = $("#savestatus").attr("classtd");
    var notestatusbill = $("#savestatus").attr("notett");
    var obj = $("#savestatus").attr("obj");
    var link = "/api/Api/updateStatusInvoice?obj=" + obj + "&id=" + idrow + "&idstatus=" + idtt + "&note=" + notett;
    var rs = GetAjax(link);
    if (rs == "0") {
        $("#" + idrow).find("." + classtd).html(namett);
        $("#" + idrow).find("." + notestatusbill).html(notett);
        $("#statusbill").modal('toggle');
    }
}
function changestatus(o) {
    $("#notestatus").val('');
    $("#savestatus").attr("classtd", $(o).attr("class"));
    $("#savestatus").attr("idrow", $(o).parent().parent().attr("id"));
    var idtt = $(o).html();
    $("#statusIv").prop("disabled",false)
    $("#statusIv").find("option").each(function () {
        if ($(this).text() == idtt) {
            $(this).prop("selected", true);
            if ($(this).attr("locked") == "1")
                $("#statusIv").prop("disabled", true)
            return false;
        }
    })
    $("#statusbill").modal('toggle');
}
function printData(idprint) {
    var contents = $("#" + idprint).html();
    var frame1 = $('<iframe />');
    frame1[0].name = "frame1";
    frame1.css({ "position": "absolute", "top": "-1000000px" });
    $("body").append(frame1);
    var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
    frameDoc.document.open();
    //Create a new HTML document.
    frameDoc.document.write('<html><head>');
    frameDoc.document.write('</head><body>');
    //Append the external CSS file.
    frameDoc.document.write('<link href="https://localhost:44312/css/Invoice.css" rel="stylesheet" type="text/css" />');
    frameDoc.document.write('<link href="https://localhost:44312/css/site.css" rel="stylesheet" type="text/css" />');

    //Append the DIV contents.
    frameDoc.document.write(contents);
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () {
        window.frames["frame1"].focus();
        window.frames["frame1"].print();
        frame1.remove();
    }, 500);

}

function FloorNumber(o) {
    if (o == "")
        return 0;
    else
        return Math.floor(convertonumber(o));
}
function printInvoice(o) {
    $("#print-detail").empty();
    var printdata = JSON.parse($(o).attr("data"));
    var printdetail = JSON.parse($(o).attr("detail"));
 
    $(".tprint").each(function () {
        var obj = $(this);
        var a = $(this).attr("getdata");
        var b = a.split("&");
        var str = "";
      
        for (var j = 0; j < b.length; j++) {
            $.each(printdata, function (index, value) {
                if (value.cl == b[j]) {
                    str = str + value.vl+" ";
                  
                }
                else if (value.cl == "totalmoney") {
                    $("#summoneyend").html(converToString(FloorNumber(value.vl)));
                }
                else if (value.cl == "moneyreturn") {
                    $(".creturn").hide();
                    if (value.vl != "0") {
                        $("#payrtprint").html(converToString(FloorNumber(value.vl)*(-1)));
                        $(".creturn").show();
                    }
                }
                else if (value.cl == "moneynotdiscount") {
                    $("#summoney").html(converToString(convertonumber(value.vl)));
                }
                else if (value.cl == "ctgift") {
                    if (value.vl != "") {
                        $(".cgiftcode").show();
                        $("#ctgift").html(value.vl);
                    }
                    else {
                        $(".cgiftcode").hide();
                    }
                }
                else if (value.cl == "unitg" && value.vl != "") {
                    $("#giftcodeprint").html(value.vl);
                }
                else if (value.cl == "moneygift" && value.vl != "") {
                  
                    var vl_ = value.vl + $("#giftcodeprint").html();
                    $("#giftcodeprint").html(vl_);
                }
                else if (value.cl == "moneypay") {
                    if (value.vl != "") 
                        $("#payvlprint").html(converToString(convertonumber(value.vl)));
                    else
                        $(".cpay").hide()
                }
                else if (value.cl == "discountpercent") {
                    if (value.vl != "") {
                        $(".cDiscount").show();
                        $("#discountprint").html(converToString(convertonumber(value.vl)) + "%");
                    }
                    else {
                        $(".cDiscount").hide();
                    }
                }
                else if (value.cl == "moneydiscount" && value.vl!="") {
                    $("#discountvlprint").html(converToString(Math.floor(convertonumber(value.vl))));
                }
            })
        }
        $(obj).html(str);
    })
    $.each(printdetail, function (index, value) {
        var tr = "<tr>";
        $(".th-detail").each(function () {
            var obj = $(this).attr("getdata");
            tr += "<td>" + value[obj]+"</td>";
        });
        tr += "</tr>";
        $(tr).appendTo("#print-detail");
    })
    generateBarcode($("#barcodeTarget").html());
  
   // $("#print-invoice").modal("toggle");
    printData('body_print');
}
function txtchangegetname(o, key) {
    var keybind = $(o).attr("key");
    var data = JSON.parse($(o).attr('data-sql'));
    $.each(data, function (index, value) {
        if (value[key] == $(o).val()) {
            $(o).attr("idvl", value[keybind]);
            $(o).val(value.name_);
            return false;
        }
       
    })
}
function binddataafterinput(obj) {
    $(obj).attr("idvl", "-1");
    if ($(obj).val() == "")
        return false;
    var o = JSON.parse($(obj).attr("data"));
    var k = $(obj).attr("key");
    var vl = $(obj).val();
    var rs = [];
    $.each(o, function (index, value) {
        if (value[k] == vl) {
            rs = value;
            return false;
        }
    });
    if (rs != "[]") {
        $(obj).attr("idvl", rs.id);
        var class_ = $(obj).attr("classbind").split('/');
        for (var i = 0; i < class_.length; i++) {
            var s_ = class_[i].split('&');
             var obind = $($("." + s_[1]));
            var v_ = rs[s_[0]];
            if ($(obind).attr("type") == "text") {
                $(obind).val(v_);
               
            }
            else
                $(obind).html(v_);
            if ($(obind).hasClass("triggerchange")) {
           
                txtchangegetname($(obind), $(obind).attr('key'));
            }
        }
    }
 
}
function changedropdowbindData(o) {
    var dbind = $(o).find(":selected").attr("databind");
    var p = $(o).parent().parent();
    var class_ = $(o).attr("class-bind").split(',');
    if (dbind == "") {
        for (var i = 0; i < class_.length; i++) {
            $(p).find("." + class_[i]).html('');
        }
        return false;
    }
    var d = JSON.parse(dbind);
    var data = $(o).attr("datas-bind").split(',');
  
    for (var i = 0; i < class_.length; i++) {
        $(p).find("." + class_[i]).html(d[data[i]]);
    }
}
function checkKeysData(str) {
    $("#faile-note").html("");
    if ($("#keys-" + str).length <= 0)
        return true;
    var o = $("#keys-" + str);
    var vl = $(o).val();
    if (vl == "")
        return false;
    var tb = $(o).attr("obj");
  
    var vlcp = $(o).attr("id-data");
    var getdata = $(o).attr("getdata");
    var keytxt = $(o).attr("keytxt").split(' ');
    var keytxts = [];
    var keycolumn = [];
    if (keytxt.length > 0) {
        var objfrom = $(o).attr("objfrom");
        for (var i = 0; i < keytxt.length; i++) {
            var strkey = keytxt[i].split("-")[0];
            var oobj = $("." + strkey + "");
            var vlk = "";
            if ($(oobj).hasClass("cl_dropdowdata") || $(oobj).hasClass("reference_")) {
                vlk = $(oobj).attr("id_");
            }
            else if ($(oobj).hasClass("optionone")) {
                var g = $(oobj).attr("group");
                vlk = $('input[name=' + g + ']:checked', "#" + $(oobj).attr("id") + "").val();
            }
            else {
                vlk = $(oobj).val();
            }
            var subkey = {
                name: strkey, vl: vlk
            }
            keycolumn.push(strkey);
            keytxts.push(subkey);
        }
    }
   

    var link = "/api/Api/CheckDataKey";
    var d = {
        obj: tb,
        id: vlcp,
        getdata: $(o).attr("keytxt"),
        keytxts: JSON.stringify(keytxts)
    }
    
    var a = PostAjax(link, JSON.stringify(d));
    if (a != "0") {
        var ex = "<p>Các thông tin sau có thể đã tồn tại</p>";
        for (var p = 0; p < keycolumn.length; p++) {
            ex = ex + "<p>" + keycolumn[p] + "</p>";
        }
        alertpoup(ex);
        return false;
    }
    return true
}
function addAttr(o) {
    var p = '<p class="p_attr"><input type="text" class="name_attr" placeholder="Tên thuộc tính" />: <input type="text" class="ct_attr" placeholder="giá trị" /><span class="remove" onclick="removeItem(this)">-</span></p>';
    var div = $(o).parent().find(".div-item-attr");
    $(p).appendTo($(div));
}
function converlinkUpload(o,sub) {
    var pathup = o.split(';');
    var p = o;
 
    if (pathup[1].search("reaplace") !== -1) {
        var f_ = pathup[1].split(',');
        var column = f_[1].split('-');
        var columng = column[2];
        var obj = $("." + column[1] + "");
        f_[1] = $(obj).attr(columng);
        pathup[1] = f_.toString();
        p= pathup.join(";");
    }
    else if (pathup[1].search("parent") !== -1) {
        var fp_ = pathup[1].split(',');
      
        var columnp = fp_[1].split('-');
        var columngp = columnp[2];
        var objp = $(sub).find("." + columnp[1] + "");
        fp_[1] = $(objp).attr(columngp);
        pathup[1] = fp_.toString();
        if (fp_[1] == "-1") {
            alert("Vui lòng chọn " + $(objp).attr("errnote"));
            return "";
        }
        p = pathup[0] + ";" + fp_[0] + "," + fp_[1] + ";" + pathup[2];
    }
        return p;
}
function openAddSubData(o) {
    var main = $(o).attr("mainobj");
    var origin = $(o).attr("origin");
    var datatarget = $(o).attr("data-target");
    var path = $(o).attr("path");
    var from_ = $(o).attr("from_");
    if (main != "" && path != "" ) {
        var a = $("." + main + "." + origin + "").attr("id_");
        var objs = $("." + path + "." + from_ + "");
        $(objs).attr("id_", a);
        if ($(objs).hasClass("cl_dropdowdata")) {
            $(objs).find(".select-data").prop('disabled', true);
            $(objs).find(".select-data").val(a);
        }
        //$(objs).prop('disabled', true);
    }

    $("" + datatarget + "").modal("show");
}
function getdatapost(o, h) {
    var column = [];
    var vl = [];
    var rs = true;
    var count = 0;

    if ($(o).find(".ct_sub_div").length > 0) {
        $(o).find(".ct_sub_div").each(function (index) {
            if ($(this).hasClass(h)) {
                var o = $(this).attr("name");
                column.push(o);
                var v_ = [];
                $(this).find("p").each(function (index) {
                    var item = $(this).attr("iditem");
                    v_.push(item);
                });
                vl.push(v_.toString());
            }
        });
    }
    if ($(o).find(".seoString").length > 0) {
        $(o).find(".seoString").each(function (index) {
            if ($(this).hasClass(h)) {
                var o = $(this).attr("name");
           
                column.push(o);
                var t = $(this).attr("typedt").split('-');
                var vseo = seoname($("." + t[1]).val());
                vl.push(vseo);
            }
        });
    }
    if ($(o).find(".data-address").length > 0) {
        $(o).find(".data-address").each(function (index) {
            if ($(this).hasClass(h)) {
                var address = $(this).find(".addstr");
                var city = $(this).find(".city");
                var ward = $(this).find(".ward");
                var district = $(this).find(".district");
                column.push($(address).attr("name"));
                column.push($(city).attr("name"));
                column.push($(ward).attr("name"));
                column.push($(district).attr("name"));
                vl.push($(address).val());
                vl.push($(city).attr("idvl"));
                vl.push($(ward).attr("idvl"));
                vl.push($(district).attr("idvl"));
            }
        });
    }
    if ($(o).find(".optionone").length > 0) {
        $(o).find(".optionone").each(function (index) {
            var o = $(this).attr("column");
            var g = $(this).attr("group");
            column.push(o);
            var v_ = $('input[name=' + g + ']:checked', "#" + $(this).attr("id") + "").val();
            vl.push(v_.toString());
        });
    }
    if ($(o).find(".mapcolumntables").length > 0) {
        $(o).find(".mapcolumntables").each(function (index) {
            if ($(this).hasClass(h)) {
                var o = $(this).attr("name");
                column.push(o);
                var v_ = [];
                $(this).find(".cl-sql").each(function (index) {
                    v_.push($(this).html());
                })
                vl.push(v_.toString());
            }
        });
    }
    if ($(o).find(".maptabletotable").length > 0) {
        $(o).find(".maptabletotable").each(function (index) {
            if ($(this).hasClass(h)) {
                var o = $(this).attr("name");
                column.push(o);
                var v_ = [];
                $(this).find(".cl-sql").each(function (index) {
                    v_.push($(this).html());
                })
                vl.push(v_.toString());
            }
        });
    }
    
    if ($(o).find(".type-datadl-choose").length > 0) {
        $(o).find(".type-datadl-choose").each(function (index) {
            if ($(this).hasClass(h)) {
                var o = $(this).attr("name");
                column.push(o);
                var v_ = [];
                $(this).find(".cl-sql").each(function (index) {
                    v_.push($(this).html());
                })
                vl.push(v_.toString());
            }
        });
    }

    if ($(o).find(".databade_").length > 0) {
        $(o).find(".databade_").each(function (index) {
            if ($(this).hasClass(h)) {
                var addcl = true;
                var o = $(this).attr("name");
                var v_ = $(this).val().trim();
                if ($(this).hasClass("mailaddress")) {
                    if ($(this).val() != "") {
                        if (!validateEmail($(this).val())) {
                            alert("vui lòng kiểm tra email");
                            $(this).focus();
                            rs = false;
                            return false;
                        }
                    }

                }
                else if ($(this).hasClass("password")) {
                    var dataold = $(this).attr("data-val");
                    if (dataold == $(this).val()) {
                        addcl = false;
                    }
                    else {
                        var vrepeat = $(this).parent().find(".datapass_repeat").val();
                        if ($(this).val() != vrepeat) {
                            alert("vui lòng nhập lại password")
                            $(this).find(".datapass_repeat").focus();
                            rs = false;
                            return false;
                        }
                    }
                }
                else if ($(this).hasClass("reference_")) {
                    v_ = $(".reference_" + o).attr("id_");
                }
                else if ($(this).hasClass("getvalue_")) {
                    v_ = $(".getvalue_" + o).attr("id_");
                }
                else if ($(this).hasClass("option")) {
                    v_ = $('input[name="' + o + '"]:checked').val();
                }
                else {
                    if ($(this).hasClass("input_number"))
                        v_ = convertonumber(v_);
                }
                
                    column.push(o);
                    vl.push(v_);
           
            }
        });
    }
    
    if ($(o).find(".datachk_").length > 0) {
        $(o).find(".datachk_").each(function (index) {
            if ($(this).hasClass(h)) {
                var o = $(this).attr("name");
                column.push(o);
                var v_ = "0";
                if ($(this).is(':checked') === true)
                    v_ = "1";
                vl.push(v_);
            }
        });
    }
   
    if ($(o).find(".div_pass").length > 0) {
        $(o).find(".div_pass").each(function (index) {
            var o = $(this).attr("name");
            column.push(o);
            var v_ = "";
            var pass = $(this).find(".datapass_").val();
            var pass_rp = $(this).find(".datapass_repeat").val();
            if (pass.toString() === pass_rp.toString()) {
                v_ = pass;
            }
            else {
                $(this).find(".datapass_repeat").focus();
                result = false;
            }
            vl.push(v_.toString());
        });
    }
    if ($(o).find(".div_img").length > 0) {
        if ($("#folder-" + h + "").val() != "") {
            var pathup = $("#folder-" + h + "").val();
            $(o).find(".div_img").each(function (index) {
                if ($(this).hasClass(h)) {
                    var o = $(this).attr("name");
                    column.push(o);
                    var v_ = [];
                    $(this).find(".c_img").each(function (index) {
                        if ($(this).attr("data") == "old_") {
                            v_.push($(this).attr("name"));
                        }

                    });
                    var n = uploadFilesbag64("."+$(this).attr("arear"), "img_new", pathup);
                    if(n!="")
                     v_.push(n);
                     vl.push(v_.toString());
               
                }
            });
        }
    }
   
    if ($(o).find(".cl_dropdowdata").length > 0) {
        $(o).find(".cl_dropdowdata").each(function (index) {
            if ($(this).hasClass(h)) {
                var o = $(this).attr("name");
                var option = $(this).attr("id_");
                if ($(this).hasClass("notnull")) {
                    if (option == "-1") {
                        var err = $(this).attr("errnote");
                        rs = false;
                        alert("vui lòng chọn " + err);
                        return false;
                    }
                }
                column.push(o);
                vl.push(option);
            }
        });
    }
    if ($(o).find(".colorpicker").length > 0) {
        $(o).find(".colorpicker").each(function (index) {
            if ($(this).hasClass(h)) {
                var o = $(this).attr("name");
                var option = $(this).find(".sp-preview-inner").css("background-color");
                column.push(o);
                vl.push(option);
            }
        });
    }
    if ($(o).find(".desc").length > 0) {

        $(o).find(".desc").each(function (index) {
            if ($(this).hasClass(h)) {
                var o = $(this).attr("name-");
                column.push(o);
                var v_ = CKEDITOR.instances[o].getData();
                vl.push(v_);
            }
        });

    }

    if ($(o).find(".table-permission").length > 0) {
        $(o).find(".table-permission").each(function (index) {
            if ($(this).hasClass(h)) {
                var o = $(this).attr("name");
                column.push(o);
                var p = [];
                $(this).find(".tr-permission").each(function (index) {
                    var v_ = {
                        obj: $(this).attr("val"),
                        view: check_ck($(this).find(".view")),
                        add: check_ck($(this).find(".add")),
                        remove: check_ck($(this).find(".remove")),
                        edit: check_ck($(this).find(".edit")),
                        all: check_ck($(this).find(".all")),
                        
                    };

                    p.push(v_);
                });
                vl.push(JSON.stringify(p));
            }
        });
    }

    if ($("#tabseo-" + h).length > 0) {
        var o = $("#tabseo-" + h).attr("name");
        column.push(o);
        var vv_ = [];
        $("#tabseo-" + h).find(".p_seo").each(function (index) {
            var v_ = {
                name: $(this).find(".name_seo").html(),
                vl: $(this).find(".ct_seo").val()
            };
            vv_.push(v_);
        });
        vl.push(JSON.stringify(vv_));
    }
 
    if ($("#tabattr-" + h).length > 0) {
                var vv_ = [];
            var o = $("#tabattr-" + h).attr("name");
                column.push(o);
        $("#tabattr-" + h).find(".p_attr").each(function (index) {
                    var v_ = {
                        name: $(this).find(".name_attr").html(),
                        vl: $(this).find(".ct_attr").val()
                    };
                    vv_.push(v_);
                })
                vl.push(JSON.stringify(vv_))
 
    }
      
    return rs = {
        rs:rs,
        cl: column,
        vl: vl
    }
}
function Setcolor() {
    var id = $('#mycolor').attr("color");
    var o = $("#value_cl").val();
    var sp = '<input type="color" name="color2" value="' + o + '" id="value_cl">';
    $(sp).appendTo("#" + id + " .color");
    $('#mycolor').modal('hide');
}
function openSubdata(o) {
    $(".sub_div").hide();
    var obj = $(o).attr("obj");
    var obj_ct = $(o).attr("content_cl"); 
    $("." + obj).find(".btn_sub_data").attr("obj", obj_ct);
    $(".sub_data").prop("checked", false);
    $("." + obj_ct + " p").each(function (index) {
        var item = $(this).attr("iditem");
        $("." + obj).find("." + obj + item).prop("checked", true);
    });
    $("."+obj).show();
}
function removeItem(o) {
    $(o).parent().remove();
}
function cancelSubdata() {
    $(".sub_div").hide();
}
function openTb_Atr() {
    $("#name_atr").val('');
    $("#value_atr").val('');
    $("#tb_atr_tmp tbody").empty();
    $("#tbody_atr tr").each(function (index) {
        var clone = $(this).clone();
        $(clone).appendTo("#tb_atr_tmp tbody");
    });
    $("#myAttr").modal("toggle");
}
function saveAtrb() {
    $("#tb_att_insert tbody").empty();
    $("#tb_atr_tmp_body tr").each(function (index) {
        var clone = $(this).clone();
        $(clone).appendTo("#tb_att_insert tbody");
    });
    $(tb).appendTo("#tb_att_insert tbody");
}
function removetr(o) {
    $(o).parent().parent().remove();
}
function addAtr() {

    var n_ = $("#name_atr").val();
    var vl_ = $("#value_atr").val();
    if (n_ === "" || vl_ === "")
        return false;
    var tr = "<tr>";
    tr += "<td class='name_'>" + n_ + "</td>";
    tr += "<td class='value_'>" + vl_ + "</td>";
    tr += "<td><i class='flaticon2-delete' onclick='removetr(this)'></i></td>";
    tr += "</tr>";
    $(tr).appendTo("#tb_atr_tmp tbody");
}
function getext_file(o) {
    var ext = o.split('.')[1];
    return ext;
}
function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
function postdata() {
    var btn = $("#save");
    var hassclass = $(btn).attr("savefrom")
    if (!validateData("notnull", hassclass)) {
       
        return false;
    }
    if ($(".btn_reference").length > 0) {
        alert("kiểm tra data");
        $(".btn_reference").focus();
        return false;
    }

    var result = true;
    var rs = getdatapost(".i_" + hassclass + "", hassclass);
    result = rs.rs;

    var key = checkKeysData($("#save").attr("savefrom"))
    if (key == false)
        result=false
    $("#cldata").val(JSON.stringify(rs.cl));
    $("#vldata").val(JSON.stringify(rs.vl));

    return result;

}

function check_ck(o) {
    if ($(o).length <= 0)
        return "";
    if ($(o).prop("checked") == true) {
        return "checked";
    }
    else if ($(o).prop("checked") == false) {
        return "";
    }
}
function save_subdata(o) {
    var p = $(o).parent().parent().find(".sub_data");
    var class_sub = $(o).attr("obj");
    $("." + class_sub).empty();
    var vl = [];
    $(p).each(function (index) {
        if ($(this).prop("checked") === true) {
            var l = "<p idItem='" + $(this).attr("values") + "' name='" + $(this).attr("name_vl")+"'>" + $(this).attr("name_vl") + "<span class='remove' onclick='removeItem(this)'>-</p>";
            vl.push($(this).attr("values"));
            $(l).appendTo("." + class_sub);
        }
       
    });
    $(o).parent().parent().find(".databade_").val(vl.toString());
    $(".sub_div").hide();
}

function validateData(c_,h_) {
    var rs = true;
    $("." + c_).removeClass("not_null");
    $("." + c_).each(function (index) {
        if ($(this).hasClass(h_) && $(this).is('input:text')) {
            var vl = $(this).val();
            if (vl == "") {
                $(this).addClass("not_null");
                $(this).focus();
                var err = $(this).attr("errnote");
                if (err != "")
                    alert("vui lòng nhập "+ err);
                rs = false;
                return false;
            }
        }
    });
    return rs;
}

function getreference(o,b,c) {
    
    var div_rf = $(o).attr("div-ref");
  
    if ($(o).val() == "") {
        $(o).attr("id_", "-1");
        $(".btn_" + div_rf + "").hide();
        $(".btn_" + div_rf + "").removeClass("btn_reference");
        return false;
    }
    var rs = "-1";
    $("." + b + "" + div_rf + " ." + c+"" + div_rf + "").each(function (index) {
        if ($(this).attr("name") == $(o).val()) {
            rs = $(this).attr("code");
            $(o).attr("id_", $(this).attr("code"));
            $(o).attr("seoname", seoname($(o).val()));
            $(".btn_" + div_rf + "").hide();
            $(".btn_" + div_rf + "").removeClass("btn_reference");
            return false;
        }

    });
    if (rs == "-1") {
        $(".btn_" + div_rf + "").show();
     
        $(".btn_" + div_rf + "").addClass("btn_reference");
    }
}
function addnewrefe(o) {
    var value = $("." + $(o).attr("input-txt")).val();
    var obj = $(o).attr("obj");
    var clreturn = $(o).attr("clreturn");
    var data = {
        clreturn: clreturn,
        value: value,
        obj: obj,
        vlofname:''
    };
    if ($(o).hasClass("getvalue")) {
        var subvalue = $(o).attr("sub-value");
        data.vlofname = $("." + subvalue).val();
    }
    var link_ = "/api/Api/AddRefen?value=" + JSON.stringify(data);
    var a_ = GetAjax(link_);
    if (a_ != "-1") {
        $("." + $(o).attr("input-txt")).attr("id_", a_);
        $("." + $(o).attr("input-txt")).attr("seoname", seoname(value));
        if ($(o).hasClass("getvalue")) {
            $(o).parent().removeClass("btn_reference");
            $(o).parent().hide();
        }
        else {
            $(o).hide();
            $(o).removeClass("btn_reference");
        }

    }
  
}
function changeDropDowndata(o) {
    $(o).parent().attr("id_", $(o).find(":selected").val())
}
function GetAjax(l) {
    var rs;
    $.ajax({
        url: l,
        async: false,
        complete: function (data) {
            rs = data.responseText;
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("lỗi nhập dữ liệu")
            return "-1";
        }
    });
    return rs;
}
function PostAjax(l, d) {
    var rs = "-1";
  
    $.ajax({
        url: l,
        type: "POST",
        dataType: "json",
        data: { data: d},
        async: false,
        complete: function (data) {

            rs=data.responseText;
        },
        error: function (data) {
            return "-1";
        }
    });
    return rs
}
function deleterecord(obj, id){
    var l = "/api/Api/delete";
    var d = {
        obj: $(obj).attr("obj"),
        id: $(obj).attr("data"),
    };
   
    var rs = PostAjax(l, JSON.stringify(d));
    if (rs == "0") {
        $(obj).parent().parent().remove();
    }
}
function viewdatainventory(o) {
  
    var data = JSON.parse($(o).attr("data"));
   
    $("#json_inventory .import").html("");
    $("#json_inventory .sale").html("");
    $("#json_inventory .export").html("");
    $("#json_inventory .invent").html("");
    $("#json_inventory .return").html("");
    $("#json_inventory .tranfer").html("");
    for (var i = 0; i < data.length; i++) {
        var d = data[i];
        var obj = $("#bodyjson-inven #wh-" + d.idwhouse);
        $(obj).find(".import").html(d.import);
        $(obj).find(".sale").html(d.sale);
        $(obj).find(".export").html(d.export);
        $(obj).find(".return").html(d.return);
        $(obj).find(".invent").html(d.invent);
        $(obj).find(".tranfer").html(d.tranfer);
        
    }
    $("#json_inventory").modal('toggle');
}
function viewdatajson(o) {
    var data = JSON.parse($(o).attr("data"))
    var target = $(o).attr("targetobj");
    $("#bodyjson-" + target).empty();
    $("#json_" + target).modal("show");
    pathimg = $("#json_" + target).attr("path");
    var mobile = $("#ismobile").val();
    head = $(".hideinput-" + target).val().split(',');
    clmb = $(".hideinput-" + target).attr("map").split(',');
    typemb = $(".hideinput-" + target).attr("typedata").split(',');
    titlemb = $(".hideinput-" + target).attr("title").split(',');
    oneline = $(".hideinput-" + target).attr("oneline").split(',');

    $.each(data, function (index, value) {
        var item = "<tr>";
        if (mobile != "True") {
            $("#tb-json-" + target + " thead th").each(function () {
                var cl = $(this).attr("col");
                var vl_ = value[cl];
                var type = $(this).attr("typedl");
                if (vl_ == null)
                    vl_ = "";
                var datss = [type, vl_];
                vl_ = getcontentTd(datss, pathimg, index + 1);
                item += "<td>" + vl_ + "</td>";
            })
        }
        else {
            var td = getstringDetailPopupMobile(value,index);
            item += td;
        }
        item += "</tr>";
       
        $(item).appendTo($("#bodyjson-" + target));
       
    });
}
function getstringDetailPopupMobile(value, index){
       
        var photo = typemb.indexOf("photo");
        var vlphoto = value[clmb[photo]];
    var item = "";
    if (photo != -1) {
        item +='<td class="td-icon">' +
            '<img src="' + pathimg + "/" + vlphoto + '" />' +
            '</td>';
    }
        item += '<td class="td-content" style="text-align:left;margin-right:5px">' +
        '<div class="header-row">';
       for (var i = 0; i < head.length; i++) {
            var indexvl = titlemb.indexOf(head[i]);
           
            var clg = clmb[indexvl];
            var vl_ = value[clg];
            var type = typemb[indexvl];
            if (vl_ == null)
                vl_ = "";
            var datss = [type, vl_];
           
            vl_ = getcontentTd(datss, pathimg, index + 1);
            if (type == "sott")
                item += '<span class="circle" style="float:left">' + vl_ + '</span>';
            else
                item += '<span>' + vl_ + '</span>';
        }

        item += '</div>';
        if (oneline[0] != "") {
            for (var i = 0; i < oneline.length; i++) {
                var l_ = oneline[i].split('-');
                var span = 1;
                item += '<div class="div-oneline" style="margin-right:5px">';
                for (var j = 0; j < l_.length; j++) {
                    var index_ = titlemb.indexOf(l_[j]);

                    var clg_ = clmb[index_];
                    var vls_ = value[clg_];
                    var type_ = typemb[index_];
                    if (vls_ == null)
                        vls_ = "";
                    var datss_ = [type_, vls_];
                    vls_ = getcontentTd(datss_, pathimg, index_ + 1);
                    item += '<div class="span-' + span + '">' + l_[j] + ':' + vls_ + '</div>';
                    span++;
                }
                item += '</div>';
            }
        }
        for (var t = 0; t < typemb.length; t++) {
            if (head.indexOf(titlemb[t]) == -1 && oneline.toString().search(titlemb[t]) == -1 && typemb[t] != "photo") {

                var clgit = clmb[t];
                var vlit_ = value[clgit];
                var typeit = typemb[t];
                if (vlit_ == null)
                    vlit_ = "";
                var datssit = [typeit, vlit_];

                vlit_ = getcontentTd(datssit, pathimg, index + 1);
                item += '<div class="div-oneline">' + titlemb[t] + ': <span> ' + vlit_ + '</span></div>';
            }
        }
    item += '</td>';
    return item;
}
function viewdetailPr(o) {
    
    var target = $(o).attr("objref");
    var objId = $(o).attr("obj");
    var tbref = $(o).attr("objref");
    $("#tb-view-" + tbref + " tbody").empty();
    var code = $(o).attr("code");
    var name = $(o).attr("name");
    pathimg = $("#" + objId).attr("path");
    var search = $(o).attr("search");
    $("#" + objId + " .modal-title").html(name);

    var mobile = $("#ismobile").val();
    head = $(".hidejson-" + target).val().split(',');
    clmb = $(".hidejson-" + target).attr("map").split(',');
    typemb = $(".hidejson-" + target).attr("typedata").split(',');
    titlemb = $(".hidejson-" + target).attr("title").split(',');
    oneline = $(".hidejson-" + target).attr("oneline").split(',');

    var infor = {
        obj: $(o).attr("objref"),
        objid: $(o).attr("objindex"),
        value: code,
        search: search
    }
    var link = "/api/Api/GetDetail?data=" + JSON.stringify(infor);
    var data = JSON.parse(GetAjax(link));
    var th_cl = [];
    if (mobile != "True") {
        $("#tb-view-" + tbref + " thead th").each(function () {
            var col = $(this).attr("typedl");
            th_cl.push(col);
        })
    }
    else {
        for (var v = 0; v < typemb.length; v++) {
            th_cl.push(typemb[v]);
        }
    }

    var urledit = $("#" + objId).attr('url');
    if (arraygetvlfromkey.length <= 0) {
    $.each(th_cl, function (key, value) {
        var tb = "";
        if (value.search("getvlfromkey") != "-1" || value.search("tooltip") != "-1") {
            var n_ = value.split('-');
            tb = n_[1];
       
            if (tb != "") {
                
                    var l_ = "/api/Api/GetAllData?obj=" + tb + "&cols=*";
                    var subData = JSON.parse(GetAjax(l_));
                    arraygetvlfromkey.push(j = { obj: tb, data: subData });

                }
           

        }


    })
}
    $.each(data, function (index, value) {
        var item = "<tr>";
        if (mobile != "True") {
            $("#tb-view-" + tbref + " thead th").each(function () {
                var col = $(this).attr("col").trim();
                if (col != "") {
                    var type = $(this).attr("typedl");
                    var vl_ = value[col];
                    if (vl_ == null)
                        vl_ = "";
                    var datss = [type, vl_];
                    vl_ = getcontentTd(datss, pathimg, j);

                    item += "<td>" + vl_ + "</td>";
                }
            })
            item += "<td><a target='_blank' href='/Action/Create/" + urledit + "/" + value["id"] + "'>Edit</a></td>";
        }
        else {
            
            var td = getstringDetailPopupMobile(value, index);
            item += td;
        }
        
            item += "</tr>";
        $(item).appendTo("#tb-view-" + tbref + " tbody");

    })

    $("#" + objId).modal('toggle');
}
function decodeEntities(encodedString) {

    var textArea = document.createElement('textarea');

    textArea.innerHTML = encodedString;

    return textArea.value;

}