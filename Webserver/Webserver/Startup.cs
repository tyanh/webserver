﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Office2010.PowerPoint;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Renci.SshNet;
using Webserver.Middlewares;
using Webserver.Models;
namespace Webserver
{
    public class Startup
    {
       
        public static string wwwroot = "";
        public static int maxrecord = 50;
        public static string runsql = "";
        public static StaticObjModel staticObjModel = new StaticObjModel();
        public static List<string> userupdate = new List<string>(); 
        public Startup(IConfiguration configuration, IHostingEnvironment _host)
        {
            Configuration = configuration;
            wwwroot = _host.WebRootPath;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
     
            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromHours(1);
                options.Cookie.HttpOnly = true;
                // Make the session cookie essential
                options.Cookie.IsEssential = true;
            });

           
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            //string constring = Configuration["ConnectionStrings:connectdata"];
            //services.Configure<ConnectionStrings>(Configuration.GetSection("ConnectionStrings"));
            services.AddHttpContextAccessor();
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            // .AddCookie(option=> {
            //    option.Cookie.HttpOnly = true;
            //    option.Cookie.SecurePolicy = CookieSecurePolicy.Always;
            //    option.Cookie.SameSite = SameSiteMode.Lax;
            //})
            .AddFacebook(facebookOptions => {
                facebookOptions.AppId = Configuration["Authentication:Facebook:AppId"];
                facebookOptions.AppSecret = Configuration["Authentication:Facebook:AppSecret"];
                facebookOptions.CallbackPath = "/Home/Login";
            });
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<SessionManager>();
            services.AddMvc();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            //    app.UseExceptionHandler("/Home/Error");
            //    app.UseHsts();
            //}

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSession();
            app.UseHttpContextItemsMiddleware();
            app.UseCookiePolicy();
            //app.UseAuthentication();
            app.UseMvc(routes =>
            {
                //routes.MapRoute(
                //    name: "default",
                //    template: "{controller=Home}/{action=Index}/{id?}");
                routes.MapRoute(
                    "Default", // Route name
                    "{controller}/{action}/{table}/{id}", // URL with parameters
                    new { controller = "Home", action = "Index", table = "", id = "" });
                routes.MapRoute(
                     "Display", // Route name
                     "{Action}/{Display}/{table}/{id}", // URL with parameters
                     new { controller = "Action", action = "Display", table = "", id = "" });
            });
            
        }
    }
}
