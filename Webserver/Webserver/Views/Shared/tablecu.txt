﻿ <table class="responsive-table @ViewBag.url" tablesave="@ViewBag.url">

        <thead>
            <tr>
                @foreach (var i in ViewBag.tittle)
                {
                    <th scope="col">@i.Trim()</th>
                }
                <th scope="col">Edit</th>
            </tr>
        </thead>
        <tbody>
            @foreach (var i in Model.data.Take(100))
            {
                var t = 0;
                var cl = ViewBag.column;

                <tr iddata="@i["id"]">

                    @foreach (var j in ViewBag.tittle)
                    {
                        string strtype = typeclinput(cl[t].Trim());
                        var vl = i[cl[t].Trim()].ToString();

                        if (strtype.Contains("strings_sub") == true)
                        {
                            string[] table = strtype.Split('_');
                            vl = namesubdata(table[table.Length - 1], vl, "id");

                        }
                        else if (strtype.Contains("reference-") == true)
                        {

                            string[] table = strtype.Split('-');
                            if (cl[t].Trim() == seof_)
                            {
                                pathfile = pathfiletmp.Replace(seof_, vl);
                            }
                            vl = namesubdata(table[1], vl, "code");

                        }
                        else if (strtype == "check")
                        {

                            if (vl == "1")
                            {
                                vl = "có";
                            }
                            else
                            {
                                vl = "không";
                            }
                        }
                        else if (strtype == "option")
                        {
                            if (cl[t].Trim() == "sex")
                            {
                                if (vl == "1")
                                {
                                    vl = "nam";
                                }
                                else if (vl == "2")
                                {
                                    vl = "nữ";
                                }
                            }


                        }
                        if (t == 0)
                        {
                            <th scope="row" strtype="@strtype">@vl</th>
                        }
                        else if (t > 0)
                        {
                            try
                            {

                                if (strtype == "photo")
                                {

                                    string[] photo = i[cl[t].Trim()].ToString().Split(',');
                                    <td data-title="@j">

                                        <img src='@pathfile/@photo[0]' style="width:25px" class='img_up'>


                                    </td>

                                }
                                else if (cl[t].Trim() == "attribute")
                                {
                                    if (i[cl[t].Trim()].ToString() != "")
                                    {
                                        List<dynamic> db = JsonConvert.DeserializeObject<List<dynamic>>(i[cl[t].Trim()].ToString());
                                        <td data-title="@j">
                                            @foreach (var dnm in db)
                                            {
                                                <p class="p_attr"><span class="name_attr">@dnm.name.ToString()</span>: <span class="ct_attr">@dnm.vl.ToString()</span></p>
                                            }
                                        </td>
                                    }
                                    else
                                    {
                                        <td data-title="@j"></td>
                                    }
                                }
                                else if (cl[t].Trim() == "listproduct")
                                {
                                    <td data-title="@j">
                                        <p><a href="javascript:void(0)" onclick="viewdetailPr(this)" name="@i["name_"].ToString()" code="@i["listproduct"].ToString()">view</a></p>
                                    </td>
                                }
                                else
                                {


                                    if (cl[t].Trim() == "city")
                                    {
                                        vl = address.getcity(vl);
                                    }
                                    else if (cl[t].Trim() == "distrist")
                                    {
                                        vl = address.getdistr(vl);
                                    }
                                    <td data-title="@j">@vl</td>
                                }


                            }
                            catch
                            {
                                @*<td data-title="@j">err: @cl[t]</td>*@
                            }
                        }
                        t++;
                    }
                    <td data-title="Edit"><a href="/Action/Create/@ViewBag.url/@i["id"]">Edit</a></td>
                </tr>
            }
        </tbody>
    </table>