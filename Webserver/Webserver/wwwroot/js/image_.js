﻿    const fileList = [];
        function openupload() {
        $(".uploadimg").trigger('click');
}
function Add_image_sub(input) {
  
    if (input.files && input.files[0]) {
        for (var i = 0; i < input.files.length; i++) {
            var reader = new FileReader();
            var img_nb = $('.img_up_sub').length + 1;
            reader.onload = function (e) {

                var o = '<div class="c_img_sub img_sub_new" ondblclick="removeImg(this)" name="' + input.files[i].name + '">'
                    + '<img src="' + e.target.result + '" class="img_up_sub img_' + img_nb + '" data="new_" /></div>';
                $(o).appendTo(".div_img_sub");

            };

            reader.readAsDataURL(input.files[i]);
        }
    }
}
function insertImg(file, arear) {

    var n_ = file.name;
    var reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onloadend = function (e) {
   
        var o = '<div class="c_img img_new" ondblclick="removeImg(this)" name="' + n_+'">'
            + '<img src="' + e.target.result + '" class="img_up" data="new_" /></div>';
        $(o).appendTo("." + arear);

    };

}
function Add_image_mobile(input) {
   
    if (input.files && input.files[0]) {
         for (var i = 0; i < input.files.length; i++) {
             insertImg(input.files[i], $(input).attr("arear"))
         
        }
        $('.uploadimg').val("");
    }
}

function Add_image(input) {
    
            if (input.files && input.files[0]) {
                for (var i = 0; i < input.files.length; i++) {
                    var reader = new FileReader();
                    var img_nb = $('.img_up').length + 1;
                    reader.onload = function (e) {

                        var o = '<div class="c_img img_new" ondblclick="removeImg(this)" name="' + input.files[i].name + '">'
                            + '<img src="' + e.target.result + '" class="img_up img_' + img_nb + '" data="new_" /></div>'
                        $(o).appendTo(".div_img");
                        fileList.push(input.files[i]);

                    };

                    reader.readAsDataURL(input.files[i]);
                }

}
return false;

}
function uploadFiles_name(o,began) {
    if (fileList.length <= 0) {
        return false;
    }
    var formData = new FormData();

    for (var i = 0; i < fileList.length; i++) {
        formData.append('file' + i, fileList[i]);
    }
    formData.append('name', o);
    formData.append('began', began);
    $.ajax({
        url: "/api/Api/uploadImgName/",
        type: "POST",
        dataType: "json",
        data: formData,
        contentType: false,
        processData: false,
        async: false,
        success: function (data) {

        },
        error: function (data) {

        }
    })

}
function uploadFilesbag64(class_img, c_,folder) {
    var fs = [];
    
    $(class_img+ " ."+c_).each(function (index) {
        var name = $(this).attr("name");
        var str = $(this).find("img").attr("src");

        $.ajax({
            url: "/api/Api/SaveImages/",
            type: "POST",
            dataType: "text",
            data: { bag: str, name: name, folder: folder },
            async: false,
            complete: function (data) {
                fs.push(data.responseText)
            },
            error: function (data) {
               
            }
        });

    });
    console.log(fs);
    return fs.toString();
}

    function uploadFiles() {
        if (fileList.length <= 0) {
            return false;
        }
    var formData = new FormData();

    for (var i = 0; i < fileList.length; i++) {
        formData.append('file' + i, fileList[i]);
    }
        $.ajax({
        url: "/api/Api/uploadImg/",
    type: "POST",
    dataType: "json",
    data: formData,
    contentType: false,
    processData: false,
    async:false,
    success: function (data) {

    },
            error: function (data) {

    }
    })

}
    function removeImg(o) {
      var index = -1;
        for (var i = 0; i < fileList.length; i++) {
            if (fileList[i].name === $(o).attr("name")) {
                index = i;

             }       
        }
        $(o).remove();
    if (index > -1) {
        fileList.splice(index, 1);
    
}

}
