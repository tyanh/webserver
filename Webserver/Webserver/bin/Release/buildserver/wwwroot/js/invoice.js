﻿var datatmp_ = [];
var ismobile = $("#ismobile").val();
var td_hidden = ["brand", "priceitem"];
function bindcodescan(o) {
    $(".codeitem").val(o);
    $(".codeitem").focus();
    $("#input-invoice-mb").modal("show");
}
function getgifcode(o) {
    $(".moneygift").attr("unit", "");
    $(".moneygift").html('');
    $(".ctgift").html('');
    $(".unitg").html('');
    var code = $(o).val();
    var link_ = "/api/Api/GetGiftCode?code=" +code;
    var a_ = GetAjax(link_);
   
    if (!checkisnum(a_)) {
        var data = JSON.parse(a_);
        var checkcode = true;
        var codeuser = data[0].codeuser;
        if (codeuser != null && codeuser != "") {
            var lcodeis = codeuser.split(',');
            if (lcodeis.indexOf(code) != -1)
                checkcode = false;
        }
        if (checkcode) {
            $(".moneygift").attr("unit", data[0].unit);
            $(".moneygift").html(converToString(data[0].valuecode));
            $(".unitg").html(data[0].unit);
            $(".ctgift").html(data[0].name_);
        }
    }
    sumends();
}
function sumbill() {
    for (var j = 0; j < valuesum.length; j++) {
        valuesum[j] = 0;
    }
    $('.item-new').each(function () {
        for (var i = 0; i < indexsum.length; i++) {
            //var s = $(this).find("td:eq(" + indexsum[i] + ")").html();
            var s = $(this).find(".tr-" + classindexsum[i]).html();
            var v1 = convertonumber(valuesum[i]);
            var v2 = convertonumber(s);
            if (v1 == "")
                v1 = 0;
            if (v2 == "")
                v2 = 0;
            var nsum = parseInt(v1) + parseInt(v2);
            valuesum[i] = nsum;
        }
    })

    for (var t = 0; t < valuesum.length; t++) {
        $(".div-sum:eq(" + t + ")").html(converToString(valuesum[t]));
    }
    sumends();
}
function sumends() {
    var sumend = $("#sumend").attr("datasum").split(",");
    var vl = convertonumber(valuesum[1]);
   
    if ($(".discountpercent").length > 0) {
        $(".moneydisc").html(0);
        var disc = $(".discountpercent").val();
        if (disc != "" && disc != "0") {
            var discmn = Math.floor(((vl * disc) / 100));
            $(".moneydisc").html(converToString(discmn));
            vl = vl - discmn;
        }
    }
   if ($(".moneygift").length > 0) {
        if ($(".moneygift").html() != "") {
            var unit = $(".moneygift").attr("unit");
            var vlgift = convertonumber($(".moneygift").html())
            if (unit == "%") {
                var disgift = Math.floor(((vl * vlgift) / 100));
                vl = vl - disgift;
            }
            else
                vl = vl - vlgift;
        }
    }
    $("#sumend").html(converToString(vl));
}
function openpayment() {
    var m = $('#sumend').html();
    $(".moneyinvoice").html(m);
    checkpayment();
    $("#mypayment").modal("show");
}
function checkpayment() {
    var m = convertonumber($(".moneyinvoice").html());
    if (m == "")
        m = "0";
    var money = convertonumber($(".paymentwidthmoney").val());
    var card = convertonumber($(".paymentwidthcard").val());
    var bank = convertonumber($(".paymentwidthbank").val());
    var ghtk = convertonumber($(".paymentwidthghtk").val());
    if (money == "")
        money = 0;
    if (card == "")
        card = 0;
    if (bank == "")
        bank = 0;
    if (ghtk == "")
        ghtk = 0;
    var p = parseFloat(money) + parseFloat(card) + parseFloat(bank) + parseFloat(ghtk);
    var rt = p - parseFloat(m);
    var reg = "";
    if (rt < 0) {
        reg = "-";
        rt = 0;
    }
    $(".moneypaycard").html(converToString(p));
    $(".moneyreturn").html(converToString(rt));
}
function calculator(vl1, vl2, cal) {
    var v1 = parseFloat(convertonumber(vl1));
    var v2 = parseFloat(convertonumber(vl2));
    var rs = 0;
    if (cal == "+")
        rs = v1 + v2;
    else if (cal == "-")
        rs = v1 - v2;
    else if (cal == "x")
        rs = v1 * v2;
    else if (cal == ":")
        rs = v1 / v2;
    if (!checkisnum(rs))
        rs = 0;
    return rs;
}
function checkisnum(str) {
    var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
    return numberRegex.test(str);
}
function resetdata() {
    $("#tb-data .binddata").each(function () {
       if ($(this).find("input[type=text]").length > 0)
           $(this).find("input[type=text]").val('');
        else
            $(this).html('');

    })
  
    var keyinfor = $("#row-data").attr("keyinfor").split("-");
    var keyinvent = keyinfor[0];
    $("#trheader").find("." + keyinvent).focus();
}
function checknotnull() {
    var rs = true;
    $("#trheader .notnull").each(function () {
        if ($(this).find("input[type=text]").val() == "") {
            rs = false;
            $(this).find("input[type=text]").focus();
            return false;
        }
    })
    return rs;
}
function checkkeyduplicate(vlkeys) {
    var rs = 0;

    $(".item-new").each(function (index) {
        var oobj = $(this);
        var key = [];
        for (var i = 0; i < vlkeys.length; i++) {
            var clkey = $(this).find(".keycode:eq(" + i + ")");
            key.push($(clkey).html());
           
        }
        
        if (vlkeys.toString() == key.toString()) {
            var more = [];
            $(".addmore").each(function () {
                if ($(this).find("input[type=text]").length > 0)
                    more.push($(this).find("input[type=text]").val());
                else
                    more.push($(this).html());
            })
           
            for (var i = 0; i < more.length; i++) {
                var nval = calculator($(this).find(".more:eq(" + i + ")").html(), more[i], "+");
                $(this).find(".more:eq(" + i + ")").html(converToString(nval));
            }
           
            $("#trheader .binddata").each(function (index) {
                if ($(this).hasClass("calculator")) {
                 
                    var calg = $(this).attr("types").split('?');
                    var cals = calg[1].split('&');
                    var first = cals[0].split('/');
                    var newobj = [first[0], first[2]];
                    var vln = [];
                    for (var f = 0; f < newobj.length; f++) {
                        if ($(oobj).find('.idcalculator:eq(' + f + ')').hasClass("more") == false) {
                            var newvl = $("#trheader .idcalculator:eq(" + f + ")");
                            if ($(newvl).find("input[type=text]").length > 0)
                                $(oobj).find('.idcalculator:eq(' + f + ')').html($(newvl).find("input[type=text]").val());
                            else
                                $(oobj).find('.idcalculator:eq(' + f + ')').html($(newvl).html())
                        }
                        vln.push($(oobj).find('.idcalculator:eq(' + f + ')').html());
                    }
                   
                    var vlsumnew = calculator(vln[0], vln[1], first[1]);
                    var class_ = $(this).attr("col");
                    $(oobj).find(".tr-" + class_).html(converToString(vlsumnew));
                }
            })
            rs = -1;
            return false;
        }
       
    })
    return rs;
}
function AddRowBillMobile(o) {
    if (!checknotnull())
        return false;
    var cbf = checkInventBeforExport($("#row-data").attr("checkiv"));
     if (!cbf) {
        return false;
    }
    var rows_mb1 = ["name_", "size"];
    var rows_mb2 = ["qlt", "pricesale", "total","priceimport"];
    var splitkey = $("#row-data").attr("checkiv").split("/");
    var keyinfor = $("#row-data").attr("keyinfor").split("-");
    var keyinvent = keyinfor[0];
    var keys1 = [];
    var count = $(".item-new").length + 1;
    var photo = "";
    var class_tr_photo = "";
    var item = "<tr class='item-new' id='" + $("." + keyinvent).val() + "'><td class='td-photo-mb'></td>";
    item += "<td class='td-mb'>";
    var row1 = "<p>";
    var row2 = "<p>";
    $("#tb-data .binddata").each(function () {
        var cols = $(this).attr("col");
        var more = "tr-" + cols;
        if ($(this).hasClass("addmore"))
            more += " more";
        if ($(this).hasClass("idcalculator"))
            more += " idcalculator";
        var vl = "";
        if ($(this).find("input[type=text]").length > 0)
            vl = $(this).find("input[type=text]").val();
        else
            vl = $(this).html();
        var keys = "";
        if ($(this).hasClass("keycode")) {
            more += " keycode";
            keys1.push(vl)
        }
        if ($(this).attr("col") == splitkey[4])
            more = more + " sumqlt";
         if (td_hidden.indexOf($(this).attr("col")) != -1) {
             more += " td-hide";
        }
        
        if ($(this).attr("types") != "photo") {
            if (rows_mb1.indexOf(cols) != -1)
                row1 += " <span class='" + more + "'>" + vl + "</span>";
            else if (rows_mb2.indexOf(cols) != -1) {
                if (rows_mb2.indexOf(cols) != 2 && row2 !="<p>")
                    row2 += " x <span class='" + more + "'>" + vl + "</span>";
                else if (rows_mb2.indexOf(cols) == 2)
                    row2 += " = <span class='" + more + "'>" + vl + "</span>";
                else
                    row2 += "<span class='" + more + "'>" + vl + "</span>";
            }
            else
                item += getstrtdInvoice(keys, more, vl, "");
        }
        else {
            class_tr_photo = "tr-" + $(this).attr("col");
            photo = vl;
        }
    })
    row1 += "</p>";
    row2 += "</p>";
    item += row1;
    item += row2;
    item += "</td>";
    item += "<td class='remove-item' onclick='removeItem(this)'>x</td>";
    item += "</tr>";
    
    if (checkkeyduplicate(keys1) == 0) {
        $(item).appendTo("#body-data");
        $("#" + $("." + keyinvent).val()).find('.td-photo-mb').html(photo);
        $("#" + $("." + keyinvent).val()).find('.td-photo-mb').addClass(class_tr_photo);
    }
    $("#tb-data thead").find("input[type=text]").val('');
    resetdata();
    sumbill();
}
function AddRowBill(o) {
    if (!checknotnull())
        return false;
    var cbf = checkInventBeforExport($("#row-data").attr("checkiv"));
    if (!cbf) {

        return false;
    }
    var splitkey = $("#row-data").attr("checkiv").split("/");
    var keyinfor = $("#row-data").attr("keyinfor").split("-");
    var keyinvent = keyinfor[0];

    var keys1 = [];
    var count = $(".item-new").length + 1;
    var item = "<tr class='item-new' id='" + $("." + keyinvent).val() + "'><td>" + count + "</td>";

    $("#tb-data .binddata").each(function () {
        var cols = $(this).attr("col");
        var more = "tr-" + cols;
        if ($(this).hasClass("addmore"))
            more += " more";
        if ($(this).hasClass("idcalculator"))
            more += " idcalculator";
        var vl = "";
        if ($(this).find("input[type=text]").length > 0)
            vl = $(this).find("input[type=text]").val();
        else
            vl = $(this).html();
        var keys = "";
        if ($(this).hasClass("keycode")) {
            //keys = "class='keycode'";
            more += " keycode";
            keys1.push(vl)
        }
        if ($(this).attr("col") == splitkey[4])
            more = more + " sumqlt";
        item += getstrtdInvoice(keys, more, vl,"");
    })
   
    item += "<td class='remove-item' onclick='removeItem(this)'>x</td>";
    item += "</tr>";
  
    if (checkkeyduplicate(keys1) == 0)
        $(item).appendTo("#body-data");
    $("#tb-data thead").find("input[type=text]").val('');
    resetdata();
    sumbill();
}
function getstrtdInvoice(keys, more, vl,sub) {
   if (ismobile.toLowerCase() == "false")
        return "<td class='" + more + "'>" + vl + "</td>";
    else
       return "<p class='" + more + "'>" + vl + "</p>";
}
function addItem(o) {
    if (ismobile.toLowerCase() != "false")
        AddRowBillMobile(o);
    else
        AddRowBill(o);
}
function AddItemInVoice(o) {
     $(o).on('keyup', function (e) {
         if (e.which == 13) {
             addItem(o);
        }
        else {
            bindcalculator();
        }
       
    })
 
}
function removeItem(o) {
    $(o).parent().remove();
    sumbill();
}
function getnamedatamap(o, v, c) {
   
    var rs = true;
    for (var i = 0; i < datas.length; i++) {
        if (datas[i].table == o) {
            var datag = datas[i].data;
             for (var j = 0; j < datag.length; j++) {
                if (datag[j][c] == v) {
                    rs = false;
                    return datag[j];
               
                }
            }
        }
        if (!rs)
            return false;
   
    }
    return v;
}
function bindcalculator() {
    $(".calculator").each(function () {
        var t = $(".calculator").attr('types').split('?')[1].split('&');
        var rs = 0;
     
        var cal = t[0].split('/');
        var vl1 = $("#" + cal[0]);
        var vl2 = $("#" + cal[2]);
        var vlue1 = "";
        var vlue2 = "";
        if ($(vl1).find("input[type=text]").length > 0)
            vlue1 = $(vl1).find("input[type=text]").val();
        else
            vlue1 = $(vl1).html();
        if ($(vl2).find("input[type=text]").length > 0)
            vlue2 = $(vl2).find("input[type=text]").val();
        else
            vlue2 = $(vl2).html();

        if (vlue1 == "")
            vlue1 = "0";
        if (vlue2 == "")
            vlue2 = "0";
        rs = calculator(vlue1, vlue2, cal[1]);
        if (t.length > 1) {
            for (var i = 1; i < t.length; i++) {
                var calnext = t[i].split('/');

                var next = calnext[1];
                vnext = "";
                if (!checkisnum(next)) {
                    if ($(next).find("input[type=text]").length > 0)
                        vnext = $(next).find("input[type=text]").val();
                    else
                        vnext = $(next).html();
                }
                else
                    vnext = next;
              
                rs = calculator(rs, vnext, calnext[0]);
            }
        }
        $(this).html(converToString(rs));
    })
}
function savebillInvoice() {
    if ($(".item-new").length <= 0) {
        alertpoup("nhập chi tiết hóa đơn");
        return false;
    }
    
    if ($("#payment").length > 0) {
        var notforlack = $("#savebill").attr("notforlack");
        var mniv =convertonumber($(".moneyinvoice").html());
        var mnpay = convertonumber($(".moneypaycard").html());
        if (mnpay == "")
            mnpay = "0";
        if ((parseFloat(mniv) > parseFloat(mnpay)) && notforlack=="1") {
            alertpoup("Thanh tóan chưa đủ");
            return false;
        }
    }
    var notnull = $("#savebill").attr("datanonull").split(",");
    var dataSub = [];
    for (var i = 0; i < notnull.length; i++) {
        var c = $("." + notnull[i]);
        var vl = "";
        if ($(c).hasClass("select-data"))
            vl = $(c).find(":selected").text();
        else if ($(c).hasClass("cl_dropdowdata"))
            vl = $(c).find(".select-data option:selected").text();
        else if ($(c).hasClass("twodropdowdata")) {
            var vl1 = $(".dropone").find(":selected").val();
            var vl2 = $(".droptwo").find(":selected").val();
            if (vl1 == vl2) {
                alertpoup("Không chọn kho xuất trùng với kho nhập");
                $(".dropone").find("option:eq(0)").prop("selected", true);
                return false;
            }
            vl = vl1;
        }
        else if ($(c).hasClass("autoandbind")) {
            vl = $(c).val();
            if (vl != "") {
                var idv = $(c).attr("idvl");
                if (idv == "-1") {
                    var vladd = [];
                    var d1 = {
                        name: $(c).attr("name"),
                        vl: $(c).val()
                    }
                    vladd.push(d1);
                    var obj_ = $(c).attr("obj");
                    var classbind_ = $(c).attr("classbind").split('/');
                    for (var n = 0; n < classbind_.length; n++) {
                        var sub_ = classbind_[n].split('&');
                        var sobj = $('.' + sub_[1]);
                        var vl_ = "";
                        if ($(sobj).hasClass("getid"))
                            vl_=$(sobj).attr("idvl");
                        else
                            vl_=$(sobj).val();
                        var inext = {
                            name: sub_[0],
                            vl: vl_
                        }
                        vladd.push(inext);
                    }
                    dataSub = {
                        obj: $(c).attr("obj"),
                        data: vladd,
                        key: $(c).attr("name")
                    }
                    
                }
            }
        }
        if (vl == "Chọn ---")
            vl = "";
        if (vl == "") {
            alertpoup($(c).parent().find(".d_left").html());
            return false;
        }
    }

    var map = $("#savebill").attr("datamap").split(",");
    var datas = [];
    for (var i = 0; i < map.length; i++) {
        var c = $("." + map[i]);
        var vl = "";
        if ($(c).hasClass("select-data"))
            vl = $(c).find(":selected").text();
        else if ($(c).hasClass("cl_dropdowdata"))
            vl = $(c).find(".select-data option:selected").text();
        else if ($(c).attr("type")=="text")
            vl = $(c).val();
        else
            vl = $(c).html();
        var data = {
            cl: map[i],
            vl: vl
        }
        datas.push(data);
    }

    var cls = [];
    $("#tb-data thead th").each(function () {
        cls.push($(this).attr("col"))
    })
    var dataitem = [];
    
    $(".item-new").each(function () {
        var templeat = JSON.parse($("#savebill").attr("templeat"));
        var objr_ = $(this);
        $.each(cls, function (index, value) {
     
            if (value != "") {
                var vli = $(objr_).find(".tr-" + value);
                var vli_ = $(vli).html();
                if ($(vli).find("img").length > 0)
                    vli_ = $(vli).find("img").attr("link");
             
                templeat[value] = vli_;
            }
        });
      //$(this).find("td").each(function(index){
      //      if (cls[index] != "") {
      //          var vli = $(this).html();
      //          if ($(this).find("img").length > 0)
      //              vli = $(this).find("img").attr("link");
      //          templeat[cls[index]] = vli;
               
      //      }
      //});
        dataitem.push(templeat);
    })

    $("#txt-bill").val(JSON.stringify(datas));
    $("#txt-detail").val(JSON.stringify(dataitem));
    if (dataSub.length>0) {
        var po = PostAjax("/api/Api/saveSubDataInvoice", JSON.stringify(dataSub));
        if (po == "0")
            return true;
        else
            return false;
    }
    return true;
}
function checkInventBeforExport(obj) {

    if (obj == "")
        return true;
    if (datatmp_ == null)
        return false;
   
    var o = obj.split("/");
    var vlget = JSON.parse(datatmp_[o[0]]);
    var cls = o[1].split("&");
    var classVl = $("." + cls[1]);
    var vl1 = "";
    if ($(classVl).hasClass('cl_dropdowdata') || $(classVl).hasClass('dropone')) {
        vl1 = $(classVl).find(":selected").val();
    }
    if (vl1 == "-1") {
        alertpoup("chọn nơi xuất");
        return false;
    }
       
    var rs = 0;
    $.each(vlget, function (index, value) {
        if (value[cls[0]] == vl1) {
            rs = value[o[2]];
            return false;
        }
      
    })
  
    var sumqlt = 0;
    var objkey = $("." + o[3]).val();
    var qltip = $("#trheader").find("." + o[4]).val();
    if ($("#"+objkey).length>0)
        sumqlt = $("#" + objkey).find(".sumqlt").html();
    sumqlt = parseFloat(sumqlt)+parseFloat(qltip);
    if (parseFloat(rs) <= 0 || parseFloat(rs) < sumqlt) {
        alertpoup("Số lượng sp ko đủ");
        return false;
    }

    return true;
}

function findinfor(o) {
 
    for (var i = 0; i < datas.length; i++) {
        if (datas[i].table == "products") {
            var dataauto = datas[i].data;
            autocompletcustom(o, dataauto);
            break;
        }
    }
     
        $(o).blur(function () {
            checkandbindItem(o);
    });
}
function checkandbindItem(o) {
    if ($(o).val() == "") {
        // resetdata();
        return false;
    }
    var search = false;
    var obj = $(o).attr("obj").split('-');

    for (var i = 0; i < datas.length; i++) {
        if (datas[i].table == obj[1]) {
            var datag = datas[i].data;

            for (var j = 0; j < datag.length; j++) {
                if (datag[j][obj[2]] == $(o).val()) {
                    datatmp_ = datag[j];
                    if ($(".mapdatacolumn").length > 0) {
                        $(".mapdatacolumn").each(function () {
                            var map = $(this).attr("datamap").split('-');

                            var vlmap = getnamedatamap(map[1], datag[j][map[2]], map[3]);

                            var clmaps = map[0].split('&');
                            var clgetmap = map[4].split('&');
                            $.each(clmaps, function (index, value) {
                                var vlm = vlmap[clgetmap[index]];

                                if ($("#trheader #" + value).attr("types") == "photo") {
                                    var limg = vlm.split(',');
                                    vlm = '<img src="/imgWeb/' + limg[0] + '" link="' + limg[0] + '" style="width:25px" class="img_up">';
                                }
                                if (map[0] == "size")
                                    vlm = vlmap["val"] + vlmap["name_"];
                                $("#trheader #" + value).html(vlm);
                            });
                            //$(this).html(vlmap[map[4]]);
                            if ($(this).hasClass("tabletotable")) {
                                var mapnext = $(this).attr("datamaptb").split('-');
                                var bnext = mapnext[1];
                                var tbnext = mapnext[2];
                                var vlnext = vlmap[mapnext[3]];
                                var datanext = getnamedatamap(tbnext, vlnext, mapnext[4]);
                                $(".onlybind#" + bnext).html(datanext[mapnext[5]]);

                            }
                        })
                    }
                    if ($(".mapdata").length > 0) {
                        $(".mapdata").each(function () {
                            var cl = $(this).attr("clvalue");
                            var vlg = datag[j][cl];
                            if ($(this).attr("types").search("numbers") != -1) {
                                vlg = converToString(vlg);
                            }
                            if ($(this).find("input[type=text]").length > 0)
                                $(this).find("input[type=text]").val(vlg);
                            else
                                $(this).html(vlg);
                        })

                    }

                    $(".onlybind").each(function () {
                        var cl = $(this).attr("col");
                        var vlg = datag[j][cl];
                        if (!$(this).hasClass("mapdata")) {
                            $(this).html(vlg);
                        }

                    })
                    search = true;
                    bindcalculator();
                    return false;
                }

            }
            if (!search) {
                alertpoup("không tồn tại sản phẩm này");

            }

            return false;
        }
    }

}
$(document).ready(function () {
    var keyadd = $("#row-data").attr('keyadd').split(',');
    var keyinfors = $("#row-data").attr('keyinfor').split(',');
    var keyinfor = [];
    for (var i = 0; i < keyadd.length; i++) {
        AddItemInVoice($('.' + keyadd[i]));
    }
    for (var j = 0; j < keyinfors.length; j++) {
        var k = keyinfors[j].split('-');
        $('.' + k[0]).attr("obj", keyinfors[j]);
        keyinfor.push(k[0]);
    }
   
    for (var h = 0; h < keyinfor.length; h++) {
     
        findinfor($('.' + keyinfor[h]));
    }
    $("#savebill").click(function () {
        var rs = savebillInvoice();
        if (rs==true)
        displayloading(this);
        return rs;
    });
    $('.getid').blur(function () {
        txtchangegetname($(this),"name_");
        
    })
    $(".paymentbill").keyup(function () {
        checkpayment();
    });
});