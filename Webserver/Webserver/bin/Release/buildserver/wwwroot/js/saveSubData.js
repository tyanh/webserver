﻿function saveSubdata(o) {
    var id = $(o).attr("div-input")
    var keys = $(o).attr("keys");
    var obj = $(o).attr("obj");
    var clumget = $(o).attr("clumget");
    var typedata = $(o).attr("typedata");
    var vlcode = $(o).attr("class-code");
    var classcheck = $(o).attr("classcheck");
    var valueEdit = $(o).attr("value-edit");
    if ($(id+" .btn_reference").length > 0) {
        alert("kiểm tra data");
        return false;
    }
    var cknotnull = validateData("notnull", classcheck);
    if (!cknotnull)
        return false;
    var rs = getdatapost(id, classcheck);
    if (!rs.rs)
        return false;
    rs.cl.push(clumget)
    rs.vl.push($("." + vlcode+"").val());
    var notnull = $(o).attr("notnull");
    var kq = {
        obj: obj,
        data: rs,
        keys: keys,
        notnull: notnull,
        clconnect: clumget,
        valueEdit: valueEdit,
        
    }
    var link = "/api/Api/AddSubData";
    var a = PostAjax(link, JSON.stringify(kq));
    if (a != "-1") {
        $("#tb_sub_" + obj + " tbody tr").each(function () {
            if ($(this).attr("iddata") == a) {
                $(this).remove(); return false;
            }
        })
        var tr = "<tr iddata='"+a+"' id='sub_vl_"+a+"'>";
        $("#tb_sub_" + obj + " .tt-sub").each(function () {
            var clumdata = $(this).attr("class-cl");
            var index_ = rs.cl.indexOf($(this).attr("class-cl"));
            var vl_ = rs.vl[index_];
      
            var objs = $(id).find("." + clumdata + "");
            if ($(objs).hasClass("datachk_")) {
                tr += '<td><input type="checkbox" disabled /></td>';
            }
            else if ($(objs).hasClass("div_img")) {
                var src = $(objs).find(".img_up:eq(0)").attr("src");
                tr += '<td><img src="' + src+'" style="width:25px" class="img_up"></td>';
            }
            else if ($(objs).hasClass("ct_sub_div")) {
                tr += "<td>";
                $(objs).find("p").each(function () {
                   tr += '<p>' + $(this).attr("name")+'</p>';
                })
                tr += "</td>";
            }
            else {
                if ($(objs).hasClass("cl_dropdowdata"))
                    vl_ = $(objs).find(".select-data option:selected").text();
                else
                    vl_ = $(objs).val();

                    tr += "<td>" + vl_ + "</td>";
                
            }
        });
        tr += "<td><a hreft=''>Edit</a></td><td><span class='btn btn-info span-btn' onclick='removeSubdata(this)' obj='" + obj + "' id_='" + a + "' clremove='" + clumget + "' vlremove='" + $("." + vlcode + "").val() + "'>delete</span></td>";
        tr += "</tr>";
        $(tr).appendTo("#tb_sub_" + obj + " tbody");
        $(id).find(".keys").val('');
        if ($(id).find(".div_img").length > 0) {
            $(id).find(".div_img").empty();
        }
    }
    else {
        $(id).find("." + a).focus();
        alert(a);
    }
}
function removeSubdata(o) {
    var kq = {
            obj : $(o).attr("obj"),
            id_: $(o).attr("id_"),
            clremove: $(o).attr("clremove"),
            vlremove: $(o).attr("vlremove"),
    }
    
    var link = "/api/Api/RemoveSubData";
    var a = PostAjax(link, JSON.stringify(kq));
    if(a=="0")
    $(o).parent().parent().remove();
}