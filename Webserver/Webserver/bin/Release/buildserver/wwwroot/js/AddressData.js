﻿var nameCity = [];
var idCity = [];
var nameTow = [];
var idTow = [];

function addOtoStr(o) {
    if (parseInt(o) < 10)
        return "0" + o;
    else
        return o;

}
function data_tow(city,o) {
    $(o).val('');
    var j = nameCity.indexOf(city);
    var CityId = idCity[j];
    nameTow = [];
    idTow = [];
    for (var i = 0; i < tows.length; i++) {
        if (tows[i].maT === CityId) {
            var n = tows[i].ten.toString();
            nameTow.push(n);
            idTow.push(tows[i].ma);
        }
    }
    autocomplet(o, nameTow);
}
function get_city_name(ID) {
    var rs = "";
    ID = addOtoStr(ID);
    for (var i = 0; i < city.length; i++) {
        var j = city[i].ma;
        if (ID == parseInt(j)) {

            rs = city[i].ten;
            return rs;
        }
    }

    return rs;

}
function get_dist_name(ID) {
    var rs = "";
    for (var i = 0; i < tows.length; i++) {
        var j = tows[i].ma;
        if (ID ==parseInt(j)) {

            rs = tows[i].ten;
            return rs;
        }
    }

    return rs;

}
function get_city_Id(name) {
    var rs = "";
    for (var i = 0; i < city.length; i++) {
        var j = city[i].ten;
        if (name == j) {

            rs = city[i].ma;
            return rs;
        }
    }

    return rs;
   
}
function get_dist_Id(name) {
    var rs = "";
    for (var i = 0; i < tows.length; i++) {
        var j = tows[i].ten;
        if (name == j) {
            rs = tows[i].ma;
            return rs;
        }
    }

    return rs;
}